#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import yaml
import jinja2


MAIN_LATEX_FILE = "vs_zbirka.tex"
VAJE_LATEX_FILE = "vs_vaje.tex"
CHAPTER_RE = re.compile(r"\\chapter{(.*?)}")
NALOGA_RE = re.compile(r"^\s*\\begin{naloga}.*?\\end{naloga}", re.DOTALL| re.M)
OZNAKA_RE = re.compile(r"\\begin{naloga}(\[.*?\])*{(.*?)}")  
KOMENTAR_RE = re.compile(r"^\s*%.*")
DELI_RE = {
    'rezultat': re.compile(r"\\begin{rezultat}(.*?)\\end{rezultat}", re.DOTALL| re.M),
    'resitev': re.compile(r"\\begin{resitev}(.*?)\\end{resitev}", re.DOTALL| re.M),
}

poglavja = [
  '01_kombinatorika',
  '02_verjetnost',
  '03_pogojna',
  '04_diskretne',
  '05_zvezne',
  '06_sredine',
  '07_vektorji',
  '08_binomska_normalna',
  '09_cli',
  '10_intervali_zaupanja',
  '11_testiranje_domnev',
  '12_linregresija',
]

def zapisi_dele(poglavje, ime, naloge, tip):
    """Zapiši dele nalog v drugo datoteko"""
    with open("{}_{}.tex".format(poglavje, tip), "w") as fp:
            fp.write("\\section{{{0}}}\n".format(ime))
            end = "\\end{{{}p}}\n".format(tip)
            for naloga in naloge:
                label = OZNAKA_RE.findall(naloga)[0][1]
                begin = "\\begin{{{}p}}{{{}}}".format(tip, label)
                deli = DELI_RE[tip].findall(naloga)
                if len(deli)>0 and len(deli[0].strip())>0:
                    fp.write("{}\n{}\n{}".format(begin, deli[0], end))
                else:
                    fp.write("\\addtocounter{{{}thm}}{{1}}\n".format(tip))

if __name__ == "__main__":
    for poglavje in poglavja:
        with open("{0}.tex".format(poglavje)) as fp:
            source = fp.read()
        source = KOMENTAR_RE.sub("", source)
        ime = CHAPTER_RE.findall(source)[0]
        naloge = NALOGA_RE.findall(source)
        for tip in ['resitev', 'rezultat']:
            zapisi_dele(poglavje, ime, naloge, tip)        
    os.system("latexmk -pdf")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "CLI {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 10. kviz (CLI)/1. skupina"
FILE_NAME = "ovs_kviz_11_1"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

    assert koliko_polj % 2 == 1
    koncna_oznaka = koliko_polj-1
text = """ <p>Ruleto sestavlja {koliko_polj} polj, oštevilčenih od 0 do {koncna_oznaka}.
    Polja z lihimi oznakami so rdeče barve, polje 0 je zelene barve in
    preostala polja s sodimi oznakami so črne barve.
    Pri igri igralec stavi 1€ na rdečo ali črno barvo. Če se kroglica ustavi na polju
    obarvanem z barvo na katero je stavil, dobi izplačana 2€ (torej ima dobiček 1€),
    sicer pa stavo izgubi.</p>
    <p>Izračunajte pričakovani dobiček igralca.&nbsp;{ans[0]}</p>
    <p>Z uporabo CLI izračunajte verjetnost, da bo igralec po {koliko_iger} igrah imel dobiček večji od {dobicek_igralca}.&nbsp;{ans[1]}</p>
    <p>Z uporabo CLI izračunajte verjetnost, da bo hiša po {koliko_iger} igrah imela dobiček večji od {dobicek_hise}.&nbsp;{ans[2]}</p>
    """


def resitev(koliko_polj, koliko_iger, dobicek_igralca, dobicek_hise):
    
    ans = []
    zr = (koliko_polj - 1)/2
    xs = (1, -1)
    ps = (zr/koliko_polj, (zr+1)/koliko_polj)
    r = stats.rv_discrete(values=(xs, ps))
    ans.append(r.mean())
    print("Upanje dobiček", r.mean())
    print("STD", r.std())
    
    m = r.mean() * koliko_iger
    s = r.std() * (koliko_iger**0.5)
    
    n = stats.norm(m, s)
    di = 1 - n.cdf(dobicek_igralca)
    print("Dobicek igralca", di)
    ans.append(di)

    
    dh = n.cdf(-dobicek_hise)
    print("Dobicek hise", dh)
    ans.append(dh)
    
    #mv, mt = mean(v), mean(t)
    #sv = std(v, ddof=1)
    #st = std(t, ddof=1)                                        
    #r = stats.pearsonr(v, t)
    #cv = cov([v, t])[0][1]
    #k = cv/sv**2
    #prediction_t = mt + k*(prediction_v-mv)
    
    #print(v)
    #print(t)
    #print(prediction_v)
    #print("sv", sv)
    #print("cv", cv)
    
    #ans = [mean(v), sv, cv, prediction_t]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    print(ans)
    cloze = text.format(koliko_polj=koliko_polj, koncna_oznaka=koncna_oznaka,
                        dobicek_igralca=dobicek_igralca, koliko_iger=koliko_iger,
                        dobicek_hise=dobicek_hise, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
from random import randint
#seed(0)
def get_data():
    koliko_iger = randint(30, 100)
    koliko_polj = randint(30, 50)
    if koliko_polj % 2 == 0:
        koliko_polj += 1        
    dobicek_igralca = randint(-1, 1)
    dobicek_hise = randint(0, 2)
    return (koliko_polj, koliko_iger, dobicek_igralca, dobicek_hise)

questions = ""
count = 1

while(count<100):
    data = get_data()
text = naloga(*data)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "CLI {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 10. kviz (CLI)/2. skupina"
FILE_NAME = "ovs_kviz_11_2"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

    assert sum(ps) - 1 < 0.0001
    p1, p2, p3 = ps
text = """ <p>
    Janez v službi vsak dan opravi 1 nalogo z verjetnosto {p1}, dve nalogi z verjetnostjo {p2} in
    tri naloge z verjetnostjo {p3}.
</p>
<p>
    Kolikšno je pričakovano število nalog, ki jih bo Janez opravil na dan?.&nbsp;{ans[0]}
</p>
<p>
    Janez bo v službi dobil nagrado, če v {koliko_dni} dneh opravi vsaj {koliko_nalog} nalog. Z uporabo CLI
    izračunajte verjetnost, da bo Janez ne bo dobil nagrade.&nbsp;{ans[1]}
</p>
<p>
    Če v {koliko_dni} opravi manj kot {norma_nalog} nalog, potem bo Janez zaradi nevestnega dela odpuščen.
    Z uporabo CLI izračunajte verjetnost, da bo Janez odbržal službo.&nbsp;{ans[2]}
</p>
    """

def resitev(koliko_dni, ps, norma_nalog, koliko_nalog):
    
    ans = []
    xs = (1, 2, 3)
    r = stats.rv_discrete(values=(xs, ps))
    ans.append(r.mean())
    print("Upanje", r.mean())
    print("STD", r.std())
    
    m = r.mean() * koliko_dni
    s = r.std() * (koliko_dni**0.5)
    n = stats.norm(m, s)
    
    di = n.cdf(koliko_nalog)
    print("Manj kot {} nalog".format(koliko_nalog), di)
    ans.append(di)
    
    dh = 1 - n.cdf(norma_nalog)
    print("Več kot {} nalog".format(norma_nalog), dh)
    ans.append(dh)
    
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    print(ans)
    cloze = text.format(koliko_dni=koliko_dni, p1=p1, p2=p2, p3=p3, norma_nalog=norma_nalog, koliko_nalog=koliko_nalog, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed, choice
from random import randint
#seed(0)
def get_data():
    koliko_dni = randint(30, 100)
    is_sum_10 = False
    while not is_sum_10:
        ps = [randint(1,8) for _ in range(3)]
        is_sum_10 = (sum(ps) == 10)
    ps = [e/10 for e in ps]
    print(ps)

    xs = (1, 2, 3)
    r = stats.rv_discrete(values=(xs, ps))
    norma_nalog = int(r.mean()*koliko_dni + choice([-2, -1, 0]))
    koliko_nalog = int(r.mean()*koliko_dni + choice([2, 3, 4]))
    return koliko_dni, ps, norma_nalog, koliko_nalog

questions = ""
count = 1

while(count<100):
    data = get_data()
text = naloga(*data)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "CLI {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 10. kviz (CLI)/3. skupina"
FILE_NAME = "ovs_kviz_11_3"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

    assert koliko_polj % 2 == 1
    koncna_oznaka = koliko_polj-1
    dobitnih = koncna_oznaka/2
text = """ <p>Pri srečelovu imamo {koliko_polj} srečk. Srečka stane 0.5€. Dobitnih srečk je {dobitnih}, ostane ne prinašajo
zadetka. Vsaka dobitna srečka prinese izplačilo 1€ (torej ima kupec 0.5€ dobička).</p>
    <p>Izračunajte pričakovani dobiček kupca pri nakupu ene srečke.&nbsp;{ans[0]}</p>
    <p>Z uporabo CLI izračunajte verjetnost, da bo kupec pri nakupu {koliko_iger} srečk imel dobiček večji od {dobicek_igralca}.&nbsp;{ans[1]}</p>
    <p>Z uporabo CLI izračunajte verjetnost, da bodo organizatorji ob prodaji {koliko_iger} srečk imeli dobiček večji od {dobicek_hise}.&nbsp;{ans[2]}</p>
    """

def resitev(koliko_polj, koliko_iger, dobicek_igralca, dobicek_hise):
    ans = []
    zr = (koliko_polj - 1)/2
    xs = (0.5, -0.5)
    ps = (zr/koliko_polj, (zr+1)/koliko_polj)
    r = stats.rv_discrete(values=(xs, ps))
    ans.append(r.mean())
    print("Upanje dobiček", r.mean())
    print("STD", r.std())
    
    m = r.mean() * koliko_iger
    s = r.std() * (koliko_iger**0.5)
    
    n = stats.norm(m, s)
    di = 1 - n.cdf(dobicek_igralca)
    print("Dobicek igralca", di)
    ans.append(di)

    
    dh = n.cdf(-dobicek_hise)
    print("Dobicek hise", dh)
    ans.append(dh)
    
    #mv, mt = mean(v), mean(t)
    #sv = std(v, ddof=1)
    #st = std(t, ddof=1)                                        
    #r = stats.pearsonr(v, t)
    #cv = cov([v, t])[0][1]
    #k = cv/sv**2
    #prediction_t = mt + k*(prediction_v-mv)
    
    #print(v)
    #print(t)
    #print(prediction_v)
    #print("sv", sv)
    #print("cv", cv)
    
    #ans = [mean(v), sv, cv, prediction_t]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    print(ans)
    cloze = text.format(koliko_polj=koliko_polj, koncna_oznaka=koncna_oznaka,
                        dobicek_igralca=dobicek_igralca, koliko_iger=koliko_iger,
                        dobicek_hise=dobicek_hise, dobitnih=dobitnih, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
from random import randint
#seed(0)
def get_data():
    koliko_iger = randint(30, 100)
    koliko_polj = randint(30, 50)
    if koliko_polj % 2 == 0:
        koliko_polj += 1        
    dobicek_igralca = randint(-2, 2)/2
    dobicek_hise = randint(0, 2)
    return (koliko_polj, koliko_iger, dobicek_igralca, dobicek_hise)

questions = ""
count = 1

while(count<100):
    data = get_data()
text = naloga(*data)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "CLI {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 10. kviz (CLI)/4. skupina"
FILE_NAME = "ovs_kviz_11_4"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

    assert sum(ps) - 1 < 0.0001
    p1, p2, p3 = ps
text = """ <p>
    Botanik se potika po gorah in išče redke rože. Eno samo najde z verjetnosto {p1}, dve z verjetnostjo {p2} in
    tri z verjetnostjo {p3}.
</p>
<p>
    Kolikšno je pričakovano število rož, ki jih bo botanik našel v enem dnevu?.&nbsp;{ans[0]}
</p>
<p>
    Botanik mora za kakovostno predstavitev zbrati vsaj {koliko_nalog} rož. Predstavitev je čez {koliko_dni} dni. Z uporabo CLI
    izračunajte verjetnost, da bo do takrat našel dovolj redkih rož.&nbsp;{ans[1]}
</p>
<p>
    Če v {koliko_dni} dneh najde manj kot {norma_nalog} rož, potem predstavitve ne more izpeljati.
    Z uporabo CLI izračunajte verjetnost, da predstavitev bo.&nbsp;{ans[2]}
</p>
    """

def resitev(koliko_dni, ps, norma_nalog, koliko_nalog):
    
    ans = []
    xs = (1, 2, 3)
    r = stats.rv_discrete(values=(xs, ps))
    ans.append(r.mean())
    print("Upanje", r.mean())
    print("STD", r.std())
    
    m = r.mean() * koliko_dni
    s = r.std() * (koliko_dni**0.5)
    n = stats.norm(m, s)
    
    di = 1 - n.cdf(koliko_nalog)
    print("Vsaj kot {} nalog".format(koliko_nalog), di)
    ans.append(di)
    
    dh = 1 - n.cdf(norma_nalog)
    print("Več kot {} nalog".format(norma_nalog), dh)
    ans.append(dh)
    
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    print(ans)
    cloze = text.format(koliko_dni=koliko_dni, p1=p1, p2=p2, p3=p3, norma_nalog=norma_nalog, koliko_nalog=koliko_nalog, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed, choice
from random import randint
#seed(0)
def get_data():
    koliko_dni = randint(30, 100)
    is_sum_10 = False
    while not is_sum_10:
        ps = [randint(1,8) for _ in range(3)]
        is_sum_10 = (sum(ps) == 10)
    ps = [e/10 for e in ps]
    print(ps)

    xs = (1, 2, 3)
    r = stats.rv_discrete(values=(xs, ps))
    norma_nalog = int(r.mean()*koliko_dni + choice([-2, -1, 0]))
    koliko_nalog = int(r.mean()*koliko_dni + choice([2, 3, 4]))
    return koliko_dni, ps, norma_nalog, koliko_nalog

questions = ""
count = 1

while(count<100):
    data = get_data()
text = naloga(*data)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "CLI {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 10. kviz (CLI)/5. skupina"
FILE_NAME = "ovs_kviz_11_5"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

    assert koliko_polj % 2 == 1
    koncna_oznaka = koliko_polj-1
    koliko_barve = koncna_oznaka/2
text = """ <p>Pri igri s kartami imamo na kupčku {koliko_polj} kart. V njem je {koliko_barve} kart
    rumene in {koliko_barve} kart zelene barve, ena pa je črna. Igralec vplača 1€ in s kupčka izbere karto.
    Če je karta zelene barve, potem dobi nazaj 2€ (torej ima dobiček 1€), sicer pa vložek izgubi.</p>
    <p>Kolikšen je pričakovani dobiček igralca v eni igri?&nbsp;{ans[0]}</p>
    <p>Z uporabo CLI izračunajte verjetnost, da bo igralec po {koliko_iger} igrah imel dobiček večji od {dobicek_igralca}.&nbsp;{ans[1]}</p>
    <p>Z uporabo CLI izračunajte verjetnost, da bo hiša po {koliko_iger} igrah imela dobiček večji od {dobicek_hise}.&nbsp;{ans[2]}</p>
    """

def resitev(koliko_polj, koliko_iger, dobicek_igralca, dobicek_hise):
    
    ans = []
    zr = (koliko_polj - 1)/2
    xs = (1, -1)
    ps = (zr/koliko_polj, (zr+1)/koliko_polj)
    r = stats.rv_discrete(values=(xs, ps))
    ans.append(r.mean())
    print("Upanje dobiček", r.mean())
    print("STD", r.std())
    
    m = r.mean() * koliko_iger
    s = r.std() * (koliko_iger**0.5)
    
    n = stats.norm(m, s)
    di = 1 - n.cdf(dobicek_igralca)
    print("Dobicek igralca", di)
    ans.append(di)

    
    dh = n.cdf(-dobicek_hise)
    print("Dobicek hise", dh)
    ans.append(dh)
    
    #mv, mt = mean(v), mean(t)
    #sv = std(v, ddof=1)
    #st = std(t, ddof=1)                                        
    #r = stats.pearsonr(v, t)
    #cv = cov([v, t])[0][1]
    #k = cv/sv**2
    #prediction_t = mt + k*(prediction_v-mv)
    
    #print(v)
    #print(t)
    #print(prediction_v)
    #print("sv", sv)
    #print("cv", cv)
    
    #ans = [mean(v), sv, cv, prediction_t]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    print(ans)
    cloze = text.format(koliko_polj=koliko_polj, koncna_oznaka=koncna_oznaka,
                        dobicek_igralca=dobicek_igralca, koliko_iger=koliko_iger,
                        dobicek_hise=dobicek_hise, koliko_barve=koliko_barve, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
from random import randint
#seed(0)
def get_data():
    koliko_iger = randint(30, 100)
    koliko_polj = randint(30, 50)
    if koliko_polj % 2 == 0:
        koliko_polj += 1        
    dobicek_igralca = randint(-1, 1)
    dobicek_hise = randint(0, 2)
    return (koliko_polj, koliko_iger, dobicek_igralca, dobicek_hise)

questions = ""
count = 1

while(count<100):
    data = get_data()
text = naloga(*data)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import base64
from math import log, exp, sqrt, log10
from scipy.stats import norm



NAME = "CLI enakomerna diskretna {}"

CATEGORY = "Kvizi za oceno/Kviz, 12. teden/" + NAME.format('')
FILE_NAME = "vs_kviz_12_1_1"
CLOSE_TPL = "{{1:NUMERICAL:={}:{}}}"
INPUT_TPL = '<input type="text" class="answer" answer="{}" precision="{}">'
INPUT_RE = re.compile(r'<input type="text" class="answer" answer="([^"]*?)" precision="([^"]*?)">')

def format_odgovor(match):
    precision = float(match.group(2))
    answer = float(match.group(1))
    pfmt = "{{:.{}f}}".format(round(-log10(precision)))
    return " \\hfill \\odgovor[{1}]{{{0}}}".format(pfmt.format(answer), precision)

text = """<p>Naj bodo \\(X_i\\in\\{{0,1,2,3,4,5,6,7,8,9\\}}\\) neodvisne 
    enakomerno porazdeljene slučajne spremenljivke.
    Naj bo \\(X=X_1+X_2+\\ldots X_{{{n}}}\\). 
    </p>
   <ol>
    <li>Izračunaj: \\(E(X)=\\) {ans[0]} in \\(\\sigma(X)=\\){ans[1]}. </li>

    <li>Oceni verjetnost, da je \\( {a}< X <{b}\\):&nbsp; {ans[2]} </li>
    
    <li>Najmanj koliko bi moral biti \\(\\Delta\\), da bi bila 
    verjetnost \\(P(|X-E(X)|<\\Delta)\\), večja kot {p}? &nbsp; {ans[3]} </li>
   </ol>
    """

    upanje = sum(i for i in range(1, 10))/10
    sigma = sqrt(n * (sum(i**2 for i in range(1,10))/10 - upanje**2))
    upanje = n * upanje
    ans = [upanje, sigma]
    a = upanje + a
    b = upanje + b

    ans.append(norm.cdf(b, upanje, sigma) - norm.cdf(a, upanje, sigma))
    delta = norm.ppf((p+1)/2, 0, sigma)
    ans.append(delta)
      
    precision = [0.001, 0.001, 0.001, 0.1]
    print(n, a, b, p, ans)
    ans_cloze = [CLOSE_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    ans_html = [INPUT_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    cloze = text.format(n=n, a=a, b=b, p=p, ans=ans_cloze)
    html = text.format(n=n, a=a, b=b, p=p, ans=ans_html)
    return cloze, html

problems = []
count = 1
b = 70
for n in [100,400]:
    for a in [-20,-50]:
        for p in [0.95, 0.9]:
            cloze, html = naloga(n, a, b, p)
            problems.append(cloze)

# change the code below only if you know what you are doing
# remove last question from the bank, since it is in tex
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

problems.pop()
questions = ""
count = 1
for cloze in problems:
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
    count += 1

# write tex file
tex = html.replace("<p>", "")
tex = tex.replace("</p>", "\n\n")
tex = tex.replace("&nbsp;", " ")
tex = tex.replace("<b>", "")
tex = tex.replace("</b>", "")
tex = tex.replace("<ol>", "\\begin{enumerate}")
tex = tex.replace("</ol>", "\\end{enumerate}")
tex = tex.replace("<li>", "\\item ")
tex = tex.replace("</li>", "")

tex = INPUT_RE.sub(format_odgovor, tex)
TEX_TEMPLATE = """
\\begin{{naloga}}
{0}
\\end{{naloga}}
"""
with open(FILE_NAME+".tex","w") as fp:
    print("Writing to file {}.tex".format(FILE_NAME))
    fp.write(TEX_TEMPLATE.format(tex))

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    code = fp.read()

link = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(bytes(code, 'utf-8')).decode())
else:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(code))

# write XML
xml = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=code, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(xml)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import base64
from math import log, exp, sqrt, log10
from scipy.stats import norm



NAME = "CLI enakomerna zvezna {}"

CATEGORY = "Kvizi za oceno/Kviz 12. teden/" + NAME.format('')
FILE_NAME = "vs_kviz_12_2_1"
CLOSE_TPL = "{{1:NUMERICAL:={}:{}}}"
INPUT_TPL = '<input type="text" class="answer" answer="{}" precision="{}">'
INPUT_RE = re.compile(r'<input type="text" class="answer" answer="([^"]*?)" precision="([^"]*?)">')

def format_odgovor(match):
    precision = float(match.group(2))
    answer = float(match.group(1))
    pfmt = "{{:.{}f}}".format(round(-log10(precision)))
    return " \\hfill \\odgovor[{1}]{{{0}}}".format(pfmt.format(answer), precision)

text = """<p>Naj bodo \\(X_i\\) neodvisne slučajne spremenljivke, ki so
    enakomerno porazdeljene na intervalu \\([3, {b}]\\).
    Naj bo \\(X=X_1+X_2+\\ldots X_{{{n}}}\\). 
    </p>
   <ol>
    <li>Izračunaj: \\(E(X)=\\) {ans[0]} in \\(\\sigma(X)=\\){ans[1]}. </li>

    <li>Oceni verjetnost, da je \\( {z1}< X <{z2}\\):&nbsp; {ans[2]} </li>
    
    <li>Najmanj koliko bi moral biti \\(\\Delta\\), da bi bila 
    verjetnost \\(P(|X-E(X)|<\\Delta)\\), večja kot {p}? &nbsp; {ans[3]} </li>
   </ol>
    """

def resitev(n, a, b, p):

    upanje = n*(3+b)/2
    sigma = sqrt(n * (b-3)**2/12)
    ans = [upanje, sigma]
    z1 = upanje - a
    z2 = upanje + 2*a

    ans.append(norm.cdf(z2, upanje, sigma) - norm.cdf(z1, upanje, sigma))
    delta = norm.ppf((p+1)/2, 0, sigma)
    ans.append(delta)
      
    precision = [0.001, 0.001, 0.001, 0.1]
    print(n, a, b, p, ans)
    ans_cloze = [CLOSE_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    ans_html = [INPUT_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    cloze = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, ans=ans_cloze)
    html = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, ans=ans_html)
    return cloze, html

problems = []
count = 1
a = 20
for n in [121,225]:
    for b in [8,9]:
        for p in [0.97, 0.99]:
            cloze, html = naloga(n, a, b, p)
            problems.append(cloze)

# change the code below only if you know what you are doing
# remove last question from the bank, since it is in tex
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

problems.pop()
questions = ""
count = 1
for cloze in problems:
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
    count += 1

# write tex file
tex = html.replace("<p>", "")
tex = tex.replace("</p>", "\n\n")
tex = tex.replace("&nbsp;", " ")
tex = tex.replace("<b>", "")
tex = tex.replace("</b>", "")
tex = tex.replace("<ol>", "\\begin{enumerate}")
tex = tex.replace("</ol>", "\\end{enumerate}")
tex = tex.replace("<li>", "\\item ")
tex = tex.replace("</li>", "")

tex = INPUT_RE.sub(format_odgovor, tex)
TEX_TEMPLATE = """
\\begin{{naloga}}
{0}
\\end{{naloga}}
"""
with open(FILE_NAME+".tex","w") as fp:
    print("Writing to file {}.tex".format(FILE_NAME))
    fp.write(TEX_TEMPLATE.format(tex))

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    code = fp.read()

link = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(bytes(code, 'utf-8')).decode())
else:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(code))

# write XML
xml = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=code, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(xml)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import base64
from math import log, exp, sqrt, log10
from scipy.stats import norm



NAME = "CLI vsota dveh {}"

CATEGORY = "Kvizi za oceno/Kviz 12. teden/" + NAME.format('')
FILE_NAME = "vs_kviz_12_3_1"
CLOSE_TPL = "{{1:NUMERICAL:={}:{}}}"
INPUT_TPL = '<input type="text" class="answer" answer="{}" precision="{}">'
INPUT_RE = re.compile(r'<input type="text" class="answer" answer="([^"]*?)" precision="([^"]*?)">')

def format_odgovor(match):
    precision = float(match.group(2))
    answer = float(match.group(1))
    pfmt = "{{:.{}f}}".format(round(-log10(precision)))
    return " \\hfill \\odgovor[{1}]{{{0}}}".format(pfmt.format(answer), precision)

text = """<p>Naj bodo \\(X_i={a}Y_i+{b}Z_i\\), kjer so 
    $$Y_i\\sim N(-1, 1), \\quad Z_i\\sim N(2, 3)$$ 
    neodvisne slučajne spremenljivke.
    Naj bo \\(X=X_1+X_2+\\ldots X_{{{n}}}\\). 
    </p>
   <ol>
    <li>Izračunaj: \\(E(X)=\\) {ans[0]} in \\(\\sigma(X)=\\){ans[1]}. </li>

    <li>Oceni verjetnost, da je \\( {z1}< X <{z2}\\):&nbsp; {ans[2]} </li>
    
    <li>Največ koliko je lahko \\(z\\), da bo 
    verjetnost \\(P(X>z)\\), večja kot {p}? &nbsp; {ans[3]} </li>
   </ol>
    """

def resitev(n, a, b, p):

    upanje = n*(-a*1+b*2)
    sigma = sqrt(n * (a**2*1 + b**2*9))
    ans = [upanje, sigma]
    z1 = round(upanje - 15*b)
    z2 = round(upanje + 16*a + 3*b)

    ans.append(norm.cdf(z2, upanje, sigma) - norm.cdf(z1, upanje, sigma))
    z = norm.ppf(1-p, upanje, sigma)
    ans.append(z)
      
    precision = [0.001, 0.001, 0.001, 0.1]
    print(n, a, b, p, ans)
    ans_cloze = [CLOSE_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    ans_html = [INPUT_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    cloze = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, ans=ans_cloze)
    html = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, ans=ans_html)
    return cloze, html

problems = []
count = 1
a = 3
n = 144
for a in [3, 5]:
    for b in [4,2]:
        for p in [0.1, 0.05]:
            cloze, html = naloga(n, a, b, p)
            problems.append(cloze)

# change the code below only if you know what you are doing
# remove last question from the bank, since it is in tex
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

problems.pop()
questions = ""
count = 1
for cloze in problems:
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
    count += 1

# write tex file
tex = html.replace("<p>", "")
tex = tex.replace("</p>", "\n\n")
tex = tex.replace("&nbsp;", " ")
tex = tex.replace("<b>", "")
tex = tex.replace("</b>", "")
tex = tex.replace("<ol>", "\\begin{enumerate}")
tex = tex.replace("</ol>", "\\end{enumerate}")
tex = tex.replace("<li>", "\\item ")
tex = tex.replace("</li>", "")

tex = INPUT_RE.sub(format_odgovor, tex)
TEX_TEMPLATE = """
\\begin{{naloga}}
{0}
\\end{{naloga}}
"""
with open(FILE_NAME+".tex","w") as fp:
    print("Writing to file {}.tex".format(FILE_NAME))
    fp.write(TEX_TEMPLATE.format(tex))

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    code = fp.read()

link = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(bytes(code, 'utf-8')).decode())
else:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(code))

# write XML
xml = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=code, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(xml)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import base64
from math import log, exp, sqrt, log10
from scipy.stats import norm



NAME = "CLI zvezna {}"

CATEGORY = "Kvizi za oceno/Kviz 12. teden/" + NAME.format('')
FILE_NAME = "vs_kviz_12_4_1"
CLOSE_TPL = "{{1:NUMERICAL:={}:{}}}"
INPUT_TPL = '<input type="text" class="answer" answer="{}" precision="{}">'
INPUT_RE = re.compile(r'<input type="text" class="answer" answer="([^"]*?)" precision="([^"]*?)">')

def format_odgovor(match):
    precision = float(match.group(2))
    answer = float(match.group(1))
    pfmt = "{{:.{}f}}".format(round(-log10(precision)))
    return " \\hfill \\odgovor[{1}]{{{0}}}".format(pfmt.format(answer), precision)

text = """<p>Naj bodo \\(X_i\\in [0, 1]\\) neodvisne zvezno porazdeljene 
    slučajne spremenljivke z gostoto \\(p_{{X_i}}=2x\\). 
    Naj bo \\(X=X_1+X_2+\\ldots X_{{{n}}}\\). 
    </p>
   <ol>
    <li>Izračunaj: \\(E(X)=\\) {ans[0]} in \\(\\sigma(X)=\\){ans[1]}. </li>

    <li>Oceni verjetnost, da je \\( X < {z1} \\lor {z2} < X \\):&nbsp; {ans[2]} </li>
    
    <li>Največ koliko je lahko \\(z\\), da bo 
    verjetnost \\(P(X>z)\\), večja kot {p}? &nbsp; {ans[3]} </li>
   </ol>
    """

def resitev(n, a, b, p):

    upanje = n*(2/3)
    sigma = sqrt(n/18)
    ans = [upanje, sigma]
    z1 = round(upanje + a)
    z2 = round(upanje + b)

    ans.append(norm.cdf(z1, upanje, sigma) + 1 - norm.cdf(z2, upanje, sigma))
    z = norm.ppf(1-p, upanje, sigma)
    ans.append(z)
      
    precision = [0.001, 0.001, 0.001, 0.1]
    print(n, a, b, p, ans)
    ans_cloze = [CLOSE_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    ans_html = [INPUT_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    cloze = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, ans=ans_cloze)
    html = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, ans=ans_html)
    return cloze, html

problems = []
count = 1
a = -2
for n in [121, 400]:
    for b in [3, 4]:
        for p in [0.025, 0.05]:
            cloze, html = naloga(n, a, b, p)
            problems.append(cloze)

# change the code below only if you know what you are doing
# remove last question from the bank, since it is in tex
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

problems.pop()
questions = ""
count = 1
for cloze in problems:
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
    count += 1

# write tex file
tex = html.replace("<p>", "")
tex = tex.replace("</p>", "\n\n")
tex = tex.replace("&nbsp;", " ")
tex = tex.replace("<b>", "")
tex = tex.replace("</b>", "")
tex = tex.replace("<ol>", "\\begin{enumerate}")
tex = tex.replace("</ol>", "\\end{enumerate}")
tex = tex.replace("<li>", "\\item ")
tex = tex.replace("</li>", "")

tex = INPUT_RE.sub(format_odgovor, tex)
TEX_TEMPLATE = """
\\begin{{naloga}}
{0}
\\end{{naloga}}
"""
with open(FILE_NAME+".tex","w") as fp:
    print("Writing to file {}.tex".format(FILE_NAME))
    fp.write(TEX_TEMPLATE.format(tex))

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    code = fp.read()

link = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(bytes(code, 'utf-8')).decode())
else:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(code))

# write XML
xml = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=code, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(xml)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import base64
from math import log, exp, sqrt, log10
from scipy.stats import norm



NAME = "CLI vsota indikatorjev {}"

CATEGORY = "Kvizi za oceno/Kviz 12. teden/" + NAME.format('')
FILE_NAME = "vs_kviz_12_5_1"
CLOSE_TPL = "{{1:NUMERICAL:={}:{}}}"
INPUT_TPL = '<input type="text" class="answer" answer="{}" precision="{}">'
INPUT_RE = re.compile(r'<input type="text" class="answer" answer="([^"]*?)" precision="([^"]*?)">')

def format_odgovor(match):
    precision = float(match.group(2))
    answer = float(match.group(1))
    pfmt = "{{:.{}f}}".format(round(-log10(precision)))
    return " \\hfill \\odgovor[{1}]{{{0}}}".format(pfmt.format(answer), precision)

text = """<p>Naj bodo \\(X_i=Y_i + Z_i\\), kjer so 
    $$Y_i\\sim \\begin{{pmatrix}}0 & 1\\cr 0.4 & 0.6\\end{{pmatrix}},\\quad 
    Z_i\\sim \\begin{{pmatrix}}0 & 1\\cr {q} & {p}\\end{{pmatrix}}$$
    neodvisne slučajne spremenljivke. 
    Naj bo \\(X=X_1+X_2+\\ldots X_{{{n}}}\\). 
    </p>
   <ol>
    <li>Izračunaj: \\(E(X)=\\) {ans[0]} in \\(\\sigma(X)=\\){ans[1]}. </li>

    <li>Oceni verjetnost, da je \\( X < {z1} \\lor {z2} < X \\):&nbsp; {ans[2]} </li>
    
    <li>Največ koliko je lahko \\(z\\), da bo 
    verjetnost \\(P(X>z)\\), večja kot {p}? &nbsp; {ans[3]} </li>
   </ol>
    """

def resitev(n, a, b, p):
    q = 1 - p
    upanje = n*(0.6 + p)
    sigma = sqrt(n*(0.6*0.4 + p*q))
    ans = [upanje, sigma]
    z1 = round(upanje + a)
    z2 = round(upanje + b)

    ans.append(norm.cdf(z1, upanje, sigma) + 1 - norm.cdf(z2, upanje, sigma))
    z = norm.ppf(1-p, upanje, sigma)
    ans.append(z)
      
    precision = [0.001, 0.001, 0.001, 0.1]
    print(n, a, b, p, ans)
    ans_cloze = [CLOSE_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    ans_html = [INPUT_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    cloze = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, q=q, ans=ans_cloze)
    html = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, q=q, ans=ans_html)
    return cloze, html

problems = []
count = 1
a = -2
for n in [121, 400]:
    for b in [3, 4]:
        for p in [0.25, 0.3]:
            cloze, html = naloga(n, a, b, p)
            problems.append(cloze)

# change the code below only if you know what you are doing
# remove last question from the bank, since it is in tex
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

problems.pop()
questions = ""
count = 1
for cloze in problems:
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
    count += 1

# write tex file
tex = html.replace("<p>", "")
tex = tex.replace("</p>", "\n\n")
tex = tex.replace("&nbsp;", " ")
tex = tex.replace("<b>", "")
tex = tex.replace("</b>", "")
tex = tex.replace("<ol>", "\\begin{enumerate}")
tex = tex.replace("</ol>", "\\end{enumerate}")
tex = tex.replace("<li>", "\\item ")
tex = tex.replace("</li>", "")

tex = INPUT_RE.sub(format_odgovor, tex)
TEX_TEMPLATE = """
\\begin{{naloga}}
{0}
\\end{{naloga}}
"""
with open(FILE_NAME+".tex","w") as fp:
    print("Writing to file {}.tex".format(FILE_NAME))
    fp.write(TEX_TEMPLATE.format(tex))

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    code = fp.read()

link = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(bytes(code, 'utf-8')).decode())
else:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(code))

# write XML
xml = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=code, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(xml)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import base64
from math import log, exp, sqrt, log10
from scipy.stats import norm


NAME = "CLI produkt {}"

CATEGORY = "Kvizi za oceno/Kviz 12. teden/" + NAME.format('')
FILE_NAME = "vs_kviz_12_6_1"
CLOSE_TPL = "{{1:NUMERICAL:={}:{}}}"
INPUT_TPL = '<input type="text" class="answer" answer="{}" precision="{}">'
INPUT_RE = re.compile(r'<input type="text" class="answer" answer="([^"]*?)" precision="([^"]*?)">')

def format_odgovor(match):
    precision = float(match.group(2))
    answer = float(match.group(1))
    pfmt = "{{:.{}f}}".format(round(-log10(precision)))
    return " \\hfill \\odgovor[{1}]{{{0}}}".format(pfmt.format(answer), precision)

text = """<p>Naj bodo \\(X_i=Y_i\\cdot Z_i\\), kjer so 
    $$Y_i\\sim \\begin{{pmatrix}}0 & 1\\cr 0.1 & 0.9\\end{{pmatrix}},\\quad 
    Z_i\\sim \\begin{{pmatrix}}0 & 1\\cr {q} & {p}\\end{{pmatrix}}$$
    neodvisne slučajne spremenljivke. 
    Naj bo \\(X=X_1+X_2+\\ldots X_{{{n}}}\\). 
    </p>
   <ol>
    <li>Izračunaj: \\(E(X)=\\) {ans[0]} in \\(\\sigma(X)=\\){ans[1]}. </li>

    <li>Oceni verjetnost, da je \\( X < {z1} \\lor {z2} < X \\):&nbsp; {ans[2]} </li>
    
    <li>Največ koliko je lahko \\(z\\), da bo 
    verjetnost \\(P(X>z)\\), večja kot {p}? &nbsp; {ans[3]} </li>
   </ol>
    """

def resitev(n, a, b, p):
    q = 1 - p
    upanje = n*(0.9*p)
    sigma = sqrt(n*(0.9*p*(1 - 0.9*p)))
    ans = [upanje, sigma]
    z1 = round(upanje + a)
    z2 = round(upanje + b)

    ans.append(norm.cdf(z1, upanje, sigma) + 1 - norm.cdf(z2, upanje, sigma))
    z = norm.ppf(1-p, upanje, sigma)
    ans.append(z)
      
    precision = [0.001, 0.001, 0.001, 0.1]
    print(n, a, b, p, ans)
    ans_cloze = [CLOSE_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    ans_html = [INPUT_TPL.format(a, prec) for a, prec in zip(ans, precision)]
    cloze = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, q=q, ans=ans_cloze)
    html = text.format(n=n, a=a, b=b, z1=z1, z2=z2, p=p, q=q, ans=ans_html)
    return cloze, html

problems = []
count = 1
a = -2
for n in [121, 400]:
    for b in [3, 4]:
        for p in [0.25, 0.3]:
            cloze, html = naloga(n, a, b, p)
            problems.append(cloze)

# change the code below only if you know what you are doing
# remove last question from the bank, since it is in tex
QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

problems.pop()
questions = ""
count = 1
for cloze in problems:
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
    count += 1

# write tex file
tex = html.replace("<p>", "")
tex = tex.replace("</p>", "\n\n")
tex = tex.replace("&nbsp;", " ")
tex = tex.replace("<b>", "")
tex = tex.replace("</b>", "")
tex = tex.replace("<ol>", "\\begin{enumerate}")
tex = tex.replace("</ol>", "\\end{enumerate}")
tex = tex.replace("<li>", "\\item ")
tex = tex.replace("</li>", "")

tex = INPUT_RE.sub(format_odgovor, tex)
TEX_TEMPLATE = """
\\begin{{naloga}}
{0}
\\end{{naloga}}
"""
with open(FILE_NAME+".tex","w") as fp:
    print("Writing to file {}.tex".format(FILE_NAME))
    fp.write(TEX_TEMPLATE.format(tex))

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    code = fp.read()

link = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(bytes(code, 'utf-8')).decode())
else:
    code = link.format(name=sys.argv[0], base64 = base64.b64encode(code))

# write XML
xml = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=code, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(xml)
QUIZ.append(QuestionSet(text, parametri, resitev18, name="kroglice"))

with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))

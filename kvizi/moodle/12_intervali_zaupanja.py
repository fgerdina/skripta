#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 11. kviz (intervali zaupanja)/1. skupina"
FILE_NAME = "vs_kviz_11_1_1"

def naloga(povp,beta,sigma):
    text = """<p>Srčni utrip v neki populaciji je porazdeljen normalno $N(\\mu,{sigma})$.
    Kvantil katere od naštetih porazdelitev potrebujemo, da lahko iz vzorca velikosti 400 izračunamo interval zaupanja za $\\mu$ stopnje zaupanja 0.95?
    {{1:MC:Studentova z 399 prostostnimi stopnjami~Studentova z 400 prostostnimi stopnjami~hi-kvadrat z 399 prostostnimi stopnjami~hi-kvadrat z 400 prostostnimi stopnjami~=standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>
    
    <p>Na vzorcu 400 ljudi smo izmerili srčni utrip in dobili povprečje {povp}. Koliko je zgornja meja intervala zaupanja za $\\mu$ stopnje zaupanja {beta}?&nbsp;{ans[0]} </p>

    <p>Na nekem drugem vzorcu smo prav tako izmerili srčni utrip in dobili povprečje {povp2}. Interval zaupanja za $\\mu$ stopnje zaupanja {beta} je bil krajši kot 0.2. Najmanj
        koliko ljudi je bilo v tem vzorcu?&nbsp;{ans[1]} </p>

    """

    n=400
    z=norm.isf(1-(1+beta)/2)

    Delta=sigma*z/sqrt(n)

    zgMeja=povp+Delta
    velVz=ceil((2*sigma*z/0.2)**2)

    print(povp,beta,sigma,zgMeja,velVz)
    
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(zgMeja),"{{1:NUMERICAL:={}:5}}".format(velVz)]
    cloze = text.format(sigma=sigma,povp=povp,povp2=povp+1,beta=beta,gamma=(1+beta)/2,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for povp in [60,65,70]:
    for beta in [0.95,0.99]:
        for sigma in [4]:
                text = naloga(povp,beta,sigma)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm,t
from math import sqrt
from math import ceil

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 11. kviz (intervali zaupanja)/1. skupina"
FILE_NAME = "vs_kviz_11_1_2"

def naloga(povp,beta,sigma):
    text = """<p>Srčni utrip v neki populaciji je porazdeljen normalno $N(\\mu,\\sigma)$.
    Kvantil katere od naštetih porazdelitev potrebujemo, da lahko iz vzorca velikosti 400 izračunamo interval zaupanja za $\\mu$ stopnje zaupanja 0.95?
    {{1:MC:=Studentova z 399 prostostnimi stopnjami~Studentova z 400 prostostnimi stopnjami~hi-kvadrat z 399 prostostnimi stopnjami~hi-kvadrat z 400 prostostnimi stopnjami~standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>
    
    <p>Na vzorcu 400 ljudi smo izmerili srčni utrip in dobili povprečje {povp} ter vzorčni odklon {s}. Koliko je zgornja meja intervala zaupanja za $\\mu$ stopnje zaupanja {beta}?&nbsp;{ans[0]} </p>

    <p>Na neki drugi populaciji, kjer vemo, da je $\\sigma={sigma}$, smo na vzorcu ljudi prav tako izmerili srčni utrip in dobili povprečje {povp2}. Interval zaupanja za $\\mu$ stopnje zaupanja {beta} je bil krajši kot 0.25. Najmanj
        koliko ljudi je bilo v tem vzorcu?&nbsp;{ans[1]} </p>

    """

    n=400
    z=t.isf(1-(1+beta)/2,n-1)

    Delta=s*z/sqrt(n)
    zgMeja=povp+Delta

    z=norm.isf(1-(1+beta)/2)    
    velVz=ceil((2*sigma*z/0.25)**2)

    print(povp,beta,sigma,zgMeja,velVz)
    
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(zgMeja),"{{1:NUMERICAL:={}:5}}".format(velVz)]
    cloze = text.format(sigma=sigma,povp=povp,povp2=povp+1,beta=beta,gamma=(1+beta)/2,ans=ans,s=s)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for povp in [60,65,70]:
    for beta in [0.95,0.99]:
        for s in [4]:
            for sigma in [5]:
                text = naloga(povp,beta,sigma)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/1. skupina"
FILE_NAME = "ovs_kviz_12_1"

def naloga(povp,beta,sigma):
    text = """ <p>Naj bosta \\(\\beta={beta}\\) in \(\\gamma={gamma}\\).
        Kvantile \\(z_\\beta\\), \\(\\chi^2_\\gamma(12)\\), \\(t_\\gamma(8)\\), \\(t_\\beta(\infty)\\) uredi po velikosti:
        {{1:MC:\\(\\chi^2_\\gamma(12)\\)>\\(z_\\beta\\)>\\(t_\\beta(\infty)\\)>\\(t_\\gamma(8)\\)~\\(z_\\beta\\)=
            \\(t_\\beta(\infty)\\)> \\(\\chi^2_\\gamma(12)\\)>\\(t_\\gamma(8)\\)~\\(\\chi^2_\\gamma(12)\\)>
            \\(t_\\gamma(8)\\)>\\(z_\\beta\\)>\\(t_\\beta(\infty)\\)~\\(\\chi^2_\\gamma(12)\\)>
            \\(z_\\beta\\)=\\(t_\\beta(\infty)\\)>\\(t_\\gamma(8)\\)~\\(t_\\gamma(8)\\)>
            \\(\\chi^2_\\gamma(12)\\)>\\(z_\\beta\\)>\\(t_\\beta(\infty)\\)~=\\(\\chi^2_\\gamma(12)\\)>
            \\(t_\\gamma(8)\\)>\\(z_\\beta\\)=\\(t_\\beta(\infty)\\)}}</p>

    <p>Srčni utrip v neki populaciji je porazdeljen normalno \(N(\mu,{sigma})\).</p>

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da lahko iz vzorca izračunamo interval zaupanja za \(\mu\) stopnje zaupanja 0.95?
    {{1:MC:Studentova z 399 prostostnimi stopnjami~Studentova z 400 prostostnimi stopnjami~hi-kvadrat z 399 prostostnimi stopnjami~hi-kvadrat z 400 prostostnimi stopnjami~=standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>
    
    <p>Na vzorcu 300 ljudi smo izmerili srčni utrip in dobili povprečje {povp}. Koliko je zgornja meja intervala zaupanja za \(\mu\) stopnje zaupanja {beta}?&nbsp;{ans[0]} </p>

    <p>Na nekem drugem vzorcu smo prav tako izmerili srčni utrip in dobili povprečje {povp2}. Interval zaupanja za \(\mu\) stopnje zaupanja {beta} je bil krajši kot 0.3. Najmanj
        koliko ljudi je bilo v tem vzorcu?&nbsp;{ans[1]} </p>

    """

    n=300
    z=norm.isf(1-(1+beta)/2)

    Delta=sigma*z/sqrt(n)

    zgMeja=povp+Delta
    velVz=ceil((2*sigma*z/0.3)**2)

    print(zgMeja)
    print(velVz)
    
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(zgMeja),"{{1:NUMERICAL:={}:5}}".format(velVz)]
    cloze = text.format(sigma=sigma,povp=povp,povp2=povp+1,beta=beta,gamma=(1+beta)/2,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for povp in [60,65,70]:
    for beta in [0.95,0.99]:
        for sigma in [4]:
                text = naloga(povp,beta,sigma)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (intervali zaupanja)/2. skupina"
FILE_NAME = "vs_kviz_12_2_1"

def naloga(k,beta):
    text = """V nekem velikem mestu so delali anketo o povprečni starosti obiskovalcev nakupovalnega centra (pri tem je za majhnega otroka odgovoril starš).
    Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Koliko je povprečna starost anketiranca?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je povprečna starost obiskovalce nakupovalnega centra porazdeljena normalno \\(N(\\mu,\\sigma)\\).

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.95 povprečne starosti?
    {{1:MC:=Studentova z {st_stop_prost} prostostnimi stopnjami~Studentova z {st_stop_prost_2} prostostnimi stopnjami~hi-kvadrat z {st_stop_prost} prostostnimi stopnjami~hi-kvadrat
        z {st_stop_prost_2} prostostnimi stopnjami~standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je dolžina intervala zaupanja stopnje zaupanja {beta} za povprečno starost obiskovalcev nakupovalnega centra?&nbsp;{ans[2]} </p>

    """
    
    odgovori=[]
    for i in range(15):
        odgovori.append(randint(1,10))
    for i in range(20):
        odgovori.append(randint(10,17))
    for i in range(64+k):
        odgovori.append(randint(18,70))
    for i in range(15):
        odgovori.append(randint(71,87))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)

    povp=1.0*sum(odgovori)/len(odgovori)

    odklon=0
    for i in range(n):
        odklon+=(odgovori[i]-povp)**2
    odklon=sqrt(odklon/(n-1))
    
    tt=t.isf(1-(1+beta)/2,n-1)
    Delta=odklon*tt/sqrt(n)

    print(povp)
    print(odklon)
    print(2*Delta)
    
    ans=[povp,odklon,2*Delta]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,beta=beta,st_stop_prost=n-1,st_stop_prost_2=n,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for k in [10,20,30]:
            for beta in [0.93,0.97]:
                    text = naloga(k,beta)
                    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (intervali zaupanja)/2. skupina"
FILE_NAME = "vs_kviz_12_2_2"

def naloga(k,beta):
    text = """V nekem velikem mestu so delali anketo o povprečni starosti obiskovalcev nakupovalnega centra (pri tem je za majhnega otroka odgovoril starš).
    Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Koliko je povprečna starost anketiranca?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je povprečna starost obiskovalce nakupovalnega centra porazdeljena normalno \\(N(\\mu,23)\\).

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.95 povprečne starosti?
    {{1:MC:Studentova z {st_stop_prost} prostostnimi stopnjami~Studentova z {st_stop_prost_2} prostostnimi stopnjami~hi-kvadrat z {st_stop_prost} prostostnimi stopnjami~hi-kvadrat
        z {st_stop_prost_2} prostostnimi stopnjami~=standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je dolžina intervala zaupanja stopnje zaupanja {beta} za povprečno starost obiskovalcev nakupovalnega centra?&nbsp;{ans[2]} </p>

    """
    
    odgovori=[]
    for i in range(15):
        odgovori.append(randint(1,10))
    for i in range(20):
        odgovori.append(randint(10,17))
    for i in range(64+k):
        odgovori.append(randint(18,70))
    for i in range(15):
        odgovori.append(randint(71,87))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)

    povp=1.0*sum(odgovori)/len(odgovori)

    odklon=0
    for i in range(n):
        odklon+=(odgovori[i]-povp)**2
    odklon=sqrt(odklon/(n-1))
    
    tt=norm.isf(1-(1+beta)/2)
    Delta=23*tt/sqrt(n)

    print(povp)
    print(odklon)
    print(2*Delta)
    
    ans=[povp,odklon,2*Delta]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,beta=beta,st_stop_prost=n-1,st_stop_prost_2=n,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for k in [10,20,30]:
            for beta in [0.93,0.97]:
                    text = naloga(k,beta)
                    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/2. skupina"
FILE_NAME = "ovs_kviz_12_2"

def naloga(beta,beta2,crka):
    text = """Dan imam naslednji izsek iz časopisnega članka:<p/>

    <pre> {besedilo}</pre>

    <p>Koliko je delež pojavitev črke <b>{crka}</b> (male in velike) v izseku, pri čemer upoštevamo vse znake razen presledkov?&nbsp;{ans[0]} </p>

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da lahko iz izseka izračunamo interval zaupanja za delež pojavitve črke {crka} stopnje zaupanja 0.95?
    {{1:MC:Studentova z 399 prostostnimi stopnjami~Studentova z 400 prostostnimi stopnjami~hi-kvadrat z 399 prostostnimi stopnjami~hi-kvadrat
        z 400 prostostnimi stopnjami~=standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je spodnja meja intervala zaupanja stopnje zaupanja {beta} za delež pojavitev črke {crka} v časopisu?&nbsp;{ans[1]} </p>

    <p>Denimo, da smo na nekem drugem izseku izračunali isti delež pojavitev črke <b>{crka}</b> v izseku, interval zaupanja
         stopnje zaupanja {beta2} za delež pojavitev te črke v časopisu pa je bil krajši od 0.03. Najmanj koliko znakov (neupoštevaje presledkov) je imel ta izsek?{ans[2]} </p>

    """

    s=[]
    s.append("Britanski alpinist Tim Mosedale opozarja, da \"toksična mešanica\" neizkušenih pohodnikov na Everestu ")
    s.append("povzroča velike težave, saj se na najvišjo goro sveta vzpenja vse več turistov, ki povzročajo \"prometne zamaške\" ")
    s.append("na poledenelih poteh. Mosedale se je po prilagoditvi v višjem predelu spuščal po zahtevnem ledenem slapu Khumbu, ")
    s.append("ko je naletel na večjo skupino neizkušenih plezalcev, ki so imeli velike težave s svojo opremo. ")
    s.append("Eden je imel celo dereze obrnjene narobe, drugi si jih ni znal namestiti, ker tega še nikdar ni počel, ")
    s.append("tretji je imel čelado samo pritrjeno na nahrbtnik. ")
    s.append("\"Tako velike skupine, skupaj z alpinisti, ki skušajo 8848 metrov visoki vrh osvojiti brez dodatnega kisika,")
    s.append("letos na Everestu povzročajo \"toksično mešanico\", je na svojem Facebooku zapisal Mosedale, ki je Everest ")
    s.append("osvojil že petkrat. Ljudje, ki se ne držijo nekaterih povsem osnovnih varnostnih načel, ogrožajo sebe, ")
    s.append("svoje osebje in vse okoli njih. Everest je že tako dovolj nevaren, ne potrebuje še popolnih novincev, ")
    s.append("na katere pazijo neizkušene šerpe iz neprofesionalnih ekip.") 
    besedilo=''.join(s)
    print(besedilo)
    print(len(besedilo))

    n=len(besedilo)-besedilo.count('č')-besedilo.count('ž')-besedilo.count('š')
    print(n)
    n=n-besedilo.count(' ')
    print(n)
    k=besedilo.count(crka)
    if crka=='a':
        k+=besedilo.count('A')
    if crka=='e':
        k+=besedilo.count('E')
    if crka=='i':
        k+=besedilo.count('I')
    print(n,k)
    
    delez=1.0*k/n
    sigma=sqrt(delez*(1-delez))
    z=norm.isf(1-(1+beta)/2)
    Delta=sigma*z/sqrt(n)

    spMeja=delez-Delta

    z2=norm.isf(1-(1+beta2)/2)
    velikost=ceil((2*sigma*z2/0.03)**2)
    
    print(crka,delez)
    print(beta,spMeja)
    print(beta2,velikost)
    
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(delez), "{{1:NUMERICAL:={}:0.001}}".format(spMeja),"{{1:NUMERICAL:={}:5}}".format(velikost)]
    cloze = text.format(besedilo=besedilo,beta=beta,beta2=beta2,crka=crka,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for beta in [0.95,0.975]:
    for beta2 in [0.99]:
        for crka in ['a','e','i']:
                text = naloga(beta,beta2,crka)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import chi2
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (intervali zaupanja)/3. skupina"
FILE_NAME = "vs_kviz_11_3_1"

def naloga(k,beta):
    text = """V nekem velikem mestu so med lastniki avtomobilov delali anketo o povprečni starosti avtomobila.
    Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Koliko je povprečna starost avtomobila?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je starost avtomobila porazdeljena normalno \\(N(\\mu,\\sigma)\\).

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.95 za standardni odklon?
    {{1:MC:Studentova z {st_stop_prost} prostostnimi stopnjami~Studentova z {st_stop_prost_2} prostostnimi stopnjami~=hi-kvadrat z {st_stop_prost} prostostnimi stopnjami~hi-kvadrat
        z {st_stop_prost_2} prostostnimi stopnjami~standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je dolžina intervala zaupanja stopnje zaupanja {beta} za standardni odklon starosti avtomobila?&nbsp;{ans[2]} </p>

    """
    
    odgovori=[]
    for i in range(30):
        odgovori.append(randint(1,3))
    for i in range(50):
        odgovori.append(randint(4,8))
    for i in range(40+k):
        odgovori.append(randint(9,15))
    for i in range(10):
        odgovori.append(randint(16,30))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)

    povp=1.0*sum(odgovori)/len(odgovori)

    odklon=0
    for i in range(n):
        odklon+=(odgovori[i]-povp)**2
    odklon=sqrt(odklon/(n-1))
    
    c1=chi2.isf(1-(1-beta)/2,n-1)
    c2=chi2.isf(1-(1+beta)/2,n-1)
    Delta=odklon*(sqrt((n-1)/c1)-sqrt((n-1)/c2))

    print(povp)
    print(odklon)
    print(Delta)
    
    ans=[povp,odklon,Delta]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,beta=beta,st_stop_prost=n-1,st_stop_prost_2=n,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for k in [10]:
            for beta in [0.92,0.96,0.98]:
                    text = naloga(k,beta)
                    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/8. skupina"
FILE_NAME = "vs_kviz_11_3_2"

def naloga(k,beta,beta2):
    text = """V nekem velikem mestu so med lastniki avtomobilov delali anketo o letniku njihovega avtomobila.
    Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Koliko je povprečni letnik avtomobila?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je letnik avtomobila v mestu porazdeljen normalno \\(N(\\mu,2.3)\\).</p>

    <p>Koliko je spodnja meja intervala zaupanja stopnje zaupanja {beta} za letnik avtomobila?&nbsp;{ans[2]} </p>

    <p>V neki drugi anketi med 400 lastniki avtomobilov o letniku njihovega avtomobila je bil
    interval zaupanja stopnje zaupanja \\(\\beta\\) dolg {velikost}. Koliko je \\(\\beta\\)?&nbsp; {ans[3]}</p>
    """
    
    odgovori=[]
    for i in range(30):
        odgovori.append(randint(2016,2018))
    for i in range(50):
        odgovori.append(randint(2011,2015))
    for i in range(40+k):
        odgovori.append(randint(2003,2010))
    for i in range(10):
        odgovori.append(randint(1988,2002))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)

    povp=1.0*sum(odgovori)/len(odgovori)

    odklon=0
    for i in range(n):
        odklon+=(odgovori[i]-povp)**2
    odklon=sqrt(odklon/(n-1))

    z=norm.isf(1-(1+beta)/2)
    Delta=2.3*z/sqrt(n)
    spMeja=povp-Delta

    z2=norm.isf(1-(1+beta2)/2)
    Delta=2.3*z2/sqrt(400)
    velikost=round(2*Delta,5)

    print(povp)
    print(odklon)
    print(spMeja)
    print(beta2)
    
    ans=[povp,odklon,spMeja,beta2]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,beta=beta,velikost=velikost,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for k in [10]:
    for beta2 in [0.93,0.97,0.91]:
        text = naloga(k,beta,beta2)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (intervali zaupanja)/3. skupina"
FILE_NAME = "vs_kviz_11_3_3"

def naloga():
    text = """
    Zbrali smo podatke o dosežku vzorca učencev na nacionalnem testu iz fizike. V naslednjem seznamu so zapisane frekvence posameznih rezultatov, pri čemer
    na \\(i\\)-tem mestu stoji frekvenca učencev, ki so dosegli \\(i-1\\) procentov:

    <pre>{frekvence}</pre>

    <p>Koliko je povprečno število doseženih procentov?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon števila doseženih procentov?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je število doseženih procentov učenca porazdeljeno normalno \\(N(\\mu,\\sigma)\\).

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.95 za \\(\\mu\\)?
    {{1:MC:=Studentova z {st_stop_prost} prostostnimi stopnjami~Studentova z {st_stop_prost_2} prostostnimi stopnjami~hi-kvadrat z {st_stop_prost} prostostnimi stopnjami~hi-kvadrat
        z {st_stop_prost_2} prostostnimi stopnjami~standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je zgornja meja intervala zaupanja stopnje zaupanja {beta} za \\(\\mu\\)?&nbsp;{ans[2]} </p>

    """
    
    frekvence=[]
    for i in range(30):
        frekvence.append(randint(0,2))
    for i in range(40):
        frekvence.append(randint(0,4))
    for i in range(20):
        frekvence.append(randint(0,3))
    for i in range(11):
        frekvence.append(randint(0,2))
    print(frekvence)
    
    n=sum(frekvence)
    m=len(frekvence)

    povp=0
    for i in range(m):
        povp+=frekvence[i]*i
    povp=1.0*povp/n

    odklon=0
    for i in range(m):
        odklon+=frekvence[i]*(i-povp)**2
    odklon=sqrt(odklon/(n-1))
    
    tt=t.isf(1-(1+beta)/2,n-1)
    Delta=odklon*tt/sqrt(n)
    zgMeja=povp+Delta
    
    print(povp)
    print(odklon)
    print(zgMeja)
    
    ans=[povp,odklon,zgMeja]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(frekvence=frekvence,beta=beta,st_stop_prost=n-1,st_stop_prost_2=n,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
while count<7:
    text = naloga()
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil
from random import randint

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/3. skupina"
FILE_NAME = "ovs_kviz_12_3"

def naloga(n,tip_avtomobila,tip_avtomobila_mesto,beta,p):
    text = """V neki državi so delali anketo o tipih avtomobilov, ki jih uporabljajo prebivalci starejši od 18 let. Možni odgovori so bili
    'limuzina', 'kupe', 'kabriolet', 'karavan', 'enoprostorec', 'nimam avtomobila'. Odgovori so bili naslednji:<p/>

    <pre> {odgovori}</pre>

    <p>Kolikšen delež anketirancev uporablja avto {tip_avtomobila}?&nbsp;{ans[0]} </p>

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.9 za delež pojavitve tipa {tip_avtomobila} v državi?
    {{1:MC:Studentova z 399 prostostnimi stopnjami~Studentova z 400 prostostnimi stopnjami~hi-kvadrat z 399 prostostnimi stopnjami~hi-kvadrat
        z 400 prostostnimi stopnjami~=standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je zgornja meja intervala zaupanja stopnje zaupanja {beta} za delež prebivalcev države starjših od 18 let, ki uporabljajo {tip_avtomobila}?&nbsp;{ans[1]} </p>

    <p>Denimo, da so v nekem mestu za vse prebivalce starejše od 18 let zbrali podatke o tipu avtomobila, ki ga uporabljajo. Delež za tip {tip_avtomobila_mesto} je bil {p}.
        Na podlagi tega so izračunali interval zaupanja stopnje zaupanja {beta} za delež tega tipa v celotni državi in
        dobili interval krajši od 0.04. Najmanj koliko prebivalcev starejših od 18 let ima to mesto?{ans[2]} </p>

    """
    
    moznosti=['limuzina', 'kupe', 'kabriolet', 'karavan', 'enoprostorec', 'nimam avtomobila']
    odgovori=[]
    for i in range(n):
        odgovori.append(moznosti[randint(0,5)])
    print(odgovori)
        
    k=odgovori.count(tip_avtomobila)
        
    delez=1.0*k/n
    sigma=sqrt(delez*(1-delez))
    z=norm.isf(1-(1+beta)/2)
    Delta=sigma*z/sqrt(n)

    zgMeja=delez+Delta

    z2=norm.isf(1-(1+beta)/2)
    velikost=ceil((2*sqrt(p*(1-p))*z2/0.04)**2)
    
    print(tip_avtomobila,delez)
    print(tip_avtomobila,zgMeja)
    print(p,tip_avtomobila_mesto,beta,velikost)
    
    ans=[delez,zgMeja,velikost]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(delez), "{{1:NUMERICAL:={}:0.001}}".format(zgMeja),"{{1:NUMERICAL:={}:5}}".format(velikost)]
    cloze = text.format(odgovori=odgovori,beta=beta,tip_avtomobila=tip_avtomobila,tip_avtomobila_mesto=tip_avtomobila_mesto,p=p,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for n in [97]:
    for tip_avtomobila in ['kupe','kabriolet']:
        for tip_avtomobila_mesto in ['karavan','enoprostorec']:
            for beta in [0.95]:
                for p in [0.16,0.18]:
                    text = naloga(n,tip_avtomobila,tip_avtomobila_mesto,beta,p)
                    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import random

NAME = "Cenilke {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (cenilke)/4. skupina"
FILE_NAME = "vs_kviz_11_4_1"

def naloga(beta,beta2):

    upanje=1.0*beta2/(beta2-1)

    n = 50
    vzorec=[]
    for i in range(n):
        a=round((1.0/(1-random()))**(1.0/beta),4)
        vzorec.append(a)

    moment=sum(vzorec)/len(vzorec)
    cenilka=moment/(moment-1)

    ans=[upanje,moment,cenilka]
    print(beta2,beta,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    
    
    text = """
    Naj ima slučajna spremenljivka $X$ gostoto
    $p_X(x)=\\left\{\\begin{array}{cc}\\frac{\\beta}{x^{\\beta+1}} & x>1\\\\ 0 & \\text{sicer}\\end{array}\\right.$, kjer je $\\beta>1$ neznan parameter.

    <p>Za $\\beta=%(beta2).1f$ izračunaj $E(X)$.&nbsp;%(ans[0])s</p>

    <p>Iz populacije smo izbrali vzorec in dobili naslednje vrednosti:</p>

    <pre>%(vzorec)s</pre>

    <p>Iz vzorca oceni upanje spremenljivke $X$.&nbsp;%(ans[1])s</p>

    <p>Na podlagi vzorca po metodi momentov oceni vrednost parametra $\\beta$.&nbsp;%(ans[2])s</p>
    
    """%{"beta2": beta2, "beta": beta, "ans[0]": ans[0], "ans[1]": ans[1], "vzorec": vzorec, "ans[2]": ans[2]}

    
    return text


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for beta2 in [1.5,2.5]:
    for beta in [2,3,4]:
        text = naloga(beta,beta2)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import log
from math import ceil
from random import random

NAME = "Cenilke {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (cenilke)/4. skupina"
FILE_NAME = "vs_kviz_11_4_2"

def naloga(beta,beta2):

    n = 50
    vzorec=[]
    for i in range(n):
        a=round((1.0/(1-random()))**(1.0/beta),4)
        vzorec.append(a)

    L=0
    cenilka=0
    for i in range(n):
        cenilka=cenilka+log(vzorec[i])
        L=L+log(beta2/(vzorec[i])**(beta2+1))

    cenilka=n/cenilka

    upan=sum(vzorec)/len(vzorec)
    
    ans=[L,upan,cenilka]
    print(beta2,beta,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    
    
    text = """
    Naj ima slučajna spremenljivka $X$ gostoto
    $p_X(x)=\\left\{\\begin{array}{cc}\\frac{\\beta}{x^{\\beta+1}} & x>1\\\\ 0 & \\text{sicer}\\end{array}\\right.$, kjer je $\\beta>1$ neznan parameter.

    <p>Iz populacije smo izbrali vzorec in dobili naslednje vrednosti:</p>

    <pre>%(vzorec)s</pre>

    <p>Na podlagi vzorca oceni upanje spremenljivke $X$.&nbsp;%(ans[1])s</p>

    <p>Za $\\beta=%(beta2).1f$ izračunaj vrednost funkcije $\\ln{L(\\beta)}$, ki nastopa v metodi največjega verjetja za ocenjevanje parametra $\\beta$.&nbsp;%(ans[0])s</p>
    
    <p>Na podlagi vzorca ocenite vrednost parametra $\\beta$ po metodi največjega verjetja.&nbsp;%(ans[2])s</p>
    
    """%{"beta2": beta2, "beta": beta, "ans[0]": ans[0], "ans[1]": ans[1], "vzorec": vzorec, "ans[2]": ans[2]}

    
    return text


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for beta2 in [1.5,2.5]:
    for beta in [2,3,4]:
        text = naloga(beta,beta2)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/4. skupina"
FILE_NAME = "ovs_kviz_12_4"

def naloga(k,beta):
    text = """V nekem velikem mestu so delali anketo o povprečni starosti obiskovalcev nakupovalnega centra (pri tem je za majhnega otroka odgovoril starš).
    Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Koliko je povprečna starost anketiranca?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je povprečna starost obiskovalce nakupovalnega centra porazdeljena normalno \\(N(\\mu,\\sigma)\\).

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.95 povprečne starosti?
    {{1:MC:=Studentova z {st_stop_prost} prostostnimi stopnjami~Studentova z {st_stop_prost_2} prostostnimi stopnjami~hi-kvadrat z {st_stop_prost} prostostnimi stopnjami~hi-kvadrat
        z {st_stop_prost_2} prostostnimi stopnjami~standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je dolžina intervala zaupanja stopnje zaupanja {beta} za povprečno starost obiskovalcev nakupovalnega centra?&nbsp;{ans[2]} </p>

    """
    
    odgovori=[]
    for i in range(15):
        odgovori.append(randint(1,10))
    for i in range(20):
        odgovori.append(randint(10,17))
    for i in range(64+k):
        odgovori.append(randint(18,70))
    for i in range(15):
        odgovori.append(randint(71,87))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)

    povp=1.0*sum(odgovori)/len(odgovori)

    odklon=0
    for i in range(n):
        odklon+=(odgovori[i]-povp)**2
    odklon=sqrt(odklon/(n-1))
    
    tt=t.isf(1-(1+beta)/2,n-1)
    Delta=odklon*tt/sqrt(n)

    print(povp)
    print(odklon)
    print(2*Delta)
    
    ans=[povp,odklon,2*Delta]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,beta=beta,st_stop_prost=n-1,st_stop_prost_2=n,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for k in [10,20,30]:
            for beta in [0.95,0.99]:
                    text = naloga(k,beta)
                    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import random

NAME = "Cenilke {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (cenilke)/5. skupina"
FILE_NAME = "vs_kviz_11_5_1"

def naloga(alpha,beta):

    n = 50
    vzorec=[]
    for i in range(n):
        a=round(alpha+random()*(beta-alpha),4)
        vzorec.append(a)

    #print(vzorec)
    
    vzorec_kvad=[]
    for i in range(n):
        vzorec_kvad.append(vzorec[i]**2)
    
    prvi_mom=sum(vzorec)/len(vzorec)
    drugi_mom=sum(vzorec_kvad)/len(vzorec_kvad)

    cen_zgor=(((drugi_mom-prvi_mom**2)*12)**(0.5)+2*prvi_mom)/2
    cen_spod=2*prvi_mom-cen_zgor
    

    ans=[prvi_mom,drugi_mom,cen_spod,cen_zgor]
    print(alpha,beta,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    
    
    text = """
    <p>Naj bo slučajna spremenljivka $X\sim E[\\alpha,\\beta]$ porazdeljena enakomerno zvezno na intervalu $[\\alpha,\\beta]$, kjer
    sta $\\alpha$ in $\\beta$ neznana parametra. Na vzorcu smo izmerili naslednje vrednosti:</p>

    <pre>%(vzorec)s</pre>

    <p>Iz vzorca oceni prvi %(ans[0])s in drugi moment %(ans[1])s spremenljivke $X$.</p>

    <p>Na podlagi vzorca po metodi momentov oceni vrednost parametrov $\\alpha$  %(ans[2])s in $\\beta$  %(ans[3])s.</p>
    
    """%{"ans[0]": ans[0], "ans[1]": ans[1], "vzorec": vzorec, "ans[2]": ans[2], "ans[3]": ans[3]}

    
    return text


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for k in range(5):
    text = naloga(1.5,2)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import random

NAME = "Cenilke {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (cenilke)/5. skupina"
FILE_NAME = "vs_kviz_11_5_2"

def naloga(alpha,beta):

    n = 50
    vzorec=[]
    for i in range(n):
        a=round(alpha+random()*(beta-alpha),4)
        vzorec.append(a)

    print(vzorec)
    
    vzorec_kvad=[]
    for i in range(n):
        vzorec_kvad.append(vzorec[i]**2)
    
    prvi_mom=sum(vzorec)/len(vzorec)
    drugi_mom=sum(vzorec_kvad)/len(vzorec_kvad)

    cen_zgor=max(vzorec)
    cen_spod=min(vzorec)
    

    ans=[prvi_mom,drugi_mom,cen_spod,cen_zgor]
    print(alpha,beta,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    
    
    text = """
    <p>Naj bo slučajna spremenljivka $X\sim E[\\alpha,\\beta]$ porazdeljena enakomerno zvezno na intervalu $[\\alpha,\\beta]$, kjer
    sta $\\alpha$ in $\\beta$ neznana parametra. Na vzorcu smo izmerili naslednje vrednosti:</p>

    <pre>%(vzorec)s</pre>

    <p>Iz vzorca oceni prvi %(ans[0])s in drugi moment %(ans[1])s spremenljivke $X$.</p>

    <p>Na podlagi vzorca po metodi največjega verjetja oceni vrednost parametrov $\\alpha$  %(ans[2])s in $\\beta$  %(ans[3])s. (Pri tem ekstrema funkcije $L(\\alpha,\\beta)$
    ne iščite z odvajanjem, temveč s premislekom o možnih vrednostih parametrov $\\alpha,\\beta$ na podlagi vzorca.)</p>
    
    """%{"ans[0]": ans[0], "ans[1]": ans[1], "vzorec": vzorec, "ans[2]": ans[2], "ans[3]": ans[3]}

    
    return text


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for k in range(5):
    text = naloga(1.5,2)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import chi2
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/5. skupina"
FILE_NAME = "ovs_kviz_12_5"

def naloga(k,beta):
    text = """V nekem velikem mestu so med lastniki avtomobilov delali anketo o povprečni starosti avtomobila.
    Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Koliko je povprečna starost avtomobila?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je starost avtomobila porazdeljena normalno \\(N(\\mu,\\sigma)\\).

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.95 za standardni odklon?
    {{1:MC:Studentova z {st_stop_prost} prostostnimi stopnjami~Studentova z {st_stop_prost_2} prostostnimi stopnjami~=hi-kvadrat z {st_stop_prost} prostostnimi stopnjami~hi-kvadrat
        z {st_stop_prost_2} prostostnimi stopnjami~standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je dolžina intervala zaupanja stopnje zaupanja {beta} za standardni odklon starosti avtomobila?&nbsp;{ans[2]} </p>

    """
    
    odgovori=[]
    for i in range(30):
        odgovori.append(randint(1,3))
    for i in range(50):
        odgovori.append(randint(4,8))
    for i in range(40+k):
        odgovori.append(randint(9,15))
    for i in range(10):
        odgovori.append(randint(16,30))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)

    povp=1.0*sum(odgovori)/len(odgovori)

    odklon=0
    for i in range(n):
        odklon+=(odgovori[i]-povp)**2
    odklon=sqrt(odklon/(n-1))
    
    c1=chi2.isf(1-(1-beta)/2,n-1)
    c2=chi2.isf(1-(1+beta)/2,n-1)
    Delta=odklon*(sqrt((n-1)/c1)-sqrt((n-1)/c2))

    print(povp)
    print(odklon)
    print(Delta)
    
    ans=[povp,odklon,Delta]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,beta=beta,st_stop_prost=n-1,st_stop_prost_2=n,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for k in [5,10,15]:
            for beta in [0.95,0.99]:
                    text = naloga(k,beta)
                    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import random

NAME = "Cenilke {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (cenilke)/6. skupina"
FILE_NAME = "vs_kviz_11_6_1"

def naloga(alpha,beta):

    cen_spod=0
    cen_zgor=0
    while cen_spod>alpha or cen_zgor<beta or cen_spod==alpha and cen_zgor==beta:
        n = 50
        vzorec=[]
        for i in range(n):
            vzorec.append(int(ceil(random()*(beta-alpha+1)-1))+alpha)

        print(vzorec)
        
        vzorec_kvad=[]
        for i in range(n):
            vzorec_kvad.append(vzorec[i]**2)
        
        prvi_mom=1.0*sum(vzorec)/len(vzorec)
        drugi_mom=1.0*sum(vzorec_kvad)/len(vzorec_kvad)

        cen_zgor=int((((drugi_mom-prvi_mom**2)*12+1)**(0.5)-1+2*prvi_mom)/2)
        print(cen_zgor)
        if (((drugi_mom-prvi_mom**2)*12+1)**(0.5)-1+2*prvi_mom)/2-cen_zgor>0.5:
            cen_zgor=cen_zgor+1
        cen_spod=int(2*prvi_mom-cen_zgor)
        print(cen_spod)
        if (2*prvi_mom-cen_zgor-cen_spod)>0.5:
            cen_spod=cen_spod+1
    
    ans=[prvi_mom,drugi_mom,cen_spod,cen_zgor]
    print(alpha,beta,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    
    
    text = """
    <p>Naj bo slučajna spremenljivka $X$ porazdeljena enakomerno diskretno na točkah $\\left\{\\alpha,\\alpha+1,\\alpha+2,\ldots,\\beta\\right\}$, kjer
    sta $\\alpha$ in $\\beta$ neznani naravni števili. Na vzorcu smo izmerili naslednje vrednosti:</p>

    <pre>%(vzorec)s</pre>

    <p>Iz vzorca oceni prvi %(ans[0])s in drugi moment %(ans[1])s spremenljivke $X$.</p>

    <p>Na podlagi vzorca po metodi momentov oceni vrednost parametrov $\\alpha$  %(ans[2])s in $\\beta$  %(ans[3])s. (Pri tem izračuna zaokrožite na najbližji celi števili.) </p>
    
    """%{"ans[0]": ans[0], "ans[1]": ans[1], "vzorec": vzorec, "ans[2]": ans[2], "ans[3]": ans[3]}

    
    return text


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for par in [[10,25],[14,44],[71,87],[40,57]]:
    text = naloga(par[0],par[1])
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import random

NAME = "Cenilke {}"
CATEGORY = "Kvizi za oceno/Kviz, 14. teden, 11. kviz (cenilke)/6. skupina"
FILE_NAME = "vs_kviz_11_6_2"

def naloga(alpha,beta):

    n = 50
    vzorec=[]
    for i in range(n):
        vzorec.append(int(ceil(random()*(beta-alpha+1)-1))+alpha)

    print(vzorec)
    
    vzorec_kvad=[]
    for i in range(n):
        vzorec_kvad.append(vzorec[i]**2)
    
    prvi_mom=1.0*sum(vzorec)/len(vzorec)
    drugi_mom=1.0*sum(vzorec_kvad)/len(vzorec_kvad)

    cen_zgor=max(vzorec)
    cen_spod=min(vzorec)

    ans=[prvi_mom,drugi_mom,cen_spod,cen_zgor]
    print(alpha,beta,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    
    
    text = """
    <p>Naj bo slučajna spremenljivka $X$ porazdeljena enakomerno diskretno na točkah $\\left\{\\alpha,\\alpha+1,\\alpha+2,\ldots,\\beta\\right\}$, kjer
    sta $\\alpha$ in $\\beta$ neznani naravni števili. Na vzorcu smo izmerili naslednje vrednosti:</p>

    <pre>%(vzorec)s</pre>

    <p>Iz vzorca oceni prvi %(ans[0])s in drugi moment %(ans[1])s spremenljivke $X$.</p>

    <p>Na podlagi vzorca po metodi največjega verjetja oceni vrednost parametrov $\\alpha$  %(ans[2])s in $\\beta$  %(ans[3])s. (Pri tem ne iščite ekstrema funkcije
    $L(\\alpha,\\beta)$ z odvajanjem, temveč s premislekom o možnih vrednostih $\\alpha$, $\\beta$ na podlagi vzorca.) </p>
    
    """%{"ans[0]": ans[0], "ans[1]": ans[1], "vzorec": vzorec, "ans[2]": ans[2], "ans[3]": ans[3]}

    
    return text


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for par in [[10,25],[14,44],[71,87],[40,57]]:
    text = naloga(par[0],par[1])
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import t
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/6. skupina"
FILE_NAME = "ovs_kviz_12_6"

def naloga():
    text = """
    Zbrali smo podatke o dosežku vzorca učencev na nacionalnem testu iz fizike. V naslednjem seznamu so zapisane frekvence posameznih rezultatov, pri čemer
    na \\(i\\)-tem mestu stoji frekvenca učencev, ki so dosegli \\(i-1\\) procentov:

    <pre>{frekvence}</pre>

    <p>Koliko je povprečno število doseženih procentov?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon števila doseženih procentov?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je število doseženih procentov učenca porazdeljeno normalno \\(N(\\mu,\\sigma)\\).

    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.95 za \\(\\mu\\)?
    {{1:MC:=Studentova z {st_stop_prost} prostostnimi stopnjami~Studentova z {st_stop_prost_2} prostostnimi stopnjami~hi-kvadrat z {st_stop_prost} prostostnimi stopnjami~hi-kvadrat
        z {st_stop_prost_2} prostostnimi stopnjami~standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je zgornja meja intervala zaupanja stopnje zaupanja {beta} za \\(\\mu\\)?&nbsp;{ans[2]} </p>

    """
    
    frekvence=[]
    for i in range(30):
        frekvence.append(randint(0,2))
    for i in range(40):
        frekvence.append(randint(0,4))
    for i in range(20):
        frekvence.append(randint(0,3))
    for i in range(11):
        frekvence.append(randint(0,2))
    print(frekvence)
    
    n=sum(frekvence)
    m=len(frekvence)

    povp=0
    for i in range(m):
        povp+=frekvence[i]*i
    povp=1.0*povp/n

    odklon=0
    for i in range(m):
        odklon+=frekvence[i]*(i-povp)**2
    odklon=sqrt(odklon/(n-1))
    
    tt=t.isf(1-(1+beta)/2,n-1)
    Delta=odklon*tt/sqrt(n)
    zgMeja=povp+Delta
    
    print(povp)
    print(odklon)
    print(zgMeja)
    
    ans=[povp,odklon,zgMeja]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(frekvence=frekvence,beta=beta,st_stop_prost=n-1,st_stop_prost_2=n,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
while count<7:
    text = naloga()
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/7. skupina"
FILE_NAME = "ovs_kviz_12_7"

def naloga(n,barva,barva2,p,beta,beta2):
    text = """V nekem velikem mestu so med lastniki avtomobila delali anketo o barvi njihovega avtomobila. Možni odgovori so bili
    'crna', 'bela', 'rdeca', 'modra', 'zelena', 'srebrna', 'drugo'. Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Kolikšen delež anketirancev ima avto barve {barva}?&nbsp;{ans[0]} </p>
    
    <p>Kvantil, katere od naštetih porazdelitev potrebujemo, da izračunamo interval zaupanja stopnje zaupanja 0.9 za delež pojavitve barve {barva2} v mestu?
    {{1:MC:Studentova z {n1} prostostnimi stopnjami~Studentova z {n2} prostostnimi stopnjami~hi-kvadrat z {n1} prostostnimi stopnjami~hi-kvadrat
        z {n2} prostostnimi stopnjami~=standardna normalna porazdelitev~binomska porazdelitev~Fisherjeva porazdelitev}}</p>

    <p>Koliko je spodnja meja intervala zaupanja stopnje zaupanja {beta} za delež lastnikov avtomobil, ki imajo avto barve {barva}?&nbsp;{ans[1]} </p>

    <p>Denimo, da so v nekem drugem mestu prav tako naredili anketo med 200 lastniki avtomobilov o barvi njihovega avtomobila.
       Delež barve {barva2} je bil {p}. Na podlagi tega so izračunali interval zaupanja stopnje zaupanja \\(\\beta\\)
       in dobili dolžino intervala enako {velikost}. Koliko je \\(\\beta\\)?&nbsp; {ans[2]}</p>

    """

    moznosti=['crna', 'bela', 'rdeca', 'modra', 'zelena', 'srebrna', 'drugo']
    m=len(moznosti)
    odgovori=[]
    for i in range(n):
        odgovori.append(moznosti[randint(0,m-1)])
    print(odgovori)

    k=odgovori.count(barva)
    
    delez=1.0*k/n

    sigma=sqrt(delez*(1-delez))
    z=norm.isf(1-(1+beta)/2)
    Delta=sigma*z/sqrt(n)

    spMeja=delez-Delta

    z2=norm.isf(1-(1+beta2)/2)
    velikost=round(2*sqrt(p*(1-p))/sqrt(200)*z2,5)
    
    print(delez)
    print(spMeja)
    print(beta2)
    
    ans=[delez,spMeja,beta2]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,barva=barva,p=p,n1=n-1,n2=n,barva2=barva2,beta=beta,velikost=velikost,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
n=134
barva2='srebrna'
beta=0.9
for barva in ['bela','modra']:
    for p in [0.13,0.17]:
        for beta2 in [0.92,0.94]:
            text = naloga(n,barva,barva2,p,beta,beta2)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt
from math import ceil
from random import randint
import random

NAME = "Intervali zaupanja {}"
CATEGORY = "Kvizi za oceno/Kviz, 13. teden, 12. kviz (intervali zaupanja)/8. skupina"
FILE_NAME = "ovs_kviz_12_8"

def naloga(k,beta,beta2):
    text = """V nekem velikem mestu so med lastniki avtomobilov delali anketo o letniku njihovega avtomobila.
    Odgovori so bili naslednji:<p/>

    <pre>{odgovori}</pre>

    <p>Koliko je povprečni letnik avtomobila?&nbsp;{ans[0]}</p>
    
    <p>Koliko je vzorčni standardni odklon?&nbsp;{ans[1]}</p>

    <p>Predpostavimo, da je letnik avtomobila v mestu porazdeljen normalno \\(N(\\mu,2.3)\\).</p>

    <p>Koliko je spodnja meja intervala zaupanja stopnje zaupanja {beta} za letnik avtomobila?&nbsp;{ans[2]} </p>

    <p>V neki drugi anketi med 400 lastniki avtomobilov o letniku njihovega avtomobila je bil
    interval zaupanja stopnje zaupanja \\(\\beta\\) dolg {velikost}. Koliko je \\(\\beta\\)?&nbsp; {ans[3]}</p>
    """
    
    odgovori=[]
    for i in range(30):
        odgovori.append(randint(2016,2018))
    for i in range(50):
        odgovori.append(randint(2011,2015))
    for i in range(40+k):
        odgovori.append(randint(2003,2010))
    for i in range(10):
        odgovori.append(randint(1988,2002))
    random.shuffle(odgovori)
    print(odgovori)
    
    n=len(odgovori)

    povp=1.0*sum(odgovori)/len(odgovori)

    odklon=0
    for i in range(n):
        odklon+=(odgovori[i]-povp)**2
    odklon=sqrt(odklon/(n-1))

    z=norm.isf(1-(1+beta)/2)
    Delta=2.3*z/sqrt(n)
    spMeja=povp-Delta

    z2=norm.isf(1-(1+beta2)/2)
    Delta=2.3*z2/sqrt(400)
    velikost=round(2*Delta,5)

    print(povp)
    print(odklon)
    print(spMeja)
    print(beta2)
    
    ans=[povp,odklon,spMeja,beta2]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(odgovori=odgovori,beta=beta,velikost=velikost,ans=ans)
    return cloze
    


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
beta=0.95
for k in [5,10,15]:
    for beta2 in [0.93,0.97]:
        text = naloga(k,beta,beta2)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import randint, seed

NAME = "geometrijska {}"
CATEGORY = "Kvizi za oceno/Kviz, 7. teden (diskretne porazdelitve)/1. skupina/geometrijska"
FILE_NAME = "ovs_kviz_7_0"

text = """ <p>Vržemo dve kocki in to ponavljamo, dokler skupno število pik ni enako <b>{pik}</b>.
    Označimo z <b>X</b> število poskusov, ki jih potrebujemo, da nam prvič uspe.</p>
    <p>Spremenljivka <b>X</b> je porazdeljena
    {{1:MC:=geometrijsko~binomsko~normalno~Poissonovo~hipergeometrijsko~enakomerno~eksponentno}}. </p>

    <p>Kolikšna je verjetnost, da bomo uspeli že v prvem poskusu? &nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo <b>X</b> enak <b>{k}</b>?&nbsp;{ans[1]}</p>

    <p>Kolikšna je verjetnost, da bomo potrebovali več kot <b>{l}</b>
    poskusov?{ans[2]}</p>
    <p>Denimo, da nam v <b>{l}</b> poskusih ni uspelo. Kolikšna je verjetnost,
    da bomo potrebovali vsaj še <b>{m}</b> poskusov?{ans[3]}</p>
    <p>Kolikšno je pričakovano število poskusov?&nbsp;{ans[4]}</p>
    """
def resitev(pik, k, l, m):
    verj = (1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1)
    p = verj[pik - 2]/36.0
    q = 1 - p
    ans = [p, 0, 0, 0, 1/p]
    ans[1] = q**(k-1)*p # P(X=k)
    ans[2] = 1 - sum(q**(i - 1)*p for i in range(1, l+1)) # P(X>l)
    ans[3] = (1 - sum(q**(i - 1)*p for i in range(1, l+m)))/ans[2] # P(X>=m+l | X>l)
    ans = ["{{1:NUMERICAL:={}:0.00001}}".format(a) for a in ans]
    cloze = text.format(pik=pik, k=k, l=l, m=m, ans=ans)
    return cloze

questions = ""
count = 1
for pik in (7, 8):
    for k in (3, 4):
        for m in (10, 12):
            l = k + 2
            text = naloga(pik, k, l, m)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import hypergeom


NAME = "hipergeometrijska {}"
CATEGORY = "Kvizi za oceno/Kviz, 7. teden (diskretne porazdelitve)/1. skupina/hipergeometrijska"
FILE_NAME = "ovs_kviz_7_1"

text = """ <p>Iz kupa <b>{N}</b> igralnih kart brez vračanja izvlečemo <b>{n}</b> kart.
    Označimo z <b>X</b> število <b>{vrsta}</b> med izvlečenimi kartami.</p>
    <p>Spremenljivka <b>X</b> je porazdeljena
    {{1:MC:geometrijsko~binomsko~normalno~Poissonovo~=hipergeometrijsko~enakomerno~eksponentno}}. </p>

    <p>Kolikšna je najmanjša: {ans[0]} in največja: {ans[1]} možna vrednost <b>X</b>?</p>
    <p>Kolikšna je verjetnost, da bo <b>X</b> enak <b>{k}</b>?&nbsp;{ans[2]}</p>

    <p>Kolikšna je verjetnost, da bo med izvlečenimi kartami vsaj <b>{l}</b>
    {vrsta}?{ans[3]}</p>
    <p>Kolikšno je pričakovano število {vrsta}?&nbsp;{ans[4]}</p>
    """
def resitev(N, n, vrsta, k, l):

    K = N/4
    hyper = hypergeom(N, K, n)
    ans = [0, min(n,K), hyper.pmf(k), hyper.sf(l-1), hyper.mean()]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(N=N, n=n, k=k, l=l, vrsta=vrsta, ans=ans)
    return cloze


questions = ""
count = 1
vrste = ('pikov', 'src', 'križev', 'kar')
for N in (32, 36):
    for n in (8, 13):
        for k in (4, 5):
            l = k + 1
            vrsta = vrste[count % 4]
            text = naloga(N, n, vrsta, k, l)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import hypergeom


NAME = "hipergeometrijska {}"
CATEGORY = "Kvizi za oceno/Kviz, 7. teden (diskretne porazdelitve)/1. skupina/hipergeometrijska"
FILE_NAME = "ovs_kviz_7_1"

text = """ <p>Iz kupa <b>{N}</b> igralnih kart brez vračanja izvlečemo <b>{n}</b> kart.
    Označimo z <b>X</b> število <b>{vrsta}</b> med izvlečenimi kartami.</p>
    <p>Spremenljivka <b>X</b> je porazdeljena
    {{1:MC:geometrijsko~binomsko~normalno~Poissonovo~=hipergeometrijsko~enakomerno~eksponentno}}. </p>

    <p>Kolikšna je najmanjša: {ans[0]} in največja: {ans[1]} možna vrednost <b>X</b>?</p>
    <p>Kolikšna je verjetnost, da bo <b>X</b> enak <b>{k}</b>?&nbsp;{ans[2]}</p>

    <p>Kolikšna je verjetnost, da bo med izvlečenimi kartami vsaj <b>{l}</b>
    {vrsta}?{ans[3]}</p>
    <p>Kolikšno je pričakovano število {vrsta}?&nbsp;{ans[4]}</p>
    """
def resitev(N, n, vrsta, k, l):

    K = N/4
    hyper = hypergeom(N, K, n)
    ans = [0, min(n,K), hyper.pmf(k), hyper.sf(l-1), hyper.mean()]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(N=N, n=n, k=k, l=l, vrsta=vrsta, ans=ans)
    return cloze


questions = ""
count = 1
vrste = ('pikov', 'src', 'križev', 'kar')
for N in (32, 36):
    for n in (8, 13):
        for k in (4, 5):
            l = k + 1
            vrsta = vrste[count % 4]
            text = naloga(N, n, vrsta, k, l)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import poisson

NAME = "Poissonova {}"
CATEGORY = "Kvizi za oceno/Kviz, 7. teden (diskretne porazdelitve)/2. skupina/poissonova"
FILE_NAME = "ovs_kviz_7_2"

text = """ <p>Predsednik ZDA Donald Trump v povprečju objavi <b>{N}</b> tweetov na teden.
    Označimo z <b>X</b> število predsednikovih tweetov v naslednjih <b>{t}</b> urah.</p>
    <p>Spremenljivka <b>X</b> je porazdeljena
    {{1:MC:geometrijsko~binomsko~normalno~=Poissonovo~hipergeometrijsko~enakomerno~eksponentno}}. </p>

    <p>Kolikšna je verjetnost, da v tem času ne bo nobenega tweeta? &nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo <b>X</b> enak <b>{k}</b>?&nbsp;{ans[1]}</p>

    <p>Kolikšna je verjetnost, da bomo deležni več kot <b>{l}</b>
    tweete?{ans[2]}</p>
    <p>Denimo, da je Trump objavil že <b>{l}</b> tweete. Kolikšna je verjetnost,
    da jih bo objavil še vsaj <b>{m}</b>?{ans[3]}</p>
    <p>Kolikšno je pričakovano število tweetov v naslednjih {t} urah?&nbsp;{ans[4]}</p>
    """
def resitev(N, t, k, l, m):
    lam = N*t/(7*24)
    p = poisson(lam)
    ans = [p.pmf(0), p.pmf(k), p.sf(l), p.sf(l + m - 1)/p.sf(l - 1), lam]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(N=N, t=t, k=k, l=l, m=m, ans=ans)
    return cloze


questions = ""
count = 1
for N in (75, 70):
    for d in (1, 2):
        for tweet in (7, 9):
            t = d*24
            k = tweet*d + 1
            l = k - 2
            m = tweet % 2
            text = naloga(N, t, k, l, m)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import randint, seed

NAME = "tabela {}"
CATEGORY = "Kvizi za oceno/Kviz, 7. teden (diskretne porazdelitve)/2. skupina/tabela"
FILE_NAME = "ovs_kviz_7_3"

text = """ <p>Naj bo <b>X</b> slučajna spremenljivka z vednostmi v {{{vrednosti}}},
    za katero velja \\(P(X=k)=c\\cdot k({N}-k)\\)</b>.</p>

    <p>Določi konstanto <b>c</b>: {ans[0]}.</p>

    <p>Kolikšna je verjetnost, da bo <b>X</b> enak <b>{k}</b>?&nbsp;{ans[1]}</p>

    <p>Kolikšna je verjetnost, da bo <b>X</b> vsaj <b>{l}</b>?{ans[2]}</p>
    <p> Kolikšna je verjetnost, da bo <b>X</b> vsaj <b>{l}</b>, če vemo, da je
    <b>liha</b>?{ans[3]}</p>
    <p>Kolikšno je pričakovana vrednost <b>X</b>?&nbsp;{ans[4]}</p>
    """
def resitev(N, k, l):
    pmf = lambda x, c: c*x*(N - x)
    v = list(range(1, N))
    vrednosti = ", ".join(str(x) for x in v)
    c = 1/sum(pmf(1, x) for x in v)
    ans = [c, pmf(c, k), sum(pmf(c, x) for x in v if x >= l), 0, sum(x*pmf(c, x) for x in v)]
    ans[3] = sum(pmf(c, x) for x in v if (x >= l) and (x%2 == 1))/sum(pmf(c, x) for x in v if x%2 == 1)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(vrednosti=vrednosti, N=N, k=k, l=l, ans=ans)
    return cloze


questions = ""
count = 1
for N in (8, 9):
    for k in (4, 6):
        for l in (6, 7):
            text = naloga(N, k, l)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import randint, seed

NAME = "tabela {}"
CATEGORY = "Kvizi za oceno/Kviz, 7. teden (diskretne porazdelitve)/2. skupina/tabela"
FILE_NAME = "ovs_kviz_7_3"

text = """ <p>Naj bo <b>X</b> slučajna spremenljivka z vednostmi v {{{vrednosti}}},
    za katero velja \\(P(X=k)=c\\cdot k({N}-k)\\)</b>.</p>

    <p>Določi konstanto <b>c</b>: {ans[0]}.</p>

    <p>Kolikšna je verjetnost, da bo <b>X</b> enak <b>{k}</b>?&nbsp;{ans[1]}</p>

    <p>Kolikšna je verjetnost, da bo <b>X</b> vsaj <b>{l}</b>?{ans[2]}</p>
    <p> Kolikšna je verjetnost, da bo <b>X</b> vsaj <b>{l}</b>, če vemo, da je
    <b>liha</b>?{ans[3]}</p>
    <p>Kolikšno je pričakovana vrednost <b>X</b>?&nbsp;{ans[4]}</p>
    """
def resitev(N, k, l):
    pmf = lambda x, c: c*x*(N - x)
    v = list(range(1, N))
    vrednosti = ", ".join(str(x) for x in v)
    c = 1/sum(pmf(1, x) for x in v)
    ans = [c, pmf(c, k), sum(pmf(c, x) for x in v if x >= l), 0, sum(x*pmf(c, x) for x in v)]
    ans[3] = sum(pmf(c, x) for x in v if (x >= l) and (x%2 == 1))/sum(pmf(c, x) for x in v if x%2 == 1)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(vrednosti=vrednosti, N=N, k=k, l=l, ans=ans)
    return cloze


questions = ""
count = 1
for N in (8, 9):
    for k in (4, 6):
        for l in (6, 7):
            text = naloga(N, k, l)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/poskusni kviz"
FILE_NAME = "ovs_kviz_8_0"

text = """ <p>V povprečju je treba ventilator v hladilnem sistemu avtomobila zamenjati
    <b>{n}</b>-krat na vsakih <b>10000</b> ur delovanja. Naj bo <b>X</b> slučajna spremenljivka, ki meri
    čas med dvema menjavama ventilatorja.</p>

    <p>Spremenljivka X je porazdeljena:
    {{1:MC:geometrijsko~enakomerno zvezno~Poissonovo~normalno~=eksponentno~Cauchyjevo}}. </p>


    <p>Koliko je <b>E(X)</b> (v urah)?&nbsp; {ans[0]} </p>

    <p>Koliko je verjetnost, da bo ventilator deloval več kot <b>10000</b> ur?&nbsp; {ans[1]} </p>
    
    <p>Denimo, da je ventilator že deloval 10000 ur. Koliko je verjetnost, da bo deloval
    še <b>5000</b> ur?&nbsp  {ans[2]}</p>
    """
def resitev(n):

    lam=1.0*n/10000
    upan=1.0/lam

    verj=exp(-lam*10000)
    verj2=exp(-lam*5000)
    
       
    ans = [upan,verj,verj2]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(n=n, ans=ans)
    return cloze

questions = ""
count = 1
for n in [3,4,5]:
            text = naloga(n)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/poskusni kviz"
FILE_NAME = "ovs_kviz_8_0"

text = """ <p>V povprečju je treba ventilator v hladilnem sistemu avtomobila zamenjati
    <b>{n}</b>-krat na vsakih <b>10000</b> ur delovanja. Naj bo <b>X</b> slučajna spremenljivka, ki meri
    čas med dvema menjavama ventilatorja.</p>

    <p>Spremenljivka X je porazdeljena:
    {{1:MC:geometrijsko~enakomerno zvezno~Poissonovo~normalno~=eksponentno~Cauchyjevo}}. </p>


    <p>Koliko je <b>E(X)</b> (v urah)?&nbsp; {ans[0]} </p>

    <p>Koliko je verjetnost, da bo ventilator deloval več kot <b>10000</b> ur?&nbsp; {ans[1]} </p>
    
    <p>Denimo, da je ventilator že deloval 10000 ur. Koliko je verjetnost, da bo deloval
    še <b>5000</b> ur?&nbsp  {ans[2]}</p>
    """
def resitev(n):

    lam=1.0*n/10000
    upan=1.0/lam

    verj=exp(-lam*10000)
    verj2=exp(-lam*5000)
    
       
    ans = [upan,verj,verj2]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(n=n, ans=ans)
    return cloze

questions = ""
count = 1
for n in [3,4,5]:
            text = naloga(n)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp


NAME = "Restavracija - eksponentna {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/1. skupina"
FILE_NAME = "ovs_kviz_8_1"

text = """ <p>V času med <b>11:30</b> do <b>13:30</b> v restavraciji ponujajo 3
    različne menije. Za meni 1 se povprečno odloči <b>{mu1}</b> študentov na uro,
    za meni 2 <b>{mu2}</b>, za meni 3 pa <b>{mu3}</b>.</p>

    <p>Koliko je pričakovano število prodanih menijev 1 in 2 v dveh urah?&nbsp; {ans[0]} </p>

    <p>Ob 12:30 zmanjka menija 3. Največ koliko časa lahko traja, da kuharji
    pripravijo nove porcije, da z verjetnostjo vsaj 0.7 v tem času noben študent ne bo
    izbral menija 3?&nbsp; {ans[1]}</p>

    <p>Ob 13:28 prodajo predzadnji obrok menija 2.
    Kolikšna je verjetnost, da bo ob 13:29
    ta meni še na voljo?&nbsp; {ans[2]}</p>

    <p>Denimo, da je ob 13:29 meni 2 res še voljo.
    Kolikšna je verjetnost, da do 13:30 zadnjega obroka tega menija ne bodo prodali?
    &nbsp; {ans[3]}</p>
    """
def resitev(mu1,mu2,mu3):

    prod12=2*(mu1+mu2)
    cas=log(0.7)/(-mu3)
    zadnji=exp(-1.0*mu2/60)
    
    ans = [prod12,cas,zadnji,zadnji]
    ans = ["{{1:NUMERICAL:={}:0.00001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2, mu3=mu3,ans=ans)
    return cloze

questions = ""
count = 1
for mu1 in [100]:
    for mu2 in [80,90]:
        for mu3 in [60,70]:
            text = naloga(mu1,mu2,mu3)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp


NAME = "Restavracija - eksponentna {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/1. skupina"
FILE_NAME = "ovs_kviz_8_1"

text = """ <p>V času med <b>11:30</b> do <b>13:30</b> v restavraciji ponujajo 3
    različne menije. Za meni 1 se povprečno odloči <b>{mu1}</b> študentov na uro,
    za meni 2 <b>{mu2}</b>, za meni 3 pa <b>{mu3}</b>.</p>

    <p>Koliko je pričakovano število prodanih menijev 1 in 2 v dveh urah?&nbsp; {ans[0]} </p>

    <p>Ob 12:30 zmanjka menija 3. Največ koliko časa lahko traja, da kuharji
    pripravijo nove porcije, da z verjetnostjo vsaj 0.7 v tem času noben študent ne bo
    izbral menija 3?&nbsp; {ans[1]}</p>

    <p>Ob 13:28 prodajo predzadnji obrok menija 2.
    Kolikšna je verjetnost, da bo ob 13:29
    ta meni še na voljo?&nbsp; {ans[2]}</p>

    <p>Denimo, da je ob 13:29 meni 2 res še voljo.
    Kolikšna je verjetnost, da do 13:30 zadnjega obroka tega menija ne bodo prodali?
    &nbsp; {ans[3]}</p>
    """
def resitev(mu1,mu2,mu3):

    prod12=2*(mu1+mu2)
    cas=log(0.7)/(-mu3)
    zadnji=exp(-1.0*mu2/60)
    
    ans = [prod12,cas,zadnji,zadnji]
    ans = ["{{1:NUMERICAL:={}:0.00001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2, mu3=mu3,ans=ans)
    return cloze

questions = ""
count = 1
for mu1 in [100]:
    for mu2 in [80,90]:
        for mu3 in [60,70]:
            text = naloga(mu1,mu2,mu3)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/2. skupina"
FILE_NAME = "ovs_kviz_8_2"

text = """ <p>Naj bo slučajna spremenljivka <b>X</b>
    zalogo vrednosti <b>[-1,{zgor}]</b> in gostoto 
    \\(p(t)=1/2\\cdot t^2\\) za negativne <b>t</b>-je ter
    \\(p(t)=c\cdot t\\)</b> za pozitivne <b>t</b>-je, pri čemer je
    <b>c</b> konstanta.</p>

    <p>Koliko je \\( P(X\\leq 0) \\)?&nbsp; {ans[0]} </p>
    
    <p>Koliko je c?&nbsp; {ans[1]} </p>
    
    <p>Koliko je \\( P(-1/2\\leq X\\leq {zgor2}) \\)</b>?&nbsp;
     {ans[2]} </p>

    <p>Koliko je \\(E(X)\\)</b>?&nbsp; {ans[3]} </p>
    """
def resitev(zgor,zgor2):

    verj=1.0/6
    kons=(1-1.0/6)*2/(zgor**2)
    verj2=1.0/6*(1.0/2)**3+kons*(1.0*zgor2)**2/2
    upan=-1.0/8+kons*(zgor)**3/3
    
    ans = [verj,kons,verj2,upan]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(zgor=zgor, zgor2=zgor2, ans=ans)
    return cloze

questions = ""
count = 1
for zgor in [2,3,4]:
    for zgor2  in [0.5,1,1.5]:
            text = naloga(zgor,zgor2)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/2. skupina"
FILE_NAME = "ovs_kviz_8_2"

text = """ <p>Naj bo slučajna spremenljivka <b>X</b>
    zalogo vrednosti <b>[-1,{zgor}]</b> in gostoto 
    \\(p(t)=1/2\\cdot t^2\\) za negativne <b>t</b>-je ter
    \\(p(t)=c\cdot t\\)</b> za pozitivne <b>t</b>-je, pri čemer je
    <b>c</b> konstanta.</p>

    <p>Koliko je \\( P(X\\leq 0) \\)?&nbsp; {ans[0]} </p>
    
    <p>Koliko je c?&nbsp; {ans[1]} </p>
    
    <p>Koliko je \\( P(-1/2\\leq X\\leq {zgor2}) \\)</b>?&nbsp;
     {ans[2]} </p>

    <p>Koliko je \\(E(X)\\)</b>?&nbsp; {ans[3]} </p>
    """
def resitev(zgor,zgor2):

    verj=1.0/6
    kons=(1-1.0/6)*2/(zgor**2)
    verj2=1.0/6*(1.0/2)**3+kons*(1.0*zgor2)**2/2
    upan=-1.0/8+kons*(zgor)**3/3
    
    ans = [verj,kons,verj2,upan]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(zgor=zgor, zgor2=zgor2, ans=ans)
    return cloze

questions = ""
count = 1
for zgor in [2,3,4]:
    for zgor2  in [0.5,1,1.5]:
            text = naloga(zgor,zgor2)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/3. skupina"
FILE_NAME = "ovs_kviz_8_3"

text = """ <p>Naj bo slučajna spremenljivka <b>X</b>
    porazdeljena enakomerno zvezno na intervalu <b>[-{spod},0]</b>,
    slučajna spremenljivka <b>Y</b> pa eksponentno <b>Exp({lam})</b>.</p>

    <p>Zaloga vrednosti slučajne spremenljivke <b>Z=X+Y</b> je 
    {{1:MC:[0,1]~[-{spod},0]~[-{spod},{spod}]~[-1,1]~=[-{spod},inf)~(inf,inf)}}. </p>

    <p>Koliko je gostota spremenljivke X v točki -1?&nbsp; {ans[0]} </p>
    
    <p>Koliko je <b>E(Z)</b>?&nbsp; {ans[1]} </p>

    <p>Določi konstanto A tako, da bo verjetnost
    \\(P(-1\leq X)=P(Y\leq A)\\)</b>?&nbsp  {ans[2]}</p>
    """
def resitev(spod,lam):

    gost=1.0/spod
    povp=-1.0*spod/2+1.0/lam
    A=log(1-1.0/spod)/(-lam)
       
    ans = [gost,povp,A]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(spod=spod,lam=lam, ans=ans)
    return cloze

questions = ""
count = 1
for spod in [10,15,20]:
    for lam in [2,3,4]:
            text = naloga(spod,lam)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/3. skupina"
FILE_NAME = "ovs_kviz_8_3"

text = """ <p>Naj bo slučajna spremenljivka <b>X</b>
    porazdeljena enakomerno zvezno na intervalu <b>[-{spod},0]</b>,
    slučajna spremenljivka <b>Y</b> pa eksponentno <b>Exp({lam})</b>.</p>

    <p>Zaloga vrednosti slučajne spremenljivke <b>Z=X+Y</b> je 
    {{1:MC:[0,1]~[-{spod},0]~[-{spod},{spod}]~[-1,1]~=[-{spod},inf)~(inf,inf)}}. </p>

    <p>Koliko je gostota spremenljivke X v točki -1?&nbsp; {ans[0]} </p>
    
    <p>Koliko je <b>E(Z)</b>?&nbsp; {ans[1]} </p>

    <p>Določi konstanto A tako, da bo verjetnost
    \\(P(-1\leq X)=P(Y\leq A)\\)</b>?&nbsp  {ans[2]}</p>
    """
def resitev(spod,lam):

    gost=1.0/spod
    povp=-1.0*spod/2+1.0/lam
    A=log(1-1.0/spod)/(-lam)
       
    ans = [gost,povp,A]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(spod=spod,lam=lam, ans=ans)
    return cloze

questions = ""
count = 1
for spod in [10,15,20]:
    for lam in [2,3,4]:
            text = naloga(spod,lam)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/4. skupina"
FILE_NAME = "ovs_kviz_8_4"

text = """<p>Zvezna slučajna spremenljivka <b>X</b> ima
    zalogo vrednosti <b>[-5,T]</b>, kjer je <b>T</b> pozitivno število.
    Na intervalu <b>[-5,0]</b> je gostota konstanta c, za točko x
    iz intervala <b>[0,T]</b> pa \\({lam}\\cdot exp(-{lam}\\cdot x)\\)</b>.
    Naj bo verjetnost \\(P(X<0)={ver}.\\)</p>

    <p>Koliko je konstanta c?&nbsp; {ans[0]} </p>
    
    <p>Koliko je T?&nbsp;{ans[1]} </p>

    <p>Koliko je \\( P(X=2) \\)</b>?&nbsp;
    {ans[2]} </p>
    
    <p>Koliko je \\( P(-2\\leq X\\leq T/2) \\)</b>?&nbsp;
     {ans[3]} </p>

    """
def resitev(ver,lam):

    gost=ver/5
    T=log(ver)/(-lam)
    verj=gost*2+(1-exp(-lam*T/2))
    
    ans = [gost,T,0,verj]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(ver=ver, lam=lam,ans=ans)
    return cloze

questions = ""
count = 1
for ver in [0.2,0.3,0.4]:
    for lam in [0.5,1.0,1.5]:
            text = naloga(ver,lam)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1
            print(count)



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/4. skupina"
FILE_NAME = "ovs_kviz_8_4"

text = """<p>Zvezna slučajna spremenljivka <b>X</b> ima
    zalogo vrednosti <b>[-5,T]</b>, kjer je <b>T</b> pozitivno število.
    Na intervalu <b>[-5,0]</b> je gostota konstanta c, za točko x
    iz intervala <b>[0,T]</b> pa \\({lam}\\cdot exp(-{lam}\\cdot x)\\)</b>.
    Naj bo verjetnost \\(P(X<0)={ver}.\\)</p>

    <p>Koliko je konstanta c?&nbsp; {ans[0]} </p>
    
    <p>Koliko je T?&nbsp;{ans[1]} </p>

    <p>Koliko je \\( P(X=2) \\)</b>?&nbsp;
    {ans[2]} </p>
    
    <p>Koliko je \\( P(-2\\leq X\\leq T/2) \\)</b>?&nbsp;
     {ans[3]} </p>

    """
def resitev(ver,lam):

    gost=ver/5
    T=log(ver)/(-lam)
    verj=gost*2+(1-exp(-lam*T/2))
    
    ans = [gost,T,0,verj]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(ver=ver, lam=lam,ans=ans)
    return cloze

questions = ""
count = 1
for ver in [0.2,0.3,0.4]:
    for lam in [0.5,1.0,1.5]:
            text = naloga(ver,lam)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1
            print(count)



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/5. skupina"
FILE_NAME = "ovs_kviz_8_5"

text = """<p>Naj bosta dani neodvisni normalno porazdeljeni
    slučajni spremenljivki <b>X~N({mu1},{sigma1})</b>,
    <b>Y~N({mu2},{sigma2})</b> in eksponentno porazdeljena spremeljivka
    <b>Z~Exp({lam})</b>.</p>

    <p>Koliko je <b>D(X+Y)?</b> &nbsp; {ans[0]} </p>

    <p>Izračunaj upanja naslednjih spremenljivk: {upanja}</p>

    Naj bosta W spremenljivka z največjim, U pa spremenljivka z najmanjšim
    upanjem iz prejšnjega vprašanja.
    
    <p>Koliko je \\( P(2\\leq W)\\)?&nbsp; {ans[1]} </p>
    
    <p>Določi T, tako da velja je
    \\( P(U\\leq {mu1})=P(W\\leq T) \\)?&nbsp; {ans[2]} </p>

    """
def resitev(mu1,mu2,sigma,lam):

    disp=sigma[0]**2+sigma[1]**2

    mu=mu1+2.0*1/lam
    up=[('X:',mu1),('Y:',mu2),('Z:',1.0/lam),('X-Y:',mu1-mu2),
        ('X+2Z',mu)]
    upanja= ", ".join("{} {{1:NUMERICAL:={}:0.0001}}".format(p[0],p[1]) for p in up)

    verj=exp(-2*lam)

    T=log(1.0/2)/(-lam)
    
    ans = [disp,verj,T]
    ans = ["{{5:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2,sigma1=sigma[0],
                        sigma2=sigma[1],lam=lam,upanja=upanja,mu=mu,ans=ans)
    return cloze

questions = ""
count = 1
for mu1 in [-40,-30]:
    for mu2 in [-10,-5]:
        for sigma in [(3,4),(6,8)]:
            for lam in [1.5]:
                text = naloga(mu1,mu2,sigma,lam)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/5. skupina"
FILE_NAME = "ovs_kviz_8_5"

text = """<p>Naj bosta dani neodvisni normalno porazdeljeni
    slučajni spremenljivki <b>X~N({mu1},{sigma1})</b>,
    <b>Y~N({mu2},{sigma2})</b> in eksponentno porazdeljena spremeljivka
    <b>Z~Exp({lam})</b>.</p>

    <p>Koliko je <b>D(X+Y)?</b> &nbsp; {ans[0]} </p>

    <p>Izračunaj upanja naslednjih spremenljivk: {upanja}</p>

    Naj bosta W spremenljivka z največjim, U pa spremenljivka z najmanjšim
    upanjem iz prejšnjega vprašanja.
    
    <p>Koliko je \\( P(2\\leq W)\\)?&nbsp; {ans[1]} </p>
    
    <p>Določi T, tako da velja je
    \\( P(U\\leq {mu1})=P(W\\leq T) \\)?&nbsp; {ans[2]} </p>

    """
def resitev(mu1,mu2,sigma,lam):

    disp=sigma[0]**2+sigma[1]**2

    mu=mu1+2.0*1/lam
    up=[('X:',mu1),('Y:',mu2),('Z:',1.0/lam),('X-Y:',mu1-mu2),
        ('X+2Z',mu)]
    upanja= ", ".join("{} {{1:NUMERICAL:={}:0.0001}}".format(p[0],p[1]) for p in up)

    verj=exp(-2*lam)

    T=log(1.0/2)/(-lam)
    
    ans = [disp,verj,T]
    ans = ["{{5:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2,sigma1=sigma[0],
                        sigma2=sigma[1],lam=lam,upanja=upanja,mu=mu,ans=ans)
    return cloze

questions = ""
count = 1
for mu1 in [-40,-30]:
    for mu2 in [-10,-5]:
        for sigma in [(3,4),(6,8)]:
            for lam in [1.5]:
                text = naloga(mu1,mu2,sigma,lam)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 8. teden, 7. kviz (zvezne porazdelitve)/5. skupina"
FILE_NAME = "vs_kviz_8_5"

text = """ <p>Naj bo slučajna spremenljivka <b>X</b>
    zalogo vrednosti <b>[-1,{zgor}]</b> in gostoto 
    \\(p(t)=1/2\\cdot t^2\\) za \\(t\\) na intervalu <b>[-1,0]</b> ter
    \\(p(t)=c\cdot t\\)</b> za \\(t\\) na intervalu <b>[0,{zgor}]</b>, pri čemer je
    <b>c</b> konstanta.</p>

    <p>Koliko je \\( P(X\\leq 0) \\)?&nbsp; {ans[0]} </p>
    
    <p>Koliko je c?&nbsp; {ans[1]} </p>
    
    <p>Koliko je \\( P(-1/2\\leq X\\leq {zgor2}) \\)</b>?&nbsp;
     {ans[2]} </p>

    """
def resitev(zgor,zgor2):

    verj=1.0/6
    kons=(1-1.0/6)*2/(zgor**2)
    verj2=1.0/6*(1.0/2)**3+kons*(1.0*zgor2)**2/2
    upan=-1.0/8+kons*(zgor)**3/3
    
    ans = [verj,kons,verj2,upan]
    print(zgor,zgor2,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(zgor=zgor, zgor2=zgor2, ans=ans)
    return cloze

questions = ""
count = 1
for zgor in [2,3,4]:
    for zgor2  in [0.5,1,1.5]:
            text = naloga(zgor,zgor2)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import quad
from scipy.optimize import fsolve


NAME = "Zvezne - kumulativna porazdelitvena funkcija {}" 
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/6. skupina"
FILE_NAME = "ovs_kviz_8_6"

text = """<p>Naj bo $X$ zvezna slučajna spremenljivka z vrednostmi na intervalu <b>[0,{x[0]}]</b>
    in kumulativno porazdelitveno funkcijo
    $$F_X(x) = {funkcija}.$$
    <p>Določi $P(X\\ge {x[1]})$: {ans[0]}, $P(X\\le {x[2]})$: {ans[1]} in $P({x[3]}\\le X\\le {x[4]})$: {ans[2]}</p>
    <p>Če z $p_X$ označimo gostoto verjetnosti spremenljivke $X$, izračunaj
    $p_X({x[1]})+p_x({x[2]})+p_X({x[3]})+p_X({x[4]})$: {ans[3]}</p>
    <p>Koliko je <b>E(X)</b>? &nbsp; {ans[4]} </p>
    <p>Koliko je <b>D(X)</b>? {ans[5]} </p>
    <p>Določi $x$, za katerega je $P(X\\ge x)={p}$: {ans[6]}
    """
def resitev(funkcija, cdf, pdf, x, p):
    b = x[0]
    ans = [0 for i in range(7)]
    E = quad(lambda t: t*pdf(t), 0, x[0])[0]
    D = quad(lambda t: (t-E)**2*pdf(t), 0, x[0])[0]
    if x[1] < 0:
        ans[0] = 1
    else:
        ans[0] = 1 - cdf(x[1])
    if x[2] > b:
        ans[1] = 1
    else:
        ans[1] = cdf(x[2])
    ans[2] = cdf(min(b, x[4])) - cdf(max(0, x[3]))

    for v in x[1:5]:
        if (v>0) and (v < b):
            ans[3] += pdf(v)
    ans[4] = E
    ans[5] = D
    ans[6] = fsolve(lambda t: 1 - cdf(t) - p, 1.0)[0]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(funkcija=funkcija, x=x, p=p, ans=ans)
    return cloze

questions = ""
count = 1
cdf = {'x^2': lambda x: x**2, 'x^3': lambda x: x**3}
pdf = {'x^2': lambda x: 2*x, 'x^3': lambda x: 3*x**2}
for funkcija in ['x^2', 'x^3']:
    for x in [(1, 0.3, 0.5, -1, 0.3), (1, -0.3, 0.2, 0.2, 2)]:
        for p in (0.9, 0.95):
                text = naloga(funkcija, cdf[funkcija], pdf[funkcija], x, p)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import quad
from scipy.optimize import fsolve


NAME = "Zvezne - kumulativna porazdelitvena funkcija {}" 
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/6. skupina"
FILE_NAME = "ovs_kviz_8_6"

text = """<p>Naj bo $X$ zvezna slučajna spremenljivka z vrednostmi na intervalu <b>[0,{x[0]}]</b>
    in kumulativno porazdelitveno funkcijo
    $$F_X(x) = {funkcija}.$$
    <p>Določi $P(X\\ge {x[1]})$: {ans[0]}, $P(X\\le {x[2]})$: {ans[1]} in $P({x[3]}\\le X\\le {x[4]})$: {ans[2]}</p>
    <p>Če z $p_X$ označimo gostoto verjetnosti spremenljivke $X$, izračunaj
    $p_X({x[1]})+p_x({x[2]})+p_X({x[3]})+p_X({x[4]})$: {ans[3]}</p>
    <p>Koliko je <b>E(X)</b>? &nbsp; {ans[4]} </p>
    <p>Koliko je <b>D(X)</b>? {ans[5]} </p>
    <p>Določi $x$, za katerega je $P(X\\ge x)={p}$: {ans[6]}
    """
def resitev(funkcija, cdf, pdf, x, p):
    b = x[0]
    ans = [0 for i in range(7)]
    E = quad(lambda t: t*pdf(t), 0, x[0])[0]
    D = quad(lambda t: (t-E)**2*pdf(t), 0, x[0])[0]
    if x[1] < 0:
        ans[0] = 1
    else:
        ans[0] = 1 - cdf(x[1])
    if x[2] > b:
        ans[1] = 1
    else:
        ans[1] = cdf(x[2])
    ans[2] = cdf(min(b, x[4])) - cdf(max(0, x[3]))

    for v in x[1:5]:
        if (v>0) and (v < b):
            ans[3] += pdf(v)
    ans[4] = E
    ans[5] = D
    ans[6] = fsolve(lambda t: 1 - cdf(t) - p, 1.0)[0]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(funkcija=funkcija, x=x, p=p, ans=ans)
    return cloze

questions = ""
count = 1
cdf = {'x^2': lambda x: x**2, 'x^3': lambda x: x**3}
pdf = {'x^2': lambda x: 2*x, 'x^3': lambda x: 3*x**2}
for funkcija in ['x^2', 'x^3']:
    for x in [(1, 0.3, 0.5, -1, 0.3), (1, -0.3, 0.2, 0.2, 2)]:
        for p in (0.9, 0.95):
                text = naloga(funkcija, cdf[funkcija], pdf[funkcija], x, p)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1

# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import expon
import matplotlib


NAME = "Zvezne - splosno {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)"
FILE_NAME = "vs_kviz_9_1"

text = """ <p>V povprečju je treba ventilator v hladilnem sistemu avtomobila zamenjati
    <b>{n}</b>-krat na vsakih <b>10000</b> ur delovanja. Naj bo <b>X</b> slučajna spremenljivka, ki meri
    čas med dvema menjavama ventilatorja.</p>

    <p>Spremenljivka X je porazdeljena:
    {{1:MC:geometrijsko~enakomerno zvezno~Poissonovo~normalno~=eksponentno~Cauchyjevo}}.</p>


    <p>Koliko je <b>E(X)</b> (v urah)?&nbsp; {ans[0]} </p>

    <p>Koliko je verjetnost, da bo ventilator deloval več kot <b>10000</b> ur?&nbsp; {ans[1]} </p>
    
    <p>Denimo, da je ventilator že deloval 10000 ur. Koliko je verjetnost, da bo deloval
    še <b>5000</b> ur?&nbsp  {ans[2]}</p>
    """
def resitev(n):

    lam=1.0*n/10000
    upan=expon.mean(0,1.0/lam)

    verj=expon.sf(10000,0,1.0/lam)
    verj2=expon.sf(5000,0,1.0/lam)

    print(upan,verj,verj2)       
    anspom = [verj,verj2]
    anspom = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in anspom]
    ans = ["{{1:NUMERICAL:={}:0.1}}".format(upan),anspom[0],anspom[1]]      
    
    cloze = text.format(n=n, ans=ans)
    return cloze

questions = ""
count = 1
for n in [2,4,5]:
            text = naloga(n)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp
from scipy.stats import expon


NAME = "Restavracija - eksponentna {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/2. skupina"
FILE_NAME = "vs_kviz_9_2"

text = """ <p>V času med <b>11:30</b> do <b>13:30</b> se v restavraciji za
    meni 1 povprečno odloči <b>{mu2}</b> študentov na uro, za meni 2 pa <b>{mu3}</b>.
    Predpostavimo, da število izborov posameznega menija sledi Poissonovi porazdelitvi.
    Naj bo <b>X</b> slučajna spremenljivka, ki meri čas med dvema izbirama menija 1.</p>
    
    <p>Spremenljivka X je porazdeljena:
    {{1:MC:geometrijsko~enakomerno zvezno~Poissonovo~normalno~=eksponentno~Cauchyjevo}}. </p>

    <p>Ob 13:28 prodajo predzadnji obrok menija 1.
    Kolikšna je verjetnost, da bo ob 13:29
    ta meni še na voljo?&nbsp; {ans[0]}</p>

    <p>Denimo, da je ob 13:29 meni 1 res še voljo.
    Kolikšna je verjetnost, da do 13:30 zadnjega obroka tega menija ne bodo prodali?
    &nbsp; {ans[1]}</p>

    <p>Ob 12:30 zmanjka menija 2. Največ koliko časa (v minutah) lahko kuharji
    pripravljajo nove porcije, da z verjetnostjo vsaj 0.7 v tem času noben študent ne bo
    izbral menija 2?&nbsp; {ans[2]}</p>
    """
def resitev(mu2,mu3):

    zadnji=expon.sf(1,0,1.0/(1.0*mu2/60))
    cas=expon.isf(0.7,0,1.0/(1.0*mu3/60))

    ans=[zadnji,zadnji,cas]
    print(mu2,mu3,ans)
    for i in [0,1]:
        ans[i]="{{1:NUMERICAL:={}:0.001}}".format(ans[i])
    ans[2]="{{1:NUMERICAL:={}:0.01}}".format(ans[2])
    print(ans)
    cloze = text.format(mu2=mu2, mu3=mu3,ans=ans)
    return cloze

questions = ""
count = 1
for mu2 in [80,90]:
    for mu3 in [60,70]:
        text = naloga(mu2,mu3)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

QUIZ.append(QuestionSet(text, parametri, resitev18, name="kroglice"))

with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))

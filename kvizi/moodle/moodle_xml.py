from itertools import repeat
import sys
import math
import inspect

QUESTION_XML_TEMPLATE = """
<question type="{type}">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="{format}">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <penalty>0.3333333</penalty>
  </question>
"""
CATEGORY_XML_TEMPLATE = """
  <question type="category">
    <category>
        <text>$course$/{category}</text>
    </category>
  </question>
"""
QUIZ_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
{}
</quiz>
"""
def map_tex(value):
    "Returns TeX string if the value has a method __tex__"
    if hasattr(value, "__tex__") and callable(getattr(value, "__tex__")):
        print("mapping tex")
        tex = value.__tex__()
    elif isinstance(value, list) or isinstance(value, tuple):
        print("mapping in list")
        tex = [map_tex(elt) for elt in value]
    elif isinstance(value, dict):
        print("mapping in dict")
        tex = {key: map_tex(value[key]) for key in value}
    else:
        tex = value
    return tex


class Quiz:
    def __init__(self, category, question_sets=[]):
        self.question_sets = question_sets
        self.category = category

    def __str__(self):
        text = self.category_xml + "\n" + self.program_xml
        for qset in self.question_sets:
            text += "\n" + str(qset)
        return QUIZ_XML_TEMPLATE.format(text)

    def __tex__(self):
        tex = "\chapter{{{}}}".format(self.category)
        for qset in self.question_sets:
            tex += "\n{}".format(qset.sample_question().tex)
        return tex

    def append(self, question_set):
        "Appends a question set"
        return self.question_sets.append(question_set)

    @property
    def tex(self):
        return self.__tex__()

    @property
    def program_xml(self):
        "XML element for the generating code"
        file_name = sys.argv[0]
        with open(file_name) as file_pointer:
            src = file_pointer.read()
        data = {"type": "description",
                "name": "python code",
                "format": "plain_text",
                "text": src}
        return QUESTION_XML_TEMPLATE.format(**data)

    @property
    def category_xml(self):
        "XML element for category"
        return CATEGORY_XML_TEMPLATE.format(category=self.category)


class QuestionSet:
    "A collection of similar questions"
    DEFAULT_CATEGORY = ""
    def __init__(self, template, parameter_sets, solutions,
                 **kwargs):
        "A collection of questions"
        self.name = kwargs.get("name")
        category = type(self).DEFAULT_CATEGORY
        #category = kwargs.get("category") or type(self).DEFAULT_CATEGORY
        self.category = "{}/{}".format(category, self.name)
        self.template = template
        self.parameter_sets = parameter_sets
        if callable(solutions):
            # get the source code of the solution
            self.solution_source = inspect.getsource(solutions)
            self.solutions = repeat(solutions)
        else:
            self.solutions = solutions
        self.questions = []
        for i, (parameters, solution) in enumerate(zip(self.parameter_sets, self.solutions)):
            self.questions.append(ClozeQuestion(self.template, parameters, 
                                        solution, name=self.name + " {}".format(i)))

    def __str__(self):
        text = self.category_xml + "\n" + self.code_xml
        for question in self.questions:
            text += "\n" + question.moodle_xml
        return text

    def sample_question(self):
        "Return a sample question. First at the moment"
        return self.questions[0]
    
    @property
    def category_xml(self):
        "XML element for category"
        return CATEGORY_XML_TEMPLATE.format(category=self.category)
    @property
    def code_xml(self):
        "XML element for code"
        data = {"type": "description",
                "name": "python solution",
                "format": "plain_text"}
        data["text"] = self.solution_source
        return QUESTION_XML_TEMPLATE.format(**data)


class ClozeQuestion:
    """A class for Moodle Questions
    
    Example:
    >>> name = "vsota"
    >>> template = "{a}+{b} = {ans}"
    >>> parameters = {"a":1, "b": 2}
    >>> solution = lambda a,b: a+b
    >>> question = ClozeQuestion(template, parameters, solution, name=name)
    >>> print(question.moodle_xml)
<question type="cloze">
    <name>
      <text>vsota</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[1+2 = 3]]></text>
    </questiontext>
    <penalty>0.3333333</penalty>
  </question>
    """
    def __init__(self, template, parameters, solution, **kwargs):
        self.name = kwargs.get("name")
        self.template = template
        self.parameters = parameters
        if callable(solution):
            # calculate solution
            self.solution = solution(**parameters)
        else:
            self.solution = solution
        # try to render the question for easier debugging
        self.moodle_xml

    def __str__(self):
        return self.moodle_xml

    @property
    def moodle_xml(self):
        "Moodle XML representation of a Question"
        data = {"type": "cloze", 
                "name": self.name,
                "format": "html"}
        data["text"] = self.template.format(**self.solution, **self.parameters)
        return QUESTION_XML_TEMPLATE.format(**data)

    def __tex__(self):
        "Question in LaTeX format"
        html = self.template.format( 
                **map_tex(self.solution), 
                **self.parameters)
        # write tex file
        tex = html.replace("<p>", "")
        tex = tex.replace("</p>", "\\par\n")
        tex = tex.replace("&nbsp;", " ")
        tex = tex.replace("<i>", "\\textit{")
        tex = tex.replace("</i>", "}")
        tex = tex.replace("<b>", "\\textbf{")
        tex = tex.replace("</b>", "}")
        tex = tex.replace("<ol>", "\\begin{enumerate}")
        tex = tex.replace("</ol>", "\\end{enumerate}")
        tex = tex.replace("<li>", "\\item ")
        tex = tex.replace("</li>", "")
        return "\\begin{{naloga}}[vaje]{{{label}}}\n{tex}\n\\end{{naloga}}".format(label=self.name, tex=tex)
    @property
    def tex(self):
        return self.__tex__()

class NumAnswer:
    """A type for numerical answer in moodle cloze format
    
    Example:
    >>> answer = NumAnswer(1/3)
    >>> answer.partial_answer(NumAnswer(1/3,points=20, precision=0.1, feedback="Not precise enough!"))
    >>> print(answer)

    """
    def __init__(self, answer, precision=0.0001, points=1, feedback="Pravilno!"):
        self.answer = answer
        self.precision = precision
        self.points = points
        self.feedback = feedback
        self.partials = []

    def __str__(self):
        return self.cloze
    def partial_answer(self, num_answer):
        self.partials.append(num_answer)

    def __tex__(self):
        "Print answer in TeX format"
        if isinstance(self.answer, int):
            pfmt = "{}"
        else:
            pfmt = "{{:.{}f}}".format(round(-math.log10(self.precision)))
        return " \\hfill \\odgovor[{1}]{{{0}}}".format(
                pfmt.format(self.answer),
                self.precision)
    @property
    def cloze(self):
        text = "{{{points}:NUMERICAL:={answer}:{precision}#{feedback}".format(
            points=self.points,
            answer=self.answer,
            precision=self.precision,
            feedback=self.feedback
        )
        for partial in self.partials:
            text += "~%{percent}%{answer}:{precision}#{feedback}".format(
                percent=partial.points,
                answer=partial.answer,
                precision=partial.precision,
                feedback=partial.feedback)
        return text + "}"
    @property
    def tex(self):
        return self.__tex__()

class ShortAnswer:
    """A type for short answers in moodle cloze format
    
    Example:
    >>> answer = ShortAnswer('banana')
    >>> answer.partial_answer(ShortAnswer('fruit',points=20, feedback="Not precise enough!"))
    >>> print(answer)
    """
    def __init__(self, answer, points=1, feedback="Pravilno!"):
        self.answer = answer
        self.points = points
        self.feedback = feedback
        self.partials = []

    def __str__(self):
        return self.cloze
    def partial_answer(self, answer):
        self.partials.append(answer)

    def __tex__(self):
        "Print answer in TeX format"
        return " \\hfill \\odgovor{{{0}}}".format(self.answer)
    @property
    def cloze(self):
        text = "{{{points}:SHORTANSWER:={answer}#{feedback}".format(
            points=self.points,
            answer=self.answer,
            feedback=self.feedback
        )
        for partial in self.partials:
            text += "~%{percent}%{answer}#{feedback}".format(
                percent=partial.points,
                answer=partial.answer,
                feedback=partial.feedback)
        return text + "}"
    @property
    def tex(self):
        return self.__tex__()



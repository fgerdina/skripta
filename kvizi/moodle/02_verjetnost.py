#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import os
from scipy.special import binom
from moodle_xml import Quiz, NumAnswer, QuestionSet, ShortAnswer

POGLAVJE = "02 verjetnost"
CATEGORY = "Vaje/{}".format(POGLAVJE)
QUIZ = Quiz(CATEGORY)

QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/priprava na vaje"

text = """
<p>Pošten kovanec vržemo <b>{n}</b> krat. Izide označimo z besedami
dolžine <b>{n}</b> setavljenimi iz črk <b>G</b>(grb) in <b>C</b>(cifra).</p>
<p>Na list zapiši vse izide! Zapiši izid, ko najprej pade grb, nato pa same cifre.&nbsp;{ans[0]}</p>
<p>Koliko je vseh možnih izidov?&nbsp;{ans[1]}</p>
<p>Kolikšna je verjetnost, da bosta padli natanko dve cifri?&nbsp;{ans[2]}</p>
"""
def resitev0(n):
    ans = (ShortAnswer('G'+(n-1)*'C'),
            NumAnswer(2**n),
            NumAnswer(n*(n-1)/2**(n+1))
            )
    return {"ans": ans}

parametri = [{"n": n} for n in [3, 4]]

QUIZ.append(QuestionSet(text, parametri, resitev0, name="kovanci"))

QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/elementarna verjetnost"

text = """
    <p>V škatli je <b>{b}</b> belih, <b>{r}</b> rdečih in <b>{c}</b> črnih kroglic. Izvlečemo 3 kroglice, ki jih <b>vračamo</b>.</p>
    <p>Kolikšna je verjetnost, da sta vsaj dve enake barve?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da sta prva in tretja beli?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da sta prva ali tretja beli?&nbsp;{ans[2]}</p>
    <p>Poskus ponovimo, a kroglic tokrat <b>ne vračamo</b>. Kolikšna je verjetnost, da sta prva ali tretja beli?&nbsp;{ans[3]}</p>
    """
def resitev1(b, r, c):
    a = b + r + c
    p2e = 1 - 6*b*r*c/a**3 
    p1b3b = b**2/a**2
    p1bv3b = 2*(b/a) - p1b3b
    p1bv3bv = 2*(b/a) - b*(b-1)/(a*(a-1))
    ans = (p2e, p1b3b, p1bv3b, p1bv3bv)
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}



parametri = []
for b in [5,6]:
    for r in [b, b+1, b+2]:
        for c in [r+1, r+2]:
            parametri.append({"b":b, "r":r, "c":c})

QUIZ.append(QuestionSet(text, parametri, resitev1, name="kroglice vračamo"))



text = """
    <p>V škatli je <b>{b}</b> belih, <b>{r}</b> rdečih in <b>{c}</b> črnih kroglic. Izvlečemo 4 kroglice, ki jih <b>ne vračamo</b>.</p>
    <p>Kolikšna je verjetnost, da so med izvlečenimi vsaj dve različni barvi?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da sta prva <b>in</b> zadnja rdeči?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da sta prva <b>ali</b> zadnja rdeči?&nbsp;{ans[2]}</p>
    <p>Poskus ponovimo, a kroglice tokrat <b>vračamo</b>. Kolikšna je verjetnost, da sta prva ali zadja rdeči?&nbsp;{ans[3]}</p>
     """

def resitev3(b, r, c):
    a = b + r + c
    ans = []
    ans.append(1 - sum(x*(x-1)*(x-2)*(x-3) for x in (b, r, c))/(a*(a-1)*(a-2)*(a-3)))
    ppresek = r*(r-1)/(a*(a-1))
    ans.append(ppresek)
    ans.append(2*r/a - ppresek)
    ans.append(2*r/a - r**2/a**2)
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}



parametri = []

for b in [5,6]:
    for r in [b, b+1, b+2]:
        for c in [r+1, r+2]:
            parametri.append({"b":b, "r":r, "c":c})



QUIZ.append(QuestionSet(text, parametri, resitev3, name="kroglic ne vračamo"))



text = """
    <p>V predalu je <b>{n}</b> parov nogavic. Zaporedoma izvlečemo <b>4</b> nogavice, ki jih ne vračamo.</p>
    <p>Kolikšna je verjetnost, da ni nobenega para?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da dobimo dva para?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da sta prva in zadnja nagavica par?&nbsp;{ans[2]}</p>
    <p>Kolikšna je verjetnost, da sta 1. in 4. ali 2. in 3. par?&nbsp;{ans[3]}</p>
    """
def resitev5(n):
    ans = []
    ans.append(1.0*(2*n-4)*(2*n-6)/((2*n-1)*(2*n-3)))
    ans.append(1.0*3*1/(2*n-1)*1/(2*n-3))
    ans.append(1.0/(2*n-1))
    ans.append(2.0/(2*n-1)-1/((2*n-1)*(2*n-3)))
    print("{n} parov: p={p}".format(n=n, p=ans))
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}



parametri = []

for n in range(11, 19):
    parametri.append({"n":n})



QUIZ.append(QuestionSet(text, parametri, resitev5, name="nogavice pari"))




text = """
    <p>V predalu je <b>{m}</b> modrih, <b>{r}</b> rdečih in <b>{c}</b> črnih nogavic. Izvlečemo 4 nogavice, ki jih ne vračamo.</p>
    <p>Kolikšna je verjetnost, da je vsaj ena modra?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da dobimo dve rdeči in dve črni?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da sta natanko dve črni ali natanko dve rdeči?&nbsp;{ans[2]}</p>
    """
def resitev6(m, r, c):
    a = m + r + c
    nm = r + c
    # p1m je narobe, izvlečemo 4!
    p1m = 1 - binom(r+c,4)/binom(r+c+m,4)
    p2c2r = binom(r, 2)*binom(c,2)/binom(a,4)
    p2r = binom(r,2)*binom(m + c,2)/binom(a,4)
    p2c = binom(c,2)*binom(m+r,2)/binom(a,4)
    punija = p2r + p2c - p2c2r
    ans = [p1m, p2c2r, punija]
    ans = [NumAnswer(a) for a in ans]
    print(ans)
    infd = 'INPUTLINE'
    cloze = text.format(m=m, r=r, c=c, ans=ans) 
    html = text.format(m=infd, r=infd, c=infd, ans=[infd for i in ans])
    return {"ans": ans}



parametri = []

for m in [5,6]:
    for r in [m, m+1, m+2]:
        for c in [r+1, r+2]:
            parametri.append({"m":m, "r":r, "c":c})


QUIZ.append(QuestionSet(text, parametri, resitev6, name="kroglice barve"))





text = """
    <p>Iz kupa z <b>{n}</b> kartami povlečemo <b>{k}</b> kart, ki jih ne vračamo.</p>
    <p>Kolikšna je verjetnost, da je vsaj ena pik?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da je polovica pikov in polovica src?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da je vsaj ena karta pik ali srce?&nbsp;{ans[2]}</p>
    <p>Kolikšna je verjetnost, da dobimo natanko dva pika ali natanko dve srci?&nbsp;{ans[3]}</p>
    """
def resitev7(n, k):
    p = n/4
    s = p
    np = 3*p
    p1p = 1 - binom(np,k)/binom(n,k)
    pps = binom(p, k/2)*binom(p,k/2)/binom(n,k)
    pvs = 1 - binom(2*p,k)/binom(n,k)
    ppvss = (2*binom(p,2)*binom(np,k-2)-binom(p,2)**2*binom(2*p,k-4))/binom(n,k)
    ans = [p1p, pps, pvs, ppvss]
    ans = [NumAnswer(a) for a in ans]
    print(ans)
    infd = 'INPUTLINE'
    cloze = text.format(n=n, k=k, ans=ans) 
    html = text.format(n=infd, k=infd, ans=[infd for i in ans])
    return {"ans": ans}



parametri = []

for n in [28,32,36]:
    for k in [6,8,10]:
        parametri.append({"n":n, "k":k})



QUIZ.append(QuestionSet(text, parametri, resitev7, name="karte"))





text = """
    <p>Dvakrat vržemo <b>{n}</b> strano kocko, ki ima na stranicah od 1 do {n} pik.</p>
    <p>Kolikšna je verjetnost, da je vsota pik enaka <b>{v}</b>?&nbsp;{ans[0]}</p>
    <p>Kocko vržemo <b>{k}</b> krat.</p> 
    <p>Kolikšna je verjetnost, da vsaj enkrat pade 1 pika?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da pade natanko dvakrat 1 <b>in</b> natanko dvakrat {n} pik?&nbsp;{ans[2]}</p>
    <p>Kolikšna je verjetnost, da pade natanko dvakrat 1 <b>ali</b> natanko dvakrat {n} pik??&nbsp;{ans[3]}</p>
    """
def resitev9(n, v, k):
    ans = [None for i in range(4)]
    ans[0] = (n - abs(n+1-v))/n**2
    ans[1] = 1 - ((n-1)/n)**k
    ans[2] = 1/n**4*((n-2)/n)**(k-4)*k*(k-1)*(k-2)*(k-3)/4
    ans[3] = 2*k*(k-1)/2*1/n**2*((n-1)/n)**(k-2) - ans[2]
    ans = [NumAnswer(a) for a in ans]
    print(ans)
    infd = 'INPUTLINE'
    cloze = text.format(n=n, v=v, k=k, ans=ans) 
    html = text.format(n=infd, v=infd, k=infd, ans=[infd for i in ans])
    return {"ans": ans}



parametri = []

for n in [4,6,8]:
    for i in [0, 1, 2]:
        parametri.append({"n":n, "v":n+i, "k":7-i})



QUIZ.append(QuestionSet(text, parametri, resitev9, name="kocka"))



QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/geometrijska verjetnost"

text = """
    <p>V času, ko so Slovenijo zajele snežne nevihte, je avtobus prihajal na postajo 
    med 12h in <b>{a}</b> minut čez poldne, vlak pa je zapuščal postajo med 12h in <b>{v}</b> minut 
    čez poldne.</p>
    <p>Kolikšna je verjetnost, da je vlak odšel pred {b} minut čez poldne?&nbsp;{ans[0]}</p>
    <p>Miha je na postajo prišel ob naključnem času med 12h in 13h. Kolikšna je verjetnost, da je ujel vlak?&nbsp;{ans[1]}</p>
    <p>Maja se je pripeljala z avtobusom. Kolikšna je verjetnost, da je ujela vlak?&nbsp;{ans[2]}</p>
    """
def resitev10(a, v, f):
    ans = (0.25*f,(v+0.0)/120,1-(a+0.0)/(2*v))
    ans = [NumAnswer(an) for an in ans]
    print(ans)
    infd = 'INPUTLINE'
    cloze = text.format(a=a, v=v, b=f*v/4, f = f, ans=ans)
    html = text.format(a=infd, v=infd, b=infd, f=infd, ans=[infd for i in ans])
    return {"b": f*v//4, "ans": ans}



parametri = []

for a in [15, 25]:
    for v in [20, 40]:
        for f in [1, 3]:
            if v > a and v/4*f > 5:
                parametri.append({"a":a, "v":v, "f":f})

QUIZ.append(QuestionSet(text, parametri, resitev10, name="snežne nevihte"))



text = """
    <p>Restavracija Lakotnik je Alenki obljubila, da bodo naročeno hrano dostavili med 12h in <b>{v}</b> minut 
    čez poldne. Njen brat Blaž ji je sporočil, da jo bo obiskal enkrat med 12h in 13h, sestra Sanja pa, da se oglasi med 12h in <b>{a}</b> minut čez poldne.</p>
    <p>Kolikšna je verjetnost, da bodo hrano dostavili pred 12:{b}?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo Blaž obiskal Alenko pred dostavo hrane?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da bo Sanja prišla pred dostavo?&nbsp;{ans[2]}</p>
    """
def resitev12(a, v, f):
    ans = (0.25*f,(v+0.0)/120,1-(a+0.0)/(2*v))
    ans = [NumAnswer(an) for an in ans]
    print(ans)
    infd = 'INPUTLINE'
    cloze = text.format(a=a, v=v, b=int(f*v/4), f = f, ans=ans)
    html = text.format(a=infd, v=infd, b=infd, f=infd, ans=[infd for i in ans])
    return {"b":f*v//4, "ans": ans}



parametri = []

for a in [15, 20]:
    for v in [20, 40]:
        for f in [1, 3]:
            if v > a and f*v/4 > 5:
                parametri.append({"a":a, "v":v, "f":f})

QUIZ.append(QuestionSet(text, parametri, resitev12, name="restavracija"))

text = """
    <p>Alenka dela za oglaševalsko agencijo in čaka, da se na TV pojavijo njene reklame. TV hiša A ji je obljubila, da bodo reklamo predvajali med 12h in <b>{v}</b> minut 
    čez poldne, TV hiša B je zagotovila, da bo reklama predvajana enkrat med 12h in 13h, TV hiša C pa, da bo na vrsti med 12h in <b>{a}</b> minut čez poldne.</p>
    <p>Kolikšna je verjetnost, da bo reklama na TV A pred 12:{b}?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo reklama B predvajana pred reklamo A?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da bo reklama C predvajana pred reklamo A?&nbsp;{ans[2]}</p>
    """
def resitev14(a, v, f):
    ans = (0.25*f,(v+0.0)/120,1-(a+0.0)/(2*v))
    ans = [NumAnswer(an) for an in ans]
    print(ans)
    infd = 'INPUTLINE'
    cloze = text.format(a=a, v=v, b=int((f*v)/4), f = f, ans=ans)
    html = text.format(a=infd, v=infd, b=infd, f=infd, ans=[infd for i in ans])
    return {"b": f*v//4, "ans": ans}



parametri = []

for a in [ 15,  25]:
    for v in [20, 40]:
        for f in [1, 3]:
            if v > a and f*v/4 > 5:
                parametri.append({"a":a, "v":v, "f":f})

QUIZ.append(QuestionSet(text, parametri, resitev14, name="reklame"))


text = """
    <p>Podjetnici Pepci je pomembna stranka obljubila, da jo pokliče med 12h in <b>{v}</b> minut 
    čez poldne. Tajnica ji bo prinesla kavo med 12h in <b>{a}</b> minut čez poldne.</p>
    <p>Kolikšna je verjetnost, da bo stranka poklicala pred 12:{b}?&nbsp;{ans[0]}</p>
    <p>Pepca bo ob naključnem času med 12h in 13h odšla na stranišče. Kolikšna je verjetnost, da bo to pred klicem stranke?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, bo dobila kavo pred klicem?&nbsp;{ans[2]}</p>
    """
def resitev16(a, v, f):
    ans = (0.25*f,(v+0.0)/120,1-(a+0.0)/(2*v))
    ans = [NumAnswer(an) for an in ans]
    print(ans)
    infd = 'INPUTLINE'
    cloze = text.format(a=a, v=v, b=int((f*v)/4), f = f, ans=ans)
    html = text.format(a=infd, v=infd, b=infd, f=infd, ans=[infd for i in ans])
    return {"b": f*v//4, "ans": ans}



parametri = []

for a in [15, 25]:
    for v in [20, 40]:
        for f in [1, 3]:
            if v > a and f*v/4 > 5:
                parametri.append({"a":a, "v":v, "f":f})

QUIZ.append(QuestionSet(text, parametri, resitev16, name="klici"))



text = """
    <p>Z računalnikom generiramo dve naključni realni števili <i>x</i> in <i>y</i> na intervalu $[0,1]$.</p>
    <p>Kolikšna je verjetnost, da bo točka <i>(x,y)</i> manj kot <b>{r}</b> oddaljena od izhodišča?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo razlika \\({a}x-{b}y\\) pozitivna?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da bo \\(x\\geq y\\) ali bo razdalja do izhodišča več kot <b>{r}</b>?&nbsp;{ans[2]}</p>
    """
def resitev18(r, a, b):
    if a>b:
        prazlika = 1 - b/(2*a)
    else:
        prazlika = a/(2*b)
    ans = [ math.pi*r**2/4,
            prazlika,
            1 - math.pi*r**2/8]
    ans = [NumAnswer(an) for an in ans]
    print("r={}, a={}, b={}: p={}".format(r,a,b,ans[1]))
    return {"ans": ans}



parametri = []

for a in [2, 5]:
    for r in [1, 0.5]:
        for b in [3, 4]:
            parametri.append({"r": r, "a": a, "b": b})


QUIZ.append(QuestionSet(text, parametri, resitev18, name="točke"))

text = """
    <p>Naključno izberemo kompleksno število <i>z=x+iy</i>, za katerega je \\(|z|\le {r}\\).</p>
    <p>Kolikšna je verjetnost, da bo <i>z</i> manj kot <b>{a}</b> oddaljena od izhodišča?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo \\(x\in [0,{a}]\\) in hkrati \\(y\in[-{b},{b}]\\)?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da bo realni del <i>z</i> negativen ali da bo razdalja do izhodišča več kot <b>{a}</b>?&nbsp;{ans[2]}</p>
    """
def resitev18(r, a, b):
    ans = [ a**2/r**2,
            2*b*a/math.pi/r**2,
            1 - a**2/r**2/2]
    ans = [NumAnswer(an) for an in ans]
    print("r={}, a={}, b={}: p={}".format(r,a,b,ans[1]))
    return {"ans": ans}



parametri = []

for a in [3, 4]:
    for r in [5, 6]:
        for b in [2, 3]:
            parametri.append({"r": r, "a": a, "b": b})


QUIZ.append(QuestionSet(text, parametri, resitev18, name="kompleksna števila"))


with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))

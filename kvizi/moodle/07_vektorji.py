#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64

NAME = "Slucajni vektorji {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/1. skupina"
FILE_NAME = "ovs_kviz_9_1"

text = """ <p>Naj bosta dani slučajni spremenljivki <b>X</b> z zalogo vrednosti <b>1,2,3</b> in <b>Y</b> z zalogo vrednosti
    <b>a,2</b>, pri čemer je <b>a</b> neznana konstanta. Naj velja
    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
    \\(P(X=3,y=a)={verj5}\\).</p>
      
     
    <p>Koliko je \\(P(X=3,y=2)\\)?&nbsp;{ans[0]} </p>

    <p>Koliko je <b>E(X)</b>?&nbsp;{ans[1]} </p>
        
    <p>Določi <b>a</b> tako, da bo <b>E(Y)={up}</b>?&nbsp;{ans[2]} </p>

    <p>Koliko je pri tem <b>a</b> kovarianca <b>cov(X,Y)</b>?&nbsp;{ans[3]} </p>

    """
def resitev(i,up):

    verjetnosti=[0.15,0.20,0.25,0.05,0.30,0.05]

    verj1=verjetnosti[i%6]
    verj2=verjetnosti[(i+1)%6]
    verj3=verjetnosti[(i+2)%6]
    verj4=verjetnosti[(i+3)%6]
    verj5=verjetnosti[(i+4)%6]
    verj6=verjetnosti[(i+5)%6]

    r1=verj1+verj2
    r2=verj3+verj4
    r3=verj5+verj6

    upX=1.0*r1+2.0*r2+3.0*r3

    q1=verj1+verj3+verj5
    q2=1-q1
    a=(up-q2*2)/q1
    
    upXY=a*verj1+2*verj2+2*a*verj3+4*verj4+3*a*verj5+6*verj6
    
    cov=upXY-upX*up
    
    ans = [verj6,upX,a,cov]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(up=up, verj1=verj1,verj2=verj2,verj3=verj3,verj4=verj4,verj5=verj5,ans=ans)
    return cloze



questions = ""
count = 1
for i in (0,1,2):
    for up in (-1,1):
                text = naloga(i,up)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64

NAME = "Slucajni vektorji {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/6. skupina"
FILE_NAME = "ovs_kviz_9_6"

text = """ <p>Naj bosta dani slučajni spremenljivki <b>X</b> z zalogo vrednosti <b>1, 2</b> in <b>Y</b> z zalogo vrednosti
    <b>1, 2, a</b>, pri čemer je <b>a</b> neznana konstanta. Naj velja
    \\(P(X=1,y=1)={verj1}\\), \\(P(X=1,y=2)={verj2}\\), \\(P(X=1,y=a)={verj3}\\),
    \\(P(X=2,y=1)={verj4}\\), \\(P(X=2,y=2)={verj5}\\), \\(P(X=2,y=a)={verj6}\\).</p>
      
     
    <p>Koliko je <b>p</b>?&nbsp;{ans[0]} </p>

    <p>Koliko je <b>E(X)</b>?&nbsp;{ans[1]} </p>
        
    <p>Določi <b>a</b> tako, da bo <b>E(Y)={up}</b>?&nbsp;{ans[2]} </p>

    <p>Koliko je pri tem <b>a</b> kovarianca <b>cov(X,Y)</b>?&nbsp;{ans[3]} </p>

    """
def resitev(i,up):

    p=0.2
    verj=[0.35-p,p,5*p/4,p/4,p+0.1,p-0.15]
    bes=['0.35-p','p','5p/4','p/4','p+0.1','p-0.15']
    
    verj1=bes[i%6]
    verj2=bes[(i+1)%6]
    verj3=bes[(i+2)%6]
    verj4=bes[(i+3)%6]
    verj5=bes[(i+4)%6]
    verj6=bes[(i+5)%6]

    r1=verj[i%6]+verj[(i+1)%6]+verj[(i+2)%6]
    r2=verj[(i+3)%6]+verj[(i+4)%6]+verj[(i+5)%6]

    upX=1.0*r1+2.0*r2

    q1=verj[i%6]+verj[(i+3)%6]
    q2=verj[(i+1)%6]+verj[(i+4)%6]
    q3=verj[(i+2)%6]+verj[(i+5)%6]
    a=(up-q1-2*q2)/q3

    upXY=1*verj[i%6]+2*verj[(i+1)%6]+a*verj[(i+2)%6]+2*verj[(i+3)%6]+4*verj[(i+4)%6]+2*a*verj[(i+5)%6]
    
    cov=upXY-upX*up
    
    ans = [p,upX,a,cov]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(up=up, verj1=verj1,verj2=verj2,verj3=verj3,verj4=verj4,verj5=verj5,verj6=verj6,ans=ans)
    return cloze



questions = ""
count = 1
for i in (0,1,2):
    for up in (-1,1):
                text = naloga(i,up)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64

NAME = "Slucajni vektorji {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/7. skupina"
FILE_NAME = "ovs_kviz_9_7"

text = """ <p>Naj bosta dani neodvisni slučajni spremenljivki <b>X</b> z zalogo vrednosti 
    <b>-1, 0, 1</b> in verjetnostmi <b>{verjx1}, {verjx2}, {verjx3}</b>,
    ter <b>Y</b> z zalogo vrednosti <b>1, 2</b> in verjetnostima
    <b>{verjy1}, {verjy2}</b>. Naj bo dana še slučajna spremenljivka
    <b>Z=XY</b>.</p>
    
    <p>Koliko je <b>P(Z=0)</b>?&nbsp;{ans[0]} </p>

    <p>Koliko je <b>E(Z)</b>?&nbsp;{ans[1]} </p>
        
    <p>Koliko je <b>E(X^2)</b>?&nbsp;{ans[2]} </p>

    <p>Koliko je kovarianca <b>cov(X^2,Z)</b>?&nbsp;{ans[3]} </p>

    """
def resitev(i,j):

    verjx=[0.1,0.3,0.6]
    verjy=[0.3,0.7]

    verjx1=verjx[i%3]
    verjx2=verjx[(i+1)%3]
    verjx3=verjx[(i+2)%3]

    verjy1=verjy[j%2]
    verjy2=verjy[(j+1)%2]
    
    verj0=verjx2

    upZ=-1*verjx1*verjy1-2*verjx1*verjy2+verjx3*verjy1+2*verjx3*verjy2

    upX2=1-verjx2

    upY=verjy1+2*verjy2
    upX3=-verjx1+verjx3
    cov=upX3*upY-upX2*upZ

    ans = [verj0,upZ,upX2,cov]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(verjx1=verjx1,verjx2=verjx2,verjx3=verjx3,verjy1=verjy1,verjy2=verjy2,ans=ans)
    return cloze



questions = ""
count = 1
for i in (0,1,2):
    for j in (0,1):
                text = naloga(i,j)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64

NAME = "Slucajni vektorji {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/8. skupina"
FILE_NAME = "ovs_kviz_9_8"

text = """ <p>Naj bosta dani neodvisni slučajni spremenljivki <b>X</b> z zalogo vrednosti 
    <b>-1, 1</b> in verjetnostmi <b>{verjx1}, {verjx2}</b>,
    ter <b>Y</b> z zalogo vrednosti <b>0, 1, 2</b> in verjetnostima
    <b>{verjy1}, {verjy2}, {verjy3}</b>. Naj bo dana še slučajna spremenljivka
    <b>Z=X+Y</b>.</p>
    
    <p>Koliko je <b>P(Z=1)</b>?&nbsp;{ans[0]} </p>

    <p>Koliko je <b>E(Z)</b>?&nbsp;{ans[1]} </p>
        
    <p>Koliko je <b>E(Y^2)</b>?&nbsp;{ans[2]} </p>

    <p>Koliko je kovarianca <b>cov(Y,Z)</b>?&nbsp;{ans[3]} </p>

    """
def resitev(i,j):

    verjx=[0.2,0.8]
    verjy=[0.2,0.1,0.7]

    verjx1=verjx[i%2]
    verjx2=verjx[(i+1)%2]

    verjy1=verjy[j%3]
    verjy2=verjy[(j+1)%3]
    verjy3=verjy[(j+2)%3]
    
    verj1=verjx2*verjy1+verjx1*verjy3

    upZ=-1*verjx1*verjy1+verj1+2*verjx2*verjy2+3*verjx2*verjy3

    upY2=verjy2+4*verjy3

    upY=verjy2+2*verjy3
    cov=upY2-upY**2

    ans = [verj1,upZ,upY2,cov]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    cloze = text.format(verjx1=verjx1,verjx2=verjx2,verjy1=verjy1,verjy2=verjy2,verjy3=verjy3,ans=ans)
    return cloze



questions = ""
count = 1
for i in [0,1]:
    for j in [0,1,2]:
                text = naloga(i,j)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import dblquad
import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/1. skupina"
FILE_NAME = "vs_kviz_10_1_1"

text = """<p>Slučajni vektor $(X,Y)$ je porazdeljen zvezno z gostoto"""
def resitev(i):

    if i==1:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot x^2y,  & 5>x> y>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==2:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot xy^2,  & 5>x> y>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==3:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot (x+2y),  & 5>x> y>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==4:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot (2x-y),  & 5>x> y>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""

    text1="""<p>Določite konstanto $c$.&nbsp; {ans[0]}</p>

    <p>Izračunajte $P(1\\le X \\le 4)$.&nbsp; {ans[1]}</p>

    <p>Izračunajte $E(X^2Y)$.&nbsp; {ans[2]}</p>"""

    gostote=[lambda y,x: x**2*y,lambda y,x: x*y**2,lambda y,x: x+2*y,lambda y,x: 2*x-y]
    c=1.0/dblquad(gostote[i-1], 0, 5, lambda x: 0, lambda x: x)[0]
    verj=c*dblquad(gostote[i-1], 1, 4, lambda x: 0, lambda x: x)[0]
    upan=c*dblquad(lambda y,x:gostote[i-1](y,x)*x**2*y, 0, 5, lambda x: 0, lambda x: x)[0]
    print(i,c,verj,upan)
    ans = [c,verj,upan]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    text1=text1.format(ans=ans)
    text=text+text1
    cloze=text
    return cloze

questions = ""
count = 1
for i in range(1,5):
        text = naloga(i)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import dblquad
import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/1. skupina"
FILE_NAME = "vs_kviz_10_1_2"

text = """<p>Slučajni vektor $(X,Y)$ je porazdeljen zvezno z gostoto"""
def resitev(i):

    if i==1:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot x^2y,  & 4>y> x>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==2:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot xy^2,  & 4>y> x>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==3:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot (x+2y),  & 4>y> x>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==4:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot (y-x),  & 4>y> x>0\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""

    text1="""<p>Določite konstanto $c$.&nbsp; {ans[0]}</p>

    <p>Izračunajte $P(1\\le Y \\le 3)$.&nbsp; {ans[1]}</p>

    <p>Izračunajte $E(XY^2)$.&nbsp; {ans[2]}</p>"""

    gostote=[lambda x,y: x**2*y,lambda x,y: x*y**2,lambda x,y: x+2*y,lambda x,y: y-x]
    c=1.0/dblquad(gostote[i-1], 0, 4, lambda y: 0, lambda y: y)[0]
    verj=c*dblquad(gostote[i-1], 1, 3, lambda y: 0, lambda y: y)[0]
    upan=c*dblquad(lambda x,y:gostote[i-1](x,y)*x*y**2, 0, 4, lambda y: 0, lambda y: y)[0]
    print(i,c,verj,upan)
    ans = [c,verj,upan]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    text1=text1.format(ans=ans)
    text=text+text1
    cloze=text
    return cloze

questions = ""
count = 1
for i in range(1,5):
        text = naloga(i)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import binom
from scipy.stats import hypergeom
import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/2. skupina"
FILE_NAME = "vs_kviz_10_2_1"

text = """<p>Slučajni vektor $(X,Y)$ je porazdeljen po shemi
    $$\\begin{array}{|c|ccc|}
        \\hline
        X\\backslash Y & 0 \ & 1 \ & 2\\\\ \\hline
        0 & 0.01 & a & 0.1\\\\
        1 & b & 0.03 & 0.2\\\\
        2 & 0.1 & 0.05 & c\\\\
        \\hline
        \\end{array}.$$</p>"""
def resitev(i):

    if i==1:
        text1="""<p>Slučajna spremenljivka $X$ je porazdeljena binomsko $B(2,0.4)$. Potem so konstante $a,b,c$ enake:
            $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""
    if i==2:
        text1="""<p>Slučajna spremenljivka $X$ šteje število rdečih kroglic, ko iz posode z 4 rdečimi in 5 belimi kroglicami izvlečemo 2 kroglici.
            Potem so konstante $a,b,c$ enake: $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""
    if i==3:
        text1="""<p>Slučajna spremenljivka $X$ šteje število sodih rezultatov, ko dvakrat zapored vržemo pošteno kocko.
            Potem so konstante $a,b,c$ enake: $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""
    if i==4:
        text1="""<p>Slučajna spremenljivka $X$ šteje število cifer pri dveh metih nepoštenega kovanca, kjer cifra pade z verjetnostjo 0.6.
            Potem so konstante $a,b,c$ enake: $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""

    text2="""<p>Koliko je $F_{X,Y}(2,1)$?"""

    text3=""" {ans[3]}</p>"""

    text4="""<p>Izračunajte $E(e^X\\cdot Y^2)$.&nbsp; {ans[4]}</p>"""

    
    fun=[lambda x: binom.pmf(x,2,0.4),lambda x: hypergeom.pmf(x,9,4,2),lambda x: binom.pmf(x,2,0.5),lambda x:binom.pmf(x,2,0.6)]

    a=fun[i-1](0)-0.01-0.1
    b=fun[i-1](1)-0.03-0.2
    c=fun[i-1](2)-0.1-0.05

    F21=0.01+a+b+0.03+0.1+0.05

    upan=a*exp(0)*1+0.1*exp(0)*4+0.03*exp(1)*1+0.2*exp(1)*4+0.05*exp(2)*1+c*exp(2)*4

    print(i,a,b,c,F21,upan)

    ans = [a,b,c,F21,upan]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    text1=text1.format(ans=ans)
    text3=text3.format(ans=ans)
    text4=text4.format(ans=ans)
    text=text+text1+text2+text3+text4
    cloze=text
    return cloze

questions = ""
count = 1
for i in range(1,5):
        text = naloga(i)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import binom
from scipy.stats import hypergeom
import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/2. skupina"
FILE_NAME = "vs_kviz_10_2_2"

text = """<p>Slučajni vektor $(X,Y)$ je porazdeljen po shemi
    $$\\begin{array}{|c|ccc|}
        \\hline
        X\\backslash Y & 0 \ & 1 \ & 2\\\\ \\hline
        0 & 0.01 & a & 0.1\\\\
        1 & b & 0.03 & 0.02\\\\
        2 & 0.1 & 0.05 & c\\\\
        \\hline
        \\end{array}.$$</p>"""
def resitev(i):

    if i==1:
        text1="""<p>Slučajna spremenljivka $Y$ je porazdeljena binomsko $B(2,0.6)$. Potem so konstante $a,b,c$ enake:
            $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""
    if i==2:
        text1="""<p>Slučajna spremenljivka $Y$ šteje število modrih kroglic, ko iz posode z 4 rdečimi in 5 modrimi kroglicami izvlečemo 2 kroglici.
            Potem so konstante $a,b,c$ enake: $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""
    if i==3:
        text1="""<p>Slučajna spremenljivka $Y$ šteje število sodih rezultatov, ko dvakrat zapored vržemo pošteno kocko.
            Potem so konstante $a,b,c$ enake: $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""
    if i==4:
        text1="""<p>Slučajna spremenljivka $Y$ šteje število grbov pri dveh metih nepoštenega kovanca, kjer grb pade z verjetnostjo 0.4.
            Potem so konstante $a,b,c$ enake: $a$: {ans[0]}, $b$: {ans[1]}, $c$: {ans[2]}.</p>"""

    text2="""<p>Koliko je $F_{X,Y}(1,2)$?"""

    text3=""" {ans[3]}</p>"""

    text4="""<p>Izračunajte $E(\sqrt{X}\\cdot (Y-2))$."""

    text5=""" {ans[4]}</p>"""

    
    fun=[lambda x: binom.pmf(x,2,0.6),lambda x: hypergeom.pmf(x,9,5,2),lambda x: binom.pmf(x,2,0.5),lambda x:binom.pmf(x,2,0.4)]

    a=fun[i-1](1)-0.03-0.05
    b=fun[i-1](0)-0.01-0.1
    c=fun[i-1](2)-0.1-0.02

    F12=0.01+a+0.1+b+0.03+0.02

    upan=b*1*(-2)+0.03*1*(-1)+0.1*sqrt(2)*(-2)+0.05*sqrt(2)*(-1)

    print(i,a,b,c,F12,upan)

    ans = [a,b,c,F12,upan]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    text1=text1.format(ans=ans)
    text3=text3.format(ans=ans)
    text5=text5.format(ans=ans)
    text=text+text1+text2+text3+text4+text5
    cloze=text
    return cloze

questions = ""
count = 1
for i in range(1,5):
        text = naloga(i)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.stats import binom
from scipy.stats import hypergeom
import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/2. skupina"
FILE_NAME = "vs_kviz_10_3_1"


    amin=0

    disk=b**2+4*c
    a1=(-b-sqrt(disk))/2
    a2=(-b+sqrt(disk))/2
    amax=a2

    a=0.1
    e=c+1
    upan=1.0/e*(-1*b*a-a**2*4-(a**2+b*a-c)+4.0)
    B=b*a/(-1.0*(a**2+b*a-c)+b*a)
    
    ans=[amin,amax,upan,B]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    print(b,c,a1,a2,amin,amax,upan,B)

text = """<p>Slučajni vektor $(X,Y)$ je porazdeljen po shemi
    $$\\begin{array}{|c|cc|}
        \\hline
        X\\backslash Y & 1 \ & 2\\\\ \\hline
        -1 & \\frac{%(b)da}{%(e)d} & \\frac{a^2}{%(e)d}\\\\
        1  & -\\frac{1}{%(e)d}(a^2+%(b)da-%(c)d)  & \\frac{%(f)d}{%(e)d}\\\\ 
        \\hline
        \\end{array}.$$</p>

        <p>Najmanj koliko je vrednost spremenljivke $a$? %(ans[0])s.</p>

        <p>Največ koliko je vrednost spremenljivke $a$? %(ans[1])s.</p>

        <p>Za $a=0.1$ izračunajte $E(XY^2)$.%(ans[2])s.</p>

        <p>Za $a=0.1$ izračunajte $b$ v shemi:
        $X|(Y=1)\\sim\\left(
        \\begin{array}{cc}-1 \ & 1\\\\ 
        b & 1-b
        \\end{array}\\right).$ %(ans[3])s.</p>""" %{"b": b, "c": c, "e": e, "f": 1, "ans[0]": ans[0],
def resitev(b,c):
                                             "ans[1]": ans[1],"ans[2]": ans[2],"ans[3]": ans[3]}

    return text

questions = ""
count = 1
for b in [3,5]:
    for c in [1,2]:
        text = naloga(b,c)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import dblquad
import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/4. skupina"
FILE_NAME = "vs_kviz_10_4_1"

text = """<p>Slučajni vektor $(X,Y)$ je porazdeljen zvezno z gostoto"""
def resitev(i):

    if i==1:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot \\frac{y^3}{x^2},  & x,y\\in [1,4]  \\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==2:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot \\frac{x}{y},  & x,y\\in [1,4]\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==3:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot e^{-x+y/2},  & x,y\\in [1,4]\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""
    if i==4:
        text=text+""" $p_{(X,Y)}(x,y)\ =\\left\{\\begin{array}{rc} c\\cdot x^3e^{y/10},  &  x,y\in [1,4]\\\\ 0 & \\text{sicer} \\end{array}\\right..$</p>"""

    text1="""<p>Določite konstanto $c$.&nbsp; {ans[0]}</p>

    <p>Izračunajte $P(2\\le X\\leq 3)$.&nbsp; {ans[1]}</p>

    <p>Izračunajte $P(X+Y\\geq 2)$.&nbsp; {ans[2]}</p>"""

    gostote=[lambda y,x: y**3/x**2,lambda y,x: x/y,lambda y,x: exp(-x+y/2),lambda y,x: x**3*exp(y/10)]
    c=1.0/dblquad(gostote[i-1], 1, 4, lambda x: 1, lambda x: 4)[0]
    verj=c*dblquad(gostote[i-1], 2, 3, lambda x: 1, lambda x: 4)[0]
    verj2=c*dblquad(gostote[i-1], 1, 4, lambda x: max(2-x,1), lambda x: 4)[0]
    print(i,c,verj,verj2)
    ans = [c,verj,verj2]
    ans = ["{{1:NUMERICAL:={}:0.0001}}".format(a) for a in ans]
    text1=text1.format(ans=ans)
    text=text+text1
    cloze=text
    return cloze

questions = ""
count = 1
for i in range(1,5):
        text = naloga(i)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import dblquad
import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/5. skupina"
FILE_NAME = "vs_kviz_10_5_1"


    funkcije=[lambda x,y: x**2+2*y, lambda x,y: exp(x)+y, lambda x,y: x**4+y,lambda x,y: 1.0/x**2+y]
    
    x=[p,2]; y=[0,1,q]; f=funkcije[i]

    verj=0
    for s in x:
        for t in y:
            verj=verj+f(s,t)
    c=1.0/verj

    verj2=0
    for s in x:
        for t in y:
            if s*t>=2:
                verj2=verj2+c*f(s,t)

    v=[0,0,0];
    for s in range(0,3):
        for t in range(0,2):
            v[s]=v[s]+c*f(x[t],y[s])
            
    print(p,q,i,verj2,v[0],v[1],v[2],sum(v))
    ans=["{{3:NUMERICAL:={}:0.001}}".format(c),"{{3:NUMERICAL:={}:0.001}}".format(verj2),
         "{{1:NUMERICAL:={}:0.001}}".format(v[0]),"{{1:NUMERICAL:={}:0.001}}".format(v[1]),"{{1:NUMERICAL:={}:0.001}}".format(v[2])]
    
    
text = """<p>Naj bosta $X$ in $Y$ diskretni slučajni spremenljivki z zalogama vrednosti $\\left\{%(p)d,2\\right\}$ (za $X$) in $\\left\{0,1,%(q)d\\right\}$ (za $Y$).
    Verjetnostna funkcija vektorja $(X,Y)$ je enaka""" %{"p": p, "q": q}
def resitev(p,q,i):

    if i==0:
        text1=""" $P(X=x,Y=y)=c(x^2+2y)$.</p>"""
    if i==1:
        text1=""" $P(X=x,Y=y)=c(e^x+y)$.</p>"""
    if i==2:
        text1=""" $P(X=x,Y=y)=c(x^4+y)$.</p>"""
    if i==3:
        text1=""" $P(X=x,Y=y)=c(\\frac{1}{x^2}+y)$.</p>"""

    text2="""
    <p>Izračunajte konstanto $c$. %(ans[0])s</p>

    <p>Izračunajte $P(XY\\geq 2)$. %(ans[1])s</p>

    <p>Koliko so konstante $u,v,z$ v naslednji shemi:
    $Y\\sim\\left(
    \\begin{array}{cc}0 \ & 1 \ & %(q)d\\\\ 
    u \ & v \ & z
    \\end{array}\\right)$? $u$: %(ans[2])s, $v$: %(ans[3])s, $z$: %(ans[4])s.</p>""" %{"p": p, "q": q, "ans[0]": ans[0],
                                             "ans[1]": ans[1],"ans[2]": ans[2],"ans[3]": ans[3], "ans[4]": ans[4]}


    return text+text1+text2

questions = ""
count = 1
for p in [-1]:
    for q in [2,4]:
        for i in range(0,4):
            text = naloga(p,q,i)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from math import log, exp, sqrt
from scipy.integrate import quad
from scipy.integrate import dblquad
from numpy import inf

import matplotlib


NAME = "Slučajni vektorji"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (slučajni vektorji)/6. skupina"
FILE_NAME = "vs_kviz_10_6_1"

text = """<p>Naj bosta $X$ in $Y$ neodvisni slučajni spremenljivki z gostotama"""
def resitev(i):

    if i==1:
        text1=""" $p_X=\\left\{\\begin{array}{cc} 2e^{-2x} & x>0\\\\ 0  & \\text{sicer} \\end{array}\\right.$ in
            $p_Y=\\left\{\\begin{array}{cc} cy^2 & 10>y>0\\\\ 0  & \\text{sicer} \\end{array}\\right..$ </p>"""
    if i==2:
        text1=""" $p_X=\\left\{\\begin{array}{cc} 3e^{-3x} & x>0\\\\ 0 \ & \\text{sicer} \\end{array}\\right.$ in 
            $p_Y=\\left\{\\begin{array}{cc} ce^{\\frac{1}{10}y}& 10>y>0\\\\ 0 \ & \\text{sicer} \\end{array}   \\right..$ </p>
            """
    if i==3:
        text1=""" $p_X=\\left\{\\begin{array}{cc} 4e^{-4x} & x>0\\\\ 0 \ & \\text{sicer} \\end{array}   \\right.$ in 
            $p_Y=\\left\{\\begin{array}{cc} c(y+e^{\\frac{1}{5}y}) & 10>y>0\\\\ 0 \ & \\text{sicer} \\end{array}   \\right..$ </p>
            """
    if i==4:
        text1=""" $p_X=\\left\{\\begin{array}{cc} e^{-x} & x>0\\\\ 0 \ & \\text{sicer} \\end{array}   \\right.$ in 
            $p_Y=\\left\{\\begin{array}{cc} ce^{-y} & 10>y>0\\\\ 0 \ & \\text{sicer} \\end{array}   \\right..$ </p>
            """

    funx=[lambda x: 2*exp(-2*x), lambda x: 3*exp(-3*x), lambda x: 4*exp(-4*x), lambda x: exp(-x)]
    funy=[lambda y: y**2, lambda y: exp(1.0/10*y),lambda y: y+exp(1.0/5*y),lambda y: exp(-y)]

    c=1/quad(funy[i-1],0,10)[0]
    p14=funx[i-1](1)*funy[i-1](4)
#   ver=c*dblquad(lambda y,x: funy[i-1](y)*funx[i-1](x), 0, 2, lambda x: 5*x, lambda x: 10)[0]
    up=quad(lambda x: funx[i-1](x)*x,0,inf)[0]
    upan=c*dblquad(lambda y,x: funy[i-1](y)*funx[i-1](x)*x*y, 0, inf, lambda x: 0, lambda x: 10)[0]
    ans=[c,p14,up,upan]
    print(i,ans)
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
          
    text2 = """<p>Koliko je $c$?  %(ans[0])s </p>

    <p>Koliko je $p_{X,Y}(1,4)$?  %(ans[1])s </p>

    <p>Izračunajte $E(X)$. %(ans[2])s </p>

    <p>Izračunajte $E(XY)$. %(ans[3])s </p>
    """ % {"ans[0]":ans[0],"ans[1]":ans[1],"ans[2]":ans[2],"ans[3]":ans[3]}

    return text+text1+text2

questions = ""
count = 1
for i in range(1,5):
        text = naloga(i)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

QUIZ.append(QuestionSet(text, parametri, resitev18, name="kroglice"))

with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import pi
import os
import sys
from scipy.stats import norm
from moodle_xml import Quiz, NumAnswer, QuestionSet, ShortAnswer

POGLAVJE = "07 normalna kratka"
CATEGORY = "Kratka vprašanja/{}".format(POGLAVJE)
QUIZ = Quiz(CATEGORY)

QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/številske"

text = "<p>Če je \(X\sim N({m},{s})\), je \(E(X)\) = {ans[0]} in \(D(x)\) = {ans[1]}</p>"


def resitev0(m, s):
    ans = [NumAnswer(m), NumAnswer(s**2)]
    return {"ans": ans}


parametri = [{"m": m, "s": s} for m in [5, -3, 8] for s in [3, 4, 5]]
QUIZ.append(QuestionSet(text, parametri, resitev0, name="parametri normalne"))

# Naloga
text = "<p>Če je \(X\sim N({m},{s})\), je \(P(X\le {x})=\Phi(\) {ans[0]} \()\).</p>"
def resitev1(m, s, x):
  return {"ans": [NumAnswer((x-m)/s)]}
parametri = [{"m": m, "s": s, "x": s*z+m} for m in [5, -2] for s in [3, 4] for z in [-1, 2, -2]]
QUIZ.append(QuestionSet(text, parametri, resitev1, name="P X le x normalna"))

# Naloga
text = "<p>Če je \(X\sim N({m},{s})\), je \(P(X>{x})=\) {ans[0]}.</p>"
def resitev2(m, s, x):
  return {"ans": [NumAnswer(1 - norm.cdf(x, loc=m, scale=s))]}
parametri = [{"m": m, "s": s, "x": s*z+m} for m in [5, -2] for s in [3, 4] for z in [-1, 2, -2]]
QUIZ.append(QuestionSet(text, parametri, resitev2, name="P(X ge x) normalna"))


# Naloga
text = """<p>Če sta \(X\sim N({m},{s})\) in \(Y\sim N({m1},{s1})\) neodvisni,
 je \(E({a}X+{b}Y)=\) {ans[0]} in \(D({a}X+{b}Y)=\) {ans[1]}.</p>"""
def resitev3(m, s, m1, s1, a, b):
  return {"ans": [NumAnswer(a) for a in (a*m+b*m1, a**2*s**2+b**2*s1**2)]}
parametri = [{"m": m, "s": s, "m1": m+s, "s1": s1, "a": m-s, "b": s1} for m in [5, -2] for s in [3, 2] for s1 in [ 2, 3]]
QUIZ.append(QuestionSet(text, parametri, resitev3, name="E(aX+bY) normalna"))


# Naloga
text = """<p>{p}-ti percentil za  \(X\sim N({m},{s})\),
 je \(x_{{0.{p}}}=\) {ans[0]}.</p>"""
def resitev4(m, s, p):
  return {"ans": [NumAnswer(norm.ppf(p/100, loc=m, scale=s), precision=0.01)]}
parametri = [{"m": m, "s": s, "p": p} for m in [4, -2] for s in [3, 5] for p in [90, 95, 99]]
QUIZ.append(QuestionSet(text, parametri, resitev4, name="percentil normalna"))


# Naloga
text = """<p>Za \(x\) velja 
\[P({m}-x \le X\le {m}+x) = {p} \]
za \(X\sim N({m},{s})\), potem je \(x=\) {ans[0]}.</p>"""
def resitev5(m, s, p):
  return {"ans": [NumAnswer(s*norm.ppf((1+p)/2), precision=0.01)]}
parametri = [{"m": m, "s": s, "p": p} for m in [-4, 3] for s in [2, 5] for p in [0.90, 0.95, 0.99]]
QUIZ.append(QuestionSet(text, parametri, resitev5, name="percentil interval normalna"))

# Naloga
text = """<p>Za \(X\sim N({m},{s})\) je 
\(P(X\in [{a},{b}]) = \) {ans[0]}.</p>"""
def resitev6(m, s, a, b):
  ans = norm.cdf(b, loc=m, scale=s) - norm.cdf(a, loc=m, scale=s)
  return {"ans": [NumAnswer(ans, precision=0.001)]}
parametri = [{"m": m, "s": s, "a": m-x*s, "b": m+x*(s-1)} for m in [-3, 4] for s in [3, 5] for x in [0.5, 1]]
QUIZ.append(QuestionSet(text, parametri, resitev6, name="P(X\in[a,b]) normalna"))

# Naloga
text = """<p>Za \(X\sim B({n},{p})\) uporabi Laplaceov približek in oceni 
\(P(X\in [{a},{b}]) = \) {ans[0]}.</p>"""
def resitev7(n, p, a, b):
  m =n*p
  s = (n*p*(1-p))**0.5
  ans = norm.cdf(b+0.5, loc=m, scale=s) - norm.cdf(a-0.5, loc=m, scale=s)
  return {"ans": [NumAnswer(ans, precision=0.001)]}
parametri = [{"n": n, "p": p, "a": round(n*p-a), "b": round(n*p+a)} for n in [100, 400] for p in [0.6, 0.4] for a in [5, 8]]
QUIZ.append(QuestionSet(text, parametri, resitev7, name="P(X\in[a,b]) binomska"))

# Naloga
text = """<p>\(X\sim B({n},{p})\) je približno porazdeljen \(N(\mu,\sigma)\) za
\(\mu = \) {ans[0]} in \(\sigma^2=\) {ans[1]}.</p>"""
def resitev8(n, p):
  m =n*p
  s2 = (n*p*(1-p))
  return {"ans": [NumAnswer(a) for a in (m, s2)]}
parametri = [{"n": n, "p": p} for n in [100, 400] for p in [0.6, 0.4, 0.7]]
QUIZ.append(QuestionSet(text, parametri, resitev8, name="binomska priblizno N(mu, sigma)"))

# Naloga
text = """<p>Če je \(z\) {p2}-ti percentil za $Z\sim N(0, 1)$, potem je 
\(P(-z\le Z\le z)=\) {ans[0]}.</p>"""
def resitev9(p, p2):
  ans = NumAnswer(p)
  return {"ans": [ans]}
parametri = [{"p": p, "p2": ((p/100+1)/2)*100} for p in [90, 95, 99]]
QUIZ.append(QuestionSet(text, parametri, resitev9, name="P(-z\le Z\le z)"))

QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/kratki odgovori"
# Naloga
text = "<p>Če je \(X\sim N(\mu,\sigma)\), je \(\mu=\) {ans[0]} in  \(\sigma^2=\){ans[1]}.</p>"

def resitev20():
    return {"ans": [ShortAnswer("E(X)"), ShortAnswer("D(X)")]}

parametri = [{}]
QUIZ.append(QuestionSet(text, parametri, resitev20, name="Normalna parametri"))

# Naloga
text = "<p>Če je \(X\sim N({m},{s})\), je \({a}X+{b}\sim \) {ans[0]}.</p>"
def resitev21(m, s, a, b):
  mu, sigma = a*m+b, abs(a*s)
  ans = ShortAnswer("N({mu},{sigma})".format(mu=mu, sigma=sigma))
  alt_ans = ShortAnswer("N({mu}, {sigma})".format(mu=mu, sigma=sigma), points=100)
  ans.partial_answer(alt_ans)
  return {"ans": [ans]}
parametri = [{"m": m, "s": s, "a": a, "b": s} for m in [5, -2] for s in [3, 4] for a in [2, 3, -3]]
QUIZ.append(QuestionSet(text, parametri, resitev21, name="linearna funkcija normalne"))

# Naloga
text = "<p>Če je \(P(X\le x)=\Phi({a}x+{b})\), je \(X\sim\) {ans[0]}.</p>"
def resitev22(a, b):
  sigma, mu = round(1/a), -round(b/a)
  ans = ShortAnswer("N({mu},{sigma})".format(mu=mu, sigma=sigma))
  alt_ans = ShortAnswer("N({mu}, {sigma})".format(mu=mu, sigma=sigma), points=100)
  ans.partial_answer(alt_ans)
  return {"ans": [ans]}
parametri = [{"a": 1/s, "b": -m/s} for m in [5, -3, 3] for s in [2, 4]]
QUIZ.append(QuestionSet(text, parametri, resitev22, name="P(X \le x)=Phi"))

# Naloga
text = """<p>Če je \(Z\sim N(0,1)\) potem je 
\(P(-a\le Z\le a)=\){ans[0]}. Vpiši poenostavljeno formulo brez presledkov. 
Namesto \(\Phi(x)\) vpiši <b>Fi(x)</b>! </p>"""
def resitev23():
  ans = ShortAnswer("2Fi(a)-1")
  alt_ans = ShortAnswer("-1+2Fi(a)", points=100)
  ans.partial_answer(alt_ans)
  return {"ans": [ans]}
parametri = [{}]
QUIZ.append(QuestionSet(text, parametri, resitev23, name="formula za P(-a le X le a) normalna"))

# Naloga
text = """<p>Če je \(z\) {p}-ti percentil za $X\sim N(2,3)$, uporabi \(z\) in izrazi
\(0.{p}=\) {ans[0]}. Vpiši izraz brez presledkov.</p>"""
def resitev24(p):
  ans = ShortAnswer("P(X<z)")
  ans.partial_answer(ShortAnswer("P(X<=z)", points=100))
  ans.partial_answer(ShortAnswer("P(z>=X)", points=100))
  ans.partial_answer(ShortAnswer("P(z>X)", points=100))
  return {"ans": [ans]}
parametri = [{"p": p} for p in [90, 95, 99]]
QUIZ.append(QuestionSet(text, parametri, resitev24, name="kvantili p=P(X le z)"))


# Naloga
text = """<p>Če je \(z\) {p2}-ti percentil za $X\sim N(0, 1)$, uporabi \(z\) in izrazi
\(0.{p}=P(\) {ans[0]} \()\). Vpiši izraz brez presledkov.</p>"""
def resitev25(p, p2):
  ans = ShortAnswer("-z<X<z")
  ans.partial_answer(ShortAnswer("-z<=X<=z", points=100))
  return {"ans": [ans]}
parametri = [{"p": p, "p2": ((p/100+1)/2)*100} for p in [90, 95, 99]]
QUIZ.append(QuestionSet(text, parametri, resitev25, name="kvantili p=P(-z\le X\le z)"))

with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())

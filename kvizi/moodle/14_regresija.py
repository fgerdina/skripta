#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "Regresijska premica {}"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (linearna regresija)/1. skupina"
FILE_NAME = "ovs_kviz_10_1"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

def naloga(v, t, prediction_v):
    text = """ <p>Biologi so izmerili višino (V) in težo (T) petih žiraf. Za višine so po vrsti 
    dobili rezultate \\( {v[0]}, {v[1]}, {v[2]}, {v[3]}, {v[4]} \\) (v metrih), za težo pa
    \\( {t[0]}, {t[1]}, {t[2]}, {t[3]}, {t[4]} \\) (v kilogramih).
    </p>
    
    <p>Izračunajte povprečno višino žiraf.&nbsp;{ans[0]} </p>
    
    <p>Izračunajte vzorčni standardni odklon za višino žirah (POZOR! Pri vzorčnem standardnem odklonu se deli z n-1).&nbsp;{ans[1]} </p>

    <p>Izračunajte vzorčno kovarianco (POZOR! Pri vzorčni kovarianci se deli z n-1). &nbsp;{ans[2]}</p>
        
    <p>Napovejte težo živali z višino {prediction_v} metrov. &nbsp;{ans[3]} </p>

    """
    mv, mt = mean(v), mean(t)    
    sv = std(v, ddof=1)
    st = std(t, ddof=1)                                        
    r = stats.pearsonr(v, t)
    cv = cov([v, t])[0][1]
    k = cv/sv**2
    prediction_t = mt + k*(prediction_v-mv)
    
    #print(v)
    #print(t)
    #print(prediction_v)
    #print("sv", sv)
    #print("cv", cv)
    
    ans = [mean(v), sv, cv, prediction_t]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    #print(ans)
    cloze = text.format(v=v, t=t, prediction_v=prediction_v, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
seed(0)
def get_data():
    mean_is_integer = False
    while not mean_is_integer:
        t = [round(normal(loc=4.8, scale=0.5), 1) for i in range(5)]
        mean_is_integer = mean(t).is_integer()

    mean_is_integer = False
    while not mean_is_integer:
        v = [int(e*200 + normal(loc=0, scale=100)) for e in t]
        mean_is_integer = mean(v).is_integer()
    pv = round(normal(loc=4.8, scale=0.5), 1)
    while pv in v:
        pv = round(normal(loc=4.8, scale=0.5), 1)
    return t, v, pv


questions = ""
count = 1



#for i in (0,1,2):
#    for up in (-1,1):
while(count<100):
    v, t, pv = get_data()
    text = naloga(v, t, pv)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "Regresijska premica {}"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (linearna regresija)/2. skupina"
FILE_NAME = "ovs_kviz_10_2"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

def naloga(v, t, prediction_t):
    text = """ <p>Zdravniki so v okviru rednega sistematskega pregleda stehtali in izmerili 5 učencev.
    Za višine so po vrsti dobili rezultate \\( {v[0]}, {v[1]}, {v[2]}, {v[3]}, {v[4]} \\) (v centimetrih),
    za teže pa \\( {t[0]}, {t[1]}, {t[2]}, {t[3]}, {t[4]} \\) (v kilogramih).
    </p>
    <p>Izračunajte povprečno težo učencev.&nbsp;{ans[0]} </p>
    
    <p>Izračunajte vzorčni standardni odklon za težo učencev (POZOR! Pri vzorčnem standardnem odklonu se deli z n-1).&nbsp;{ans[1]} </p>

    <p>Izračunajte vzorčno kovarianco (POZOR! Pri vzorčni kovarianci se deli z n-1). &nbsp;{ans[2]}</p>
        
    <p>Napovejte višino učenca (v centimerih) s težo {prediction_t} kilogramov. &nbsp;{ans[3]} </p>

    """
    mv, mt = mean(v), mean(t)    
    sv = std(v, ddof=1)
    st = std(t, ddof=1)                                        
    r = stats.pearsonr(v, t)
    cv = cov([v, t])[0][1]
    k = cv/st**2
    prediction_v = mv + k*(prediction_t-mt)
    
    #print("v", v)
    #print("t", t)
    #print("pt", prediction_t)
    #print("predicted", prediction_v)
    #print("sv", sv)
    #print("cv", cv)
    
    ans = [mean(t), st, cv, prediction_v]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    #print(ans)
    cloze = text.format(v=v, t=t, prediction_t=prediction_t, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
# Comment out for debugging
# seed(0)

def get_data():
    mean_is_integer = False
    while not mean_is_integer:
        t = [int(normal(loc=80, scale=10)) for i in range(5)]
        mean_is_integer = mean(t).is_integer()
    itm = 22
    mean_is_integer = False
    while not mean_is_integer:
        v = [int(((e/itm)**0.5)*100 + normal(loc=0, scale=10)) for e in t]
        mean_is_integer = mean(v).is_integer()
    pt = int(round(normal(loc=80, scale=10), 1))
    while pt in t:
        pt = int(round(normal(loc=80, scale=10), 1))
    return t, v, pt


questions = ""
count = 1



#for i in (0,1,2):
#    for up in (-1,1):
while(count<100):
    t, v, pt = get_data()
    text = naloga(v, t, pt)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "Regresijska premica {}"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (linearna regresija)/3. skupina"
FILE_NAME = "ovs_kviz_10_3"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

def naloga(v, t, prediction_t):
    text = """ <p>Študenti z inteligenčnimi kvocienti \\( {v[0]}, {v[1]}, {v[2]}, {v[3]}, {v[4]} \\)
    so na matematičnem testu dosegli rezultate \\( {t[0]}, {t[1]}, {t[2]}, {t[3]}, {t[4]} \\).
    </p>
    <p>Izračunajte povprečni rezultat učencev na testu.&nbsp;{ans[0]} </p>
    
    <p>Izračunajte vzorčni standardni odklon za rezultat na testu(POZOR! Pri vzorčnem standardnem odklonu se deli z n-1).&nbsp;{ans[1]} </p>

    <p>Izračunajte vzorčno kovarianco (POZOR! Pri vzorčni kovarianci se deli z n-1). &nbsp;{ans[2]}</p>
        
    <p>Napovejte inteligenčni kvocient študenta, ki je na testu dosegel rezultat {prediction_t} . &nbsp;{ans[3]} </p>

    """
    mv, mt = mean(v), mean(t)    
    sv = std(v, ddof=1)
    st = std(t, ddof=1)                                        
    r = stats.pearsonr(v, t)
    cv = cov([v, t])[0][1]
    k = cv/st**2
    prediction_v = mv + k*(prediction_t-mt)
    
    print("v", v)
    print("t", t)
    print("pt", prediction_t)
    print("predicted", prediction_v)
    print("sv", sv)
    print("cv", cv)
    
    ans = [mean(t), st, cv, prediction_v]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    #print(ans)
    cloze = text.format(v=v, t=t, prediction_t=prediction_t, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
# Comment out for debugging
# seed(0)

def get_data():
    mean_is_integer = False
    while not mean_is_integer:
        t = [int(normal(loc=80, scale=10)) for i in range(5)]
        mean_is_integer = mean(t).is_integer()
    itm = 22
    mean_is_integer = False
    while not mean_is_integer:
        v = [int(((e/itm)**0.5)*100 + normal(loc=0, scale=10)) - 80 for e in t]
        mean_is_integer = mean(v).is_integer()
    pt = int(round(normal(loc=80, scale=10), 1))
    while pt in t:
        pt = int(round(normal(loc=80, scale=10), 1))
    return t, v, pt


questions = ""
count = 1



#for i in (0,1,2):
#    for up in (-1,1):
while(count<100):
    t, v, pt = get_data()
    text = naloga(v, t, pt)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "Regresijska premica {}"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (linearna regresija)/4. skupina"
FILE_NAME = "ovs_kviz_10_4"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

def naloga(v, t, prediction_v):
    text = """ <p>Biologi so izmerili dolžino glave in težo za pet kokoši. Za dolžino glave so po vrsti 
    dobili rezultate \\( {v[0]}, {v[1]}, {v[2]}, {v[3]}, {v[4]} \\) (v centimetrih), za težo pa
    \\( {t[0]}, {t[1]}, {t[2]}, {t[3]}, {t[4]} \\) (v gramih).
    </p>
    
    <p>Izračunajte povprečno dolžino glave kokoši.&nbsp;{ans[0]} </p>
    
    <p>Izračunajte vzorčni standardni odklon za dolžino glave (POZOR! Pri vzorčnem standardnem odklonu se deli z n-1).&nbsp;{ans[1]} </p>

    <p>Izračunajte vzorčno kovarianco (POZOR! Pri vzorčni kovarianci se deli z n-1). &nbsp;{ans[2]}</p>
        
    <p>Napovejte težo kokoši z dolžino glave {prediction_v} centimetrov. &nbsp;{ans[3]} </p>

    """
    mv, mt = mean(v), mean(t)    
    sv = std(v, ddof=1)
    st = std(t, ddof=1)                                        
    r = stats.pearsonr(v, t)
    cv = cov([v, t])[0][1]
    k = cv/sv**2
    prediction_t = mt + k*(prediction_v-mv)
    
    #print(v)
    #print(t)
    #print(prediction_v)
    #print("sv", sv)
    #print("cv", cv)
    
    ans = [mean(v), sv, cv, prediction_t]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    #print(ans)
    cloze = text.format(v=v, t=t, prediction_v=prediction_v, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
seed(0)
def get_data():
    mean_is_integer = False
    while not mean_is_integer:
        t = [round(normal(loc=4.8, scale=0.5), 1) for i in range(5)]
        mean_is_integer = mean(t).is_integer()

    mean_is_integer = False
    while not mean_is_integer:
        v = [int(e*200 + normal(loc=0, scale=100)) for e in t]
        mean_is_integer = mean(v).is_integer()
    pv = round(normal(loc=4.8, scale=0.5), 1)
    while pv in v:
        pv = round(normal(loc=4.8, scale=0.5), 1)
    return t, v, pv


questions = ""
count = 1



#for i in (0,1,2):
#    for up in (-1,1):
while(count<100):
    v, t, pv = get_data()
    text = naloga(v, t, pv)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64


from scipy import stats
from numpy import cov, std, mean

NAME = "Regresijska premica {}"
CATEGORY = "Kvizi za oceno/Kviz, 10. teden, 9. kviz (linearna regresija)/5. skupina"
FILE_NAME = "ovs_kviz_10_5"

#    \\(P(X=1,y=a)={verj1}\\), \\(P(X=1,y=2)={verj2}\\),
#    \\(P(X=2,y=a)={verj3}\\), \\(P(X=2,y=2)={verj4}\\),
#    \\(P(X=3,y=a)={verj5}\\).</p>

def naloga(v, t, prediction_v):
    text = """ <p>Pet avtomobilov je za pot po vrsti porabilo sledečo količino goriva
     \\( {v[0]}, {v[1]}, {v[2]}, {v[3]}, {v[4]} \\) (v litrih), njiho skupna masa pa je znašala
    \\( {t[0]}, {t[1]}, {t[2]}, {t[3]}, {t[4]} \\) (v kilogramih).
    </p>
    
    <p>Izračunajte povprečno porabo goriva na poti.&nbsp;{ans[0]} </p>
    
    <p>Izračunajte vzorčni standardni odklon za porabo goriva na poti (POZOR! Pri vzorčnem standardnem odklonu se deli z n-1).&nbsp;{ans[1]} </p>

    <p>Izračunajte vzorčno kovarianco (POZOR! Pri vzorčni kovarianci se deli z n-1). &nbsp;{ans[2]}</p>
        
    <p>Napovejte skupno težo avtomobila s porabo goriva {prediction_v} litrov. &nbsp;{ans[3]} </p>

    """
    mv, mt = mean(v), mean(t)    
    sv = std(v, ddof=1)
    st = std(t, ddof=1)                                        
    r = stats.pearsonr(v, t)
    cv = cov([v, t])[0][1]
    k = cv/sv**2
    prediction_t = mt + k*(prediction_v-mv)
    
    #print(v)
    #print(t)
    #print(prediction_v)
    #print("sv", sv)
    #print("cv", cv)
    
    ans = [mean(v), sv, cv, prediction_t]
    ans = ["{{1:NUMERICAL:={}:0.01}}".format(a) for a in ans]
    #print(ans)
    cloze = text.format(v=v, t=t, prediction_v=prediction_v, ans=ans)
    return cloze


QUESTION_TEMPLATE = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

from numpy.random import normal, seed
seed(0)
def get_data():
    mean_is_integer = False
    while not mean_is_integer:
        t = [round(normal(loc=4.8, scale=0.5), 1) for i in range(5)]
        mean_is_integer = mean(t).is_integer()

    mean_is_integer = False
    while not mean_is_integer:
        v = [int(e*200 + normal(loc=0, scale=100)) for e in t]
        mean_is_integer = mean(v).is_integer()
    pv = round(normal(loc=4.8, scale=0.5), 1)
    while pv in v:
        pv = round(normal(loc=4.8, scale=0.5), 1)
    return t, v, pv


questions = ""
count = 1



#for i in (0,1,2):
#    for up in (-1,1):
while(count<100):
    v, t, pv = get_data()
    text = naloga(v, t, pv)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import randint, seed

NAME = "frekvence {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/poskusni"
FILE_NAME = "ovs_kviz_6_0"

text = """ <p>Dano je besedilo, sestavljeno iz črk <b>{crke}</b>:
    <pre>{besedilo}
    </pre>
    Določi frekvence (št. pojavitev) za posamezne črke v besedilu:
    {frekvence}
    <p>Kolikšen je delež črke <b>{crka}</b>?&nbsp;{ans[0]}</p>

    <p>Kolikšen je delež <b>prvih {k} črk</b>?{ans[1]}</p>"""
def resitev(crke, k, r_seed):
    seed(r_seed)
    m = len(crke)
    besedilo = [crke[randint(0, m-1)] for _ in range(500)]
    besedilo = "".join(besedilo)
    frekvence = ""
    for crka in crke:
        frekvence += "<b>{crka}</b>: {{1:NUMERICAL:={freq}:0.1}}, ".format(
            freq=besedilo.count(crka), crka=crka)
    ans = [besedilo.count(crke[k])/len(besedilo),
           sum(besedilo.count(crke[_]) for _ in range(k))/len(besedilo)]
    ans = ["{{{}:NUMERICAL:={}:0.001}}".format(m, a) for a in ans]
    infd = 'INPUTLINE'
    cloze = text.format(crke=crke, k=k, crka=crke[k], besedilo=besedilo,
                        frekvence=frekvence, ans=ans)
    return cloze, text

question_template = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for crke in ("ABCD", "PQRST"):
    for k in (2, 3):
        for r_seed in (123, 456):
            cloze, html = naloga(crke, k, r_seed)
            questions += question_template.format(name=NAME.format(count), text=cloze)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import randint, seed

NAME = "frekvence {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/poskusni"
FILE_NAME = "ovs_kviz_6_0"

text = """ <p>Dano je besedilo, sestavljeno iz črk <b>{crke}</b>:
    <pre>{besedilo}
    </pre>
    Določi frekvence (št. pojavitev) za posamezne črke v besedilu:
    {frekvence}
    <p>Kolikšen je delež črke <b>{crka}</b>?&nbsp;{ans[0]}</p>

    <p>Kolikšen je delež <b>prvih {k} črk</b>?{ans[1]}</p>"""
def resitev(crke, k, r_seed):
    seed(r_seed)
    m = len(crke)
    besedilo = [crke[randint(0, m-1)] for _ in range(500)]
    besedilo = "".join(besedilo)
    frekvence = ""
    for crka in crke:
        frekvence += "<b>{crka}</b>: {{1:NUMERICAL:={freq}:0.1}}, ".format(
            freq=besedilo.count(crka), crka=crka)
    ans = [besedilo.count(crke[k])/len(besedilo),
           sum(besedilo.count(crke[_]) for _ in range(k))/len(besedilo)]
    ans = ["{{{}:NUMERICAL:={}:0.001}}".format(m, a) for a in ans]
    infd = 'INPUTLINE'
    cloze = text.format(crke=crke, k=k, crka=crke[k], besedilo=besedilo,
                        frekvence=frekvence, ans=ans)
    return cloze, text

question_template = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for crke in ("ABCD", "PQRST"):
    for k in (2, 3):
        for r_seed in (123, 456):
            cloze, html = naloga(crke, k, r_seed)
            questions += question_template.format(name=NAME.format(count), text=cloze)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import random, randint, seed

NAME = "frekvence števil {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/1. skupina"
FILE_NAME = "ovs_kviz_6_1"

text = """ <p>Dan je vzorec številskih vrednosti:
    <pre>{besedilo}
    </pre>
    Določi frekvence (št. pojavitev) za posamezne vrednosti:
    <p>{frekvence}</p>
    <p>Iz vzorca naključno izberemo eno število.</p>
    <p>Kolikšna je verjetnost, da bo izbrana vrednost <b>{n}</b>?&nbsp;{ans[0]}</p>

    <p>Kolikšen je verjetnost, da bo izbrana vrednost manjša ali enaka <b>{k}</b>?{ans[1]}</p>"""
def resitev(m, n, r_seed):
    seed(r_seed)
    vzorec = [round(m*random()**2) for _ in range(randint(500, 550))]
    besedilo = ",".join(map(str, vzorec))
    n_vzorca = len(vzorec)
    frekvence = [vzorec.count(_) for _ in range(m+1)]
    frekvence_text = ", ".join("<b>{}</b>: {{1:NUMERICAL:={}:0.1}}".format(i, vzorec.count(i))
                              for i in range(m+1))
    ans = [frekvence[n]/n_vzorca, sum(frekvence[_] for _ in range(n))/n_vzorca]
    ans = ["{{{}:NUMERICAL:={}:0.001}}".format(m+1, a) for a in ans]
    cloze = text.format(n=n, k=n-1, besedilo=besedilo,
                        frekvence=frekvence_text, ans=ans)
    return cloze, text


questions = ""
count = 1
for m in (3,):
    for n in (2, 3):
        for r_seed in (123, 456, 765):
            cloze, html = naloga(m, n, r_seed)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import random, randint, seed

NAME = "frekvence števil {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/1. skupina"
FILE_NAME = "ovs_kviz_6_1"

text = """ <p>Dan je vzorec številskih vrednosti:
    <pre>{besedilo}
    </pre>
    Določi frekvence (št. pojavitev) za posamezne vrednosti:
    <p>{frekvence}</p>
    <p>Iz vzorca naključno izberemo eno število.</p>
    <p>Kolikšna je verjetnost, da bo izbrana vrednost <b>{n}</b>?&nbsp;{ans[0]}</p>

    <p>Kolikšen je verjetnost, da bo izbrana vrednost manjša ali enaka <b>{k}</b>?{ans[1]}</p>"""
def resitev(m, n, r_seed):
    seed(r_seed)
    vzorec = [round(m*random()**2) for _ in range(randint(500, 550))]
    besedilo = ",".join(map(str, vzorec))
    n_vzorca = len(vzorec)
    frekvence = [vzorec.count(_) for _ in range(m+1)]
    frekvence_text = ", ".join("<b>{}</b>: {{1:NUMERICAL:={}:0.1}}".format(i, vzorec.count(i))
                              for i in range(m+1))
    ans = [frekvence[n]/n_vzorca, sum(frekvence[_] for _ in range(n))/n_vzorca]
    ans = ["{{{}:NUMERICAL:={}:0.001}}".format(m+1, a) for a in ans]
    cloze = text.format(n=n, k=n-1, besedilo=besedilo,
                        frekvence=frekvence_text, ans=ans)
    return cloze, text


questions = ""
count = 1
for m in (3,):
    for n in (2, 3):
        for r_seed in (123, 456, 765):
            cloze, html = naloga(m, n, r_seed)
            questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
            count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import binom

NAME = "binomska M in M {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/2. skupina"
FILE_NAME = "ovs_kviz_6_2"

text = """ <p>Za določene vrste bonbonov je <b>{p}</b> procentov bonbonov
    <b>{barva}</b> barve. Iz več vrečk na slepo izberemo natanko <b>{n}</b>
    bonbonov. Naj bo X število bonbonov {barva} barve. Izračunaj verjetnosti za
    naslednje vrednosti X

    <p>{tabela}</p>


    <p>Kolikšna je verjetnost, da bo število bonbonov {barva} barve <b>manj kot {m}</b>?&nbsp;{ans[0]}</p>

    <p>Kolikšen je verjetnost, da bo število X med  <b>{k}</b> in <b>{kk}</b>(vključno z obema mejama)?{ans[1]}</p>"""
def resitev(n, p, m, barva):

    mu = round(n*p)
    k = mu - 5
    kk = mu + 10
    x = [mu+i for i in range(-1, 2)]

    tabela = ", ".join("P(X=<b>{}</b>): {{1:NUMERICAL:={}:0.00001}}".format(i, binom.pmf(i, n, p))
                       for i in x)
    ans = [binom.cdf(m-1, n, p), sum(binom.pmf(_, n, p) for _ in range(k, kk+1))]
    ans = ["{{3:NUMERICAL:={}:0.00001}}".format(a) for a in ans]
    cloze = text.format(p=round(p*100), n=n, m=m, tabela=tabela, k=k, kk=kk, barva=barva, ans=ans)
    return cloze, text


questions = ""
count = 1
BARVE = (("modre", 24), ("rjave", 14), ("zelene", 16))
for n in (100, 150):
    for barva in BARVE:
        barva, p = barva
        p = p/100
        m = round(n*p) - 3
        cloze, html = naloga(n, p, m, barva)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import binom

NAME = "binomska M in M {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/2. skupina"
FILE_NAME = "ovs_kviz_6_2"

text = """ <p>Za določene vrste bonbonov je <b>{p}</b> procentov bonbonov
    <b>{barva}</b> barve. Iz več vrečk na slepo izberemo natanko <b>{n}</b>
    bonbonov. Naj bo X število bonbonov {barva} barve. Izračunaj verjetnosti za
    naslednje vrednosti X

    <p>{tabela}</p>


    <p>Kolikšna je verjetnost, da bo število bonbonov {barva} barve <b>manj kot {m}</b>?&nbsp;{ans[0]}</p>

    <p>Kolikšen je verjetnost, da bo število X med  <b>{k}</b> in <b>{kk}</b>(vključno z obema mejama)?{ans[1]}</p>"""
def resitev(n, p, m, barva):

    mu = round(n*p)
    k = mu - 5
    kk = mu + 10
    x = [mu+i for i in range(-1, 2)]

    tabela = ", ".join("P(X=<b>{}</b>): {{1:NUMERICAL:={}:0.00001}}".format(i, binom.pmf(i, n, p))
                       for i in x)
    ans = [binom.cdf(m-1, n, p), sum(binom.pmf(_, n, p) for _ in range(k, kk+1))]
    ans = ["{{3:NUMERICAL:={}:0.00001}}".format(a) for a in ans]
    cloze = text.format(p=round(p*100), n=n, m=m, tabela=tabela, k=k, kk=kk, barva=barva, ans=ans)
    return cloze, text


questions = ""
count = 1
BARVE = (("modre", 24), ("rjave", 14), ("zelene", 16))
for n in (100, 150):
    for barva in BARVE:
        barva, p = barva
        p = p/100
        m = round(n*p) - 3
        cloze, html = naloga(n, p, m, barva)
        questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=cloze)
        count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import random, randint, seed

NAME = "frekvenčna tabela {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/3. skupina"
FILE_NAME = "ovs_kviz_6_3"

text = """ <p>Za vzorec števil med <b>0</b> in <b>{n}</b> so dane frekvence (število
    pojavitev): <pre> {tabela} </pre>

    <p>Določi velikost vzorca: {ans[0]}</p>

    <p>Iz vzorca naključno izberemo eno število.</p> <p>Določi verjetnosti, da
    bodo izbrane vrednosti {verjetnosti}</p>

    <p>Kolikšna je verjetnost, da bo izbrana vrednost <b>manjša ali enaka
    {k}</b>?{ans[1]}</p> """
def resitev(r_seed):
    seed(r_seed)
    n = randint(70, 90)
    k = randint(n//2, 3*n//4)
    x_vrednosti = tuple(range(n+1))
    frekvence = [randint(0, 10+x) for x in x_vrednosti]
    tabela = ",".join("{}".format(f) for f in frekvence)
    n_vzorca = sum(frekvence)
    x_0 = randint(10, n - 5)
    izbrani_x = [i + x_0 for i in (-1, 0, 1)]
    izbrani_p = [(x, frekvence[x]/n_vzorca) for x in izbrani_x]
    verjetnosti = "<b>{}</b>:\n".format(", ".join(str(x) for x in izbrani_x))
    verjetnosti += ", ".join("P(X={}) = {{1:NUMERICAL:={}:0.0000001}}".format(*p)
                             for p in izbrani_p)
    ans = [n_vzorca, sum(frekvence[_] for _ in range(k+1))/n_vzorca]
    ans = ["{{3:NUMERICAL:={}:0.00001}}".format(a) for a in ans]
    cloze = text.format(n=n, k=k, verjetnosti=verjetnosti,
                        tabela=tabela, ans=ans)
    return cloze


questions = ""
count = 1
for r_seed in (123, 456, 765, 6561, 7623, 1265):
    question = naloga(r_seed)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=question)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from random import random, randint, seed

NAME = "frekvenčna tabela {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/3. skupina"
FILE_NAME = "ovs_kviz_6_3"

text = """ <p>Za vzorec števil med <b>0</b> in <b>{n}</b> so dane frekvence (število
    pojavitev): <pre> {tabela} </pre>

    <p>Določi velikost vzorca: {ans[0]}</p>

    <p>Iz vzorca naključno izberemo eno število.</p> <p>Določi verjetnosti, da
    bodo izbrane vrednosti {verjetnosti}</p>

    <p>Kolikšna je verjetnost, da bo izbrana vrednost <b>manjša ali enaka
    {k}</b>?{ans[1]}</p> """
def resitev(r_seed):
    seed(r_seed)
    n = randint(70, 90)
    k = randint(n//2, 3*n//4)
    x_vrednosti = tuple(range(n+1))
    frekvence = [randint(0, 10+x) for x in x_vrednosti]
    tabela = ",".join("{}".format(f) for f in frekvence)
    n_vzorca = sum(frekvence)
    x_0 = randint(10, n - 5)
    izbrani_x = [i + x_0 for i in (-1, 0, 1)]
    izbrani_p = [(x, frekvence[x]/n_vzorca) for x in izbrani_x]
    verjetnosti = "<b>{}</b>:\n".format(", ".join(str(x) for x in izbrani_x))
    verjetnosti += ", ".join("P(X={}) = {{1:NUMERICAL:={}:0.0000001}}".format(*p)
                             for p in izbrani_p)
    ans = [n_vzorca, sum(frekvence[_] for _ in range(k+1))/n_vzorca]
    ans = ["{{3:NUMERICAL:={}:0.00001}}".format(a) for a in ans]
    cloze = text.format(n=n, k=k, verjetnosti=verjetnosti,
                        tabela=tabela, ans=ans)
    return cloze


questions = ""
count = 1
for r_seed in (123, 456, 765, 6561, 7623, 1265):
    question = naloga(r_seed)
    questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=question)
    count += 1


# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.special import binom

NAME = "Bayes spam {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/4. skupina"
FILE_NAME = "ovs_kviz_6_4"

text = """ <p>Med vso pošto je <b>{s}</b> procentov nezaželene. Med nezaželeno pošto vsako <b>{i_s}</b>
    sporočilo vsebuje besedo <b>invoice</b>. Delež sporočil z besedo <b>invoice</b> med zaželeno pošto je <b>{i_h}</b>.
    Dobimo novo sporočilo.
    </p>

    <p> Kolikšna je verjetnost, da sporočilo vsebuje besedo <b>invoice</b>?&nbsp; {ans[0]}</p>

    <p>Kolikšna je verjetnost, da je sporočilo <b>zaželeno</b> in vsebuje besedo <b>invoice</b>? {ans[1]}</p>

    <p>Opazimo, da sporočilo vsebuje besedo <b>invoice</b>. Kolikšna je verjetnost, da je sporočilo <b>nezaželeno</b>?&nbsp;{ans[2]}</p> """
def resitev(s, i_s, i_h):
    spam = s/100
    ham = 1 - spam
    invoice = i_s*spam + i_h*ham
    i_and_ham = i_h*ham
    spam_invoice = i_s*spam/invoice
    ans =  [invoice, i_and_ham, spam_invoice]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    infd = 'INPUTLINE'
    cloze = text.format(s=s, i_s=i_s, i_h=i_h, ans=ans)
    html = text.format(s=infd, i_s=infd, i_h=infd, ans=[infd for i in ans])
    return cloze, html

question_template = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for s in [60, 40]:
    for i_s in [0.2, 0.25]:
        for i_h in [0.02, 0.03]:
            cloze, html = naloga(s, i_s, i_h)
            questions += question_template.format(name=NAME.format(count), text=cloze)
            count += 1



#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.special import binom

NAME = "Bayes spam {}"
CATEGORY = "Kvizi za oceno/Kviz, 6. teden (binomska, histogrami)/4. skupina"
FILE_NAME = "ovs_kviz_6_4"

text = """ <p>Med vso pošto je <b>{s}</b> procentov nezaželene. Med nezaželeno pošto vsako <b>{i_s}</b>
    sporočilo vsebuje besedo <b>invoice</b>. Delež sporočil z besedo <b>invoice</b> med zaželeno pošto je <b>{i_h}</b>.
    Dobimo novo sporočilo.
    </p>

    <p> Kolikšna je verjetnost, da sporočilo vsebuje besedo <b>invoice</b>?&nbsp; {ans[0]}</p>

    <p>Kolikšna je verjetnost, da je sporočilo <b>zaželeno</b> in vsebuje besedo <b>invoice</b>? {ans[1]}</p>

    <p>Opazimo, da sporočilo vsebuje besedo <b>invoice</b>. Kolikšna je verjetnost, da je sporočilo <b>nezaželeno</b>?&nbsp;{ans[2]}</p> """
def resitev(s, i_s, i_h):
    spam = s/100
    ham = 1 - spam
    invoice = i_s*spam + i_h*ham
    i_and_ham = i_h*ham
    spam_invoice = i_s*spam/invoice
    ans =  [invoice, i_and_ham, spam_invoice]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    infd = 'INPUTLINE'
    cloze = text.format(s=s, i_s=i_s, i_h=i_h, ans=ans)
    html = text.format(s=infd, i_s=infd, i_h=infd, ans=[infd for i in ans])
    return cloze, html

question_template = """
<question type="cloze">
    <name>
      <text>{name}</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{text}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <penalty>0.3333333</penalty>
    <hidden>0</hidden>
  </question>
"""

questions = ""
count = 1
for s in [60, 40]:
    for i_s in [0.2, 0.25]:
        for i_h in [0.02, 0.03]:
            cloze, html = naloga(s, i_s, i_h)
            questions += question_template.format(name=NAME.format(count), text=cloze)
            count += 1



# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt

NAME = "Izpit - normalna {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/2. skupina"
FILE_NAME = "ovs_kviz_9_2"

text = """ <p>
    Naj normalno porazdeljeni slučajni spremenljivki <b>X</b> in <b>Y</b> predstavljata
    število točk, ki jih študentje dosežejo na izpitu pri predmetu <b>A</b> (<b>X</b>) in <b>B</b> (<b>Y</b>),
    pri čemer ima <b>X</b> povprečje <b>{mu1}</b> in standardni odklon <b>{sigma1}</b>,
    <b>Y</b> pa povprečje <b>{mu2}</b> in standardni odklon <b>{sigma2}</b>. Izpit opravimo, če zberemo vsaj <b>50</b> točk.</p>

    <p>Verjetnost, da bomo na izpitu predmeta <b>B</b> zbrali vsaj <b>2</b>-krat več točk kot na izpitu predmeta <b>A</b> je naslednja:
    {{1:MC:P(Y>{c})~P(Y>X/2)~=P(Y>2X)~P(X>2Y)~P(X>Y/2)}}</p>
    
    <p>Koliko je verjetnost, da študent <b>opravi</b> izpit pri predmetu <b>A</b>?&nbsp;{ans[0]} </p>

    <p>Kolikšna je verjetnost, da študent <b>ne opravi</b> izpita pri predmetu <b>B</b>?&nbsp;{ans[1]} </p>

    <p>Kolikšna je verjetnost, da študent <b>opravi</b> izpita pri obeh predmetih?&nbsp;{ans[2]} </p>

    """
def resitev(mu1,mu2,sigma1,sigma2):

    c=2*mu1
    stdA=1.0*(50-mu1)/sigma1
    oprA=1-norm.cdf(stdA)

    stdB=1.0*(50-mu2)/sigma2
    neoprB=norm.cdf(stdB)
    oprB=1-neoprB

    oprOba=oprA*oprB

    ans = [oprA,neoprB,oprOba]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2, sigma1=sigma1,sigma2=sigma2,c=c,ans=ans)
    return cloze



questions = ""
count = 1
for mu1 in (52,55):
    for mu2 in (48,51):
        for sigma1 in [10]:
            for sigma2 in [14]:
                text = naloga(mu1,mu2,sigma1,sigma2)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt

NAME = "Zamuda - normalna {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/3. skupina"
FILE_NAME = "ovs_kviz_9_3"

text = """ <p>Torkove vaje za OVS se začnejo ob <b>9:15</b>.
    Naj normalno porazdeljeni slučajni spremenljivki <b>X</b> in <b>Y</b> predstavljata
    število minut čez <b>9</b>, ko na vaje prideta Kaja (<b>X</b>) in Zoja (<b>Y</b>),
    pri čemer sta njuna prihoda neodvisna drug od drugega.
    <b>X</b> ima povprečje <b>{mu1}</b> in standardni odklon <b>{sigma1}</b>,
    <b>Y</b> pa ima povprečje <b>{mu2}</b> in standardni odklon <b>{sigma2}</b>. </p>

    <p>Kolikšna je verjetnost, da bo Kaja zamudila na vaje?&nbsp;{ans[0]} </p>
    
    <p>Kolikšna je verjetnost, da bo Zoja prišla na vaje pravočasno?&nbsp;{ans[1]} </p>

    <p>Kolikšna je verjetnost, da bosta obe zamudili na vaje?&nbsp;{ans[2]} </p>

    <p>Verjetnost, da bo Zoja prišla na vaje pred Kajo je naslednja:
    {{1:MC:P(X+Y>0)~=P(X>Y)~P(Y<15)~P(X-Y<{mu1})~P(X>Y+1)~P(Y>X)}}</p>

    """
def resitev(mu1,mu2,sigma1,sigma2):

    stdKaj=1.0*(15-mu1)/sigma1
    zamKaj=1-norm.cdf(stdKaj)

    stdZoj=1.0*(15-mu2)/sigma2
    pravZoj=norm.cdf(stdZoj)
    zamZoj=1-pravZoj

    zamOba=zamKaj*zamZoj

    ans = [zamKaj,pravZoj,zamOba]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2, sigma1=sigma1,sigma2=sigma2,ans=ans)
    return cloze



questions = ""
count = 1
for mu1 in (11,12):
    for mu2 in (10, 13):
        for sigma1 in [2]:
            for sigma2 in [3]:
                text = naloga(mu1,mu2,sigma1,sigma2)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt

NAME = "Zamuda - normalna {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/4. skupina"
FILE_NAME = "ovs_kviz_9_4"

text = """ <p>Torkove vaje za OVS se začnejo ob <b>11:15</b>.
    Naj normalno porazdeljeni slučajni spremenljivki <b>X</b> in <b>Y</b> predstavljata
    število minut čez <b>11</b>, ko na vaje prideta Karmen (<b>X</b>) in
    Tara (<b>Y</b>),
    pri čemer sta njuna prihoda neodvisna drug od drugega.
    <b>X</b> ima povprečje <b>{mu1}</b> in standardni odklon <b>{sigma1}</b>,
    <b>Y</b> pa ima povprečje <b>{mu2}</b> in standardni odklon <b>{sigma2}</b>. </p>

    <p>Verjetnost, da bo Karmen prišla na vaje pred Taro je naslednja:
    {{1:MC:P(X+Y>0)~P(X>Y)~P(Y<15)~P(X-Y<{mu1})~P(X>Y+1)~=P(Y>X)}}</p>
    
    <p>Kolikšna je verjetnost, da bo Karmen zamudila za več kot 2 minuti?&nbsp;{ans[0]} </p>
    
    <p>Kolikšna je verjetnost, da bo Tara prišla na vaje pred 11:14?&nbsp;{ans[1]} </p>

    <p>Kolikšna je verjetnost, da Tara pride na vaje med 11:{mu2} in 11:14?&nbsp;{ans[2]} </p>

    """
def resitev(mu1,mu2,sigma1,sigma2):

    stdKar=1.0*(17-mu1)/sigma1
    zamKar=1-norm.cdf(stdKar)

    stdTar=1.0*(14-mu2)/sigma2
    pravTar=norm.cdf(stdTar)
    
    vmesTar=pravTar-1.0/2


    ans = [zamKar,pravTar,vmesTar]
    ans = ["{{1:NUMERICAL:={}:0.001}}".format(a) for a in ans]
    cloze = text.format(mu1=mu1, mu2=mu2, sigma1=sigma1,sigma2=sigma2,ans=ans)
    return cloze



questions = ""
count = 1
for mu1 in (11,12):
    for mu2 in (10, 13):
        for sigma1 in [2]:
            for sigma2 in [3]:
                text = naloga(mu1,mu2,sigma1,sigma2)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import base64
from scipy.stats import norm
from math import sqrt

NAME = "Srčni utrip - normalna {}"
CATEGORY = "Kvizi za oceno/Kviz, 9. teden, 8. kviz (zvezne porazdelitve)/5. skupina"
FILE_NAME = "ovs_kviz_9_5"

text = """ <p>Srčni utrip v neki populaciji je porazdeljen normalno s povprečjem <b>{mu}</b>
    in standardnim odklonom <b>{sigma}</b>.</p>
    
    <p>Kolikšen delež ljudi ima utrip pod <b>{c}</b>?&nbsp;{ans[0]} </p>

    <p>Kolikšen delež ljudi ima utrip nad <b>{d}</b>?&nbsp;{ans[1]} </p>

    <p>Neodvisno izberemo dve osebi iz populacije.</p>

    <p>Pričakovana vsota utripov teh dveh oseb je &nbsp;{ans[2]}, pričakovan standardni odklon vsote pa &nbsp;{ans[3]}.</p> 

    <p> Kakšna je verjetnost, da ima ena izmed izbranih oseb
    utrip pod <b>{c}</b>, druga pa nad <b>{d}</b>?&nbsp;{ans[4]} </p>

    """
def resitev(mu,sigma):

    std1=1.0*(-5)/sigma
    pod=norm.cdf(std1)

    std2=1.0*4/sigma
    nad=1-norm.cdf(std2)

    vsota=2*mu

    odklon=sqrt(2)*sigma
    
    podNad=2*pod*nad

    ans=["{{2:NUMERICAL:={}:0.001}}".format(pod),"{{2:NUMERICAL:={}:0.001}}".format(nad),
        "{{1:NUMERICAL:={}:0.0001}}".format(vsota),"{{1:NUMERICAL:={}:0.0001}}".format(odklon),"{{2:NUMERICAL:={}:0.0001}}".format(podNad)]
    cloze = text.format(c=mu-5,d=mu+4,mu=mu, sigma=sigma,ans=ans)
    return cloze



questions = ""
count = 1
for mu in [60,65,70]:
        for sigma in [4,5]:
                text = naloga(mu,sigma)
                questions += QUESTION_TEMPLATE.format(name=NAME.format(count), text=text)
                count += 1



# moodle XML export

MOODLE_XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<quiz>
<!-- question: 0  -->
  <question type="category">
    <category>
        <text>$course$/{category}</text>

    </category>
  </question>

  <question type="description">
    <name>
      <text>python code</text>
    </name>
    <questiontext format="html">
      <text><![CDATA[{code}]]></text>
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>0.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
  </question>

  {questions}

</quiz>
"""
# read the code

with open(sys.argv[0]) as fp:
    CODE = fp.read()

LINK = """Program, s katerim so naloge generirane
  <a name="{name}" href="data:text/x-python;base64,{base64}">{name}</a>"""
# replace code with a link to base encoded file
if sys.version_info[0] >= 3:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(bytes(CODE, 'utf-8')).decode())
else:
    CODE = LINK.format(name=sys.argv[0], base64=base64.b64encode(CODE))

# write XML
XML = MOODLE_XML_TEMPLATE.format(category=CATEGORY, code=CODE, questions=questions)
with open(FILE_NAME+".xml", "w") as fp:
    print("Writing to file: {}.xml".format(FILE_NAME))
    fp.write(XML)

QUIZ.append(QuestionSet(text, parametri, resitev18, name="kroglice"))

with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import pi
import os
from scipy.special import binom
from moodle_xml import Quiz, NumAnswer, QuestionSet, ShortAnswer

POGLAVJE = "03 pogojna verjetnost"
CATEGORY = "Vaje/{}".format(POGLAVJE)
QUIZ = Quiz(CATEGORY)

QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/priprava na vaje"

text = """<p>Vržemo dve kocki.</p>  

    <p>Kolikšna je verjetnost, da je vsota pik na obeh kockah manjša od
    {n}?&nbsp;{ans[0]}</p> <p>Kolikšna je verjetnost, da je število pik na prvi
    enako {m} in vsota
    pik na obeh kockah manjša od {n}?&nbsp;{ans[1]}</p>

    <p>Po metu nam prijatelj pove, da je skupaj padlo manj kot {n} pik, nič pa 
    nam ne pove o posameznih kockah.  Kolikšna je pogojna verjetnost, da je število
    pik na prvi kocki enako {m}, če je vsota pik manjša od {n}?&nbsp;{ans[2]}</p> """

def resitev0(n, m):
    k = [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1]
    assert sum(k) == 36
    pA = sum(k[:(n-2)])/36
    pAB = (n-m-1)/36
    assert pAB < 1/6
    pB_pog_A = pAB/pA
    ans = [NumAnswer(a) for a in (pA, pAB, pB_pog_A)]
    return {"ans": ans}


parametri = [{"n": n, "m": m} for n in [7, 8, 9] for m in [3, 4, 5]]
QUIZ.append(QuestionSet(text, parametri, resitev0, name="dve kocki"))


QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/pogojna verjetnost"

text = """ <p>Na mizi sta dve posodi. V prvi posodi je <b>{r[0]} rdečih</b> in
    <b>{m[0]} modrih</b> kroglic, v drugi pa <b>{r[1]} rdečih</b> in <b>{m[1]}
    modrih</b>. Iz prve posode na slepo izberemo 1 kroglico in jo prestavimo v
    drugo posodo. Nato iz druge posode izvlečemo dve kroglici.</p>

    <p>Kolikšna je verjetnost, da smo prestavili modro
    kroglico?&nbsp;{ans[0]}</p>

    <p>Kolikšna je verjetnost, da sta izvlečeni kroglici modri?{ans[1]}</p>

    <p>Kolikšna je verjetnost, da smo prestavili modro kroglico, če sta obe
    izvlečeni modri?&nbsp;{ans[2]}</p> """

def resitev1(r, m):
    prva = m[0] + r[0]
    druga = m[1] + r[1]
    h1 = m[0]/prva*binom(1 + m[1], 2)/binom(1 + druga, 2)
    h2 = r[0]/prva*binom(m[1], 2)/binom(1 + druga, 2)
    
    ans = [NumAnswer(a) for a in (m[0]/prva, h1 + h2, h1/(h1+h2))]
    return {"ans": ans}

parametri = [{"r": r, "m":m} for r in [(5, 7), (7, 5), (6, 7)] for m in [(6, 8), (8, 6)]]
QUIZ.append(QuestionSet(text, parametri, resitev1, name="kroglice dve posodi"))


text = """
    <p>Restavracija Lakotnik je Alenki obljubila, da bodo naročeno hrano
    dostavili med 12h in <b>{v}</b> minut čez poldne. Njen brat Blaž ji je
    sporočil, da jo bo obiskal enkrat med 12h in 13h.</p>
    <p>Kolikšna je verjetnost, da bodo hrano dostavili pred 12:{b}?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo Blaž obiskal Alenko pred dostavo hrane?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da je bila dostava največ <b>{b}</b> minut za
    obiskom Blaža, če vemo, da je Blaž obiskal Alenko pred
    dostavo?&nbsp;{ans[2]}</p> """
def resitev2(v, b):
    ans = (b/v, (v+0.0)/120, 1-(v-b)**2/v**2)
    ans = [NumAnswer(an) for an in ans]
    return {"ans": ans}


parametri = []
for v in [20, 30, 40]:
    for b in [10, 15, 20]:
            if b < v:
                parametri.append({"v":v,"b": b})


QUIZ.append(QuestionSet(text, parametri, resitev2, name="restavracija"))

text = """
    <p>V škatli je <b>{b}</b> belih, <b>{r}</b> rdečih in <b>{c}</b> črnih
    kroglic. Izvlečemo 3 kroglice, ki jih <b>vračamo</b>.</p> <p>Kolikšna je
    verjetnost, da sta vsaj dve enake barve?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da sta prva in tretja beli?&nbsp;{ans[1]}</p>
    <p>Kolikšna je verjetnost, da sta prva in tretja beli, če sta bili vsaj dve
    enake barve?&nbsp;{ans[2]}</p>
    """
def resitev3(b, r, c):
    a = b + r + c
    p2e = 1 - 6*b*r*c/a**3 
    p1b3b = b**2/a**2 
    p1b3b_ce_2e = p1b3b/p2e
    ans = (p2e, p1b3b, p1b3b_ce_2e)
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}


parametri = []
for b in [5,6]:
    for r in [b, b+1, b+2]:
        for c in [r+1, r+2]:
            parametri.append({"b":b, "r":r, "c": c})


QUIZ.append(QuestionSet(text, parametri, resitev3, name="kroglice"))


text = """ <p>V posodi je <b>{r[0]} rumenih</b> in <b>{m[0]} modrih</b> kroglic.
    Vržemo kocko in, če je število pik manjše od <b>{pik}</b>, v posodo dodamo
    <b>{r[1]} rumene</b>, sicer pa <b>{m[1]} modre</b> kroglice. Iz posode nato
    na slepo izberemo <b>1</b> kroglico.</p>
    <ol>
    <li>Kolikšna je verjetnost, da smo izvlekli rumeno
    kroglico?&nbsp;{ans[0]}</li>

    <li>Kolikšna je verjetnost, da smo v posodo dodali rumene kroglice?{ans[1]}</li>

    <li>Kolikšna je verjetnost, da je padla 6, če je bila izvlečena kroglica
    modra?&nbsp;{ans[2]}</li> 
    </ol>
    """
def resitev4(r, m, pik):
    posoda = m[0] + r[0]
    p = (pik-1)/6
    r_h1 = (r[0] + r[1])/(posoda + r[1])
    r_h2 = r[0]/(posoda + m[1])
    ans =  [p*r_h1 + (1-p)*r_h2]
    ans.append(p)
    ans.append(1/6*(1-r_h2)/(1-ans[0]))
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}


parametri = []
for r in [(5, 3), (7, 4)]:
    for m in [(6, 3), (8, 4)]:
        for pik in [3, 5]:
            parametri.append({"r": r, "m": m, "pik": pik})

QUIZ.append(QuestionSet(text, parametri, resitev4, name="kocka in kroglice"))

text = """ <p>Vržemo pošten kovanec. Če pade <b>cifra</b>, vržemo <b>eno</b> kocko, če
    pade <b>grb</b> pa <b>dve</b> kocki.
    </br>

    Kolikšna je verjetnost, da je število pik manj kot <b>{k}</b>, če pade cifra?&nbsp; {ans[0]}</p>

    <p>Kolikšna je verjetnost, da je število pik manj kot <b>{k}</b>, če pade grb?&nbsp; {ans[1]}</p>

    <p>Kolikšna je verjetnost, da je število pik manj kot <b>{k}</b>? {ans[2]}</p>

    <p>Kolikšna je verjetnost, da je padla <b>{cg}</b>, če je število pik manj kot
    <b>{k}</b>?&nbsp;{ans[3]}</p> """
def resitev5(k, cg):
    p = 0.5
    p_c = (0, 1/6, 2/6, 3/6, 4/6, 5/6)
    p_g = (0, 0, 1/36, 3/36, 6/36, 10/36)
    p_k = p*p_c[k-1] + p*p_g[k-1]
    p_cg = {"grb": p*p_g[k-1]/p_k, "cifra": p*p_c[k-1]/p_k}
    ans = (p_c[k-1], p_g[k-1], p_k, p_cg[cg])
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}


parametri = []
for k in [3, 4, 5]:
    for cg in ["grb", "cifra"]:
        parametri.append({"k": k, "cg": cg})


QUIZ.append(QuestionSet(text, parametri, resitev5, name="kovanec in kocki"))

text = """
    <p>Z računalnikom generiramo dve naključni realni števili <i>x</i> in
    <i>y</i> na intervalu $[0,1]$.</p> <p>Kolikšna je verjetnost, da bo točka
    <i>(x,y)</i> več kot <b>{r}</b> oddaljena od izhodišča?&nbsp;{ans[0]}</p>
    <p>Kolikšna je verjetnost, da bo razlika <i>(x+y)-{a}</i> pozitivna in
    razdalja točke <i>(x,y)</i> do izhodišča manj kot
    <b>{r}</b>?&nbsp;{ans[1]}</p> <p>Kolikšna je verjetnost, da bo
    <i>y+x-{a}>0</i>, če vemo, da je bila razdalja do izhodišča manj kot
    <b>{r}</b>?&nbsp;{ans[2]}</p>
    """ 
def resitev6(r, a):
    ans = [ 1-pi*r**2/4,
            a**2/2,
            (a**2/2)/(pi*r**2/4)]
    ans = [NumAnswer(an) for an in ans]
    return {"ans": ans}


parametri = []
for a in [0.3, 0.5, 0.4]:
    for r in [1, 0.5]:
        parametri.append({"r": r, "a": a})


QUIZ.append(QuestionSet(text, parametri, resitev6, name="enakomerna na kvadratu"))



QuestionSet.DEFAULT_CATEGORY = CATEGORY+"/Bayes"

text = """ <p>V skladišču je <b>{p}</b> procentov jabolk iz Poljske, <b>{h}</b>
    procentov iz Hrvaške, ostala pa so iz Slovenije. Delež jabolk vrste zlati
    delišes je med jabolki iz Poljske enak <b>{zdp}</b>, med jabolki iz Hrvaške
    je delež enak <b>{zdh}</b>, med slovenskimi jabolki pa <b>{zds}</b>.
   Naključno izberemo en zaboj jabolk.</p>
    <ol>
    <li> Kolikšna je verjetnost, da smo
    izbrali zlati delišes iz Slovenije? &nbsp; {ans[0]}</li>
    <li>Kolikšna je verjetnost, da smo izbrali zlati delišes?{ans[1]}</li>
    <li>Kolikšna je verjetnost, da so jabolka slovenska, če smo izbrali zlati delišes?&nbsp;{ans[2]}</li> 
    </ol>
    """
def resitev7(p, h, zdp, zdh, zds):
    sl_in_zd = (1-h/100-p/100)*zds
    pl_in_zd = p/100*zdp
    hr_in_zd = h/100*zdh
    zd = sl_in_zd + pl_in_zd + hr_in_zd
    ans =  [sl_in_zd, sl_in_zd + pl_in_zd + hr_in_zd, sl_in_zd/zd]
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}


parametri = []

for p in [30, 20]:
    for h in [10, 15]:
        for zds in [0.2, 0.3]:
            parametri.append({"p": p, "h": h, "zdp": h/100, "zdh": (p+h)/100, "zds": zds})
            


QUIZ.append(QuestionSet(text, parametri, resitev7, name="jabolka"))


text = """ <p>Med vso pošto je <b>{s}</b> procentov nezaželene. Med nezaželeno pošto vsako <b>{i_s}.</b>
    sporočilo vsebuje besedo <b>invoice</b>. Delež sporočil z besedo <b>invoice</b> med zaželeno pošto je <b>{i_h}</b>.
    Dobimo novo sporočilo.
    </p>
    <ol>
    <li> Kolikšna je verjetnost, da sporočilo vsebuje besedo <b>invoice</b>?&nbsp; {ans[0]}</li>
    <li>Kolikšna je verjetnost, da je sporočilo <b>zaželeno</b> in vsebuje besedo <b>invoice</b>? {ans[1]}</li>
    <li>Opazimo, da sporočilo vsebuje besedo <b>invoice</b>. Kolikšna je verjetnost, da je sporočilo <b>nezaželeno</b>?&nbsp;{ans[2]}</li> 
    </ol>"""
def resitev8(s, i_s, i_h):
    spam = s/100
    ham = 1 - spam
    invoice = i_s*spam + i_h*ham
    i_and_ham = i_h*ham
    spam_invoice = i_s*spam/invoice
    ans =  [invoice, i_and_ham, spam_invoice]
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}


parametri = []
for s in [60, 40]:
    for i_s in [0.2, 0.25]:
        for i_h in [0.02, 0.03]:
            parametri.append({"s": s, "i_s": i_s, "i_h": i_h})

QUIZ.append(QuestionSet(text, parametri, resitev8, name="spam"))

text = """ <p>Verjetnost, da ženska zanosi, če ima prejšnji mesec nezaščiten
seks je <b>{p}</b> procentov. Test nosečnosti je pozitiven za <b>{tp}</b>
procentov žensk, ki so noseče. Poleg tega je test pozitiven za <b>{fp}</b>
procente žensk, ki niso noseče.  Mlad par se trudi zanositi in dekle opravi
nosečniški test. 
    </p>
    <ol>
    <li> Kolikšna je verjetnost, da je test pozitiven?&nbsp; {ans[0]}</li>
    <li>Kolikšna je verjetnost, da je dekle <b>zanosilo</b> in test <b>ni pozitiven</b>? {ans[1]}</li>
    <li> Kolikšna je verjetnost, da je dekle zanosilo, če je test pozitiven??&nbsp;{ans[2]}</li> 
    </ol>"""
def resitev9(p, tp, fp):
    p0 = p/100
    p_tp = tp/100*p0 + fp/100*(1-p0)
    pz_in_nt = p0*(1-tp/100)
    pz_ce_tp = (tp/100*p0)/p_tp
    ans =  [p_tp, pz_in_nt, pz_ce_tp]
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}


parametri = []
for p in [30, 20]:
    for tp in [95, 97]:
        for fp in [2, 3]:
            parametri.append({"p":p, "tp": tp, "fp": fp})

QUIZ.append(QuestionSet(text, parametri, resitev9, name="nosečniški test"))

text = """ <p>Študent se od <b>{n}</b> izpitnih vprašanj nauči le
    <b>{k}</b>. Za vsako vprašanje, ki se ga nauči, je potem še 30%
    verjetnosti, da pozabi odgovor, za vsako vprašanje, ki se ga ne nauči, pa
    je še 10% verjetnosti, da odgovor ugane.     
    </p>
    <ol>
    <li> Kolikšna je verjetnost, da na naključno izbrano vprašanje študent
    odgovoril prailno?&nbsp; {ans[0]}</li> 
    <li>Če je študent odgovoril pravilno, kolikšna je verjetnost, da je dobil
    vprašanje, ki se ga je naučil? &nbsp;{ans[1]}</li>
    <li> Študent na izpitu dobi 2 vprašanji in izpit opravi, če zna na obe
    pravilno odgovoriti. Kolikšna je verjetnost, da izpit
    opravi?&nbsp;{ans[2]}</li> 
    </ol>"""
def resitev10(n, k):
    p = k/n*0.7+(n-k)/n*0.1
    pn_ce_p = (k/n*0.7)/p
    pi = binom(k, 2)/binom(n, 2)*0.7**2 + k*(n-k)/binom(n, 2)*0.7*0.1 + binom(n-k, 2)/binom(n, 2)*0.1**2
    ans =[p, pn_ce_p, pi]
    ans = [NumAnswer(a) for a in ans]
    return {"ans": ans}


parametri = []
for n in [50, 60, 70]:
    for p in [2/3, 3/4]:
        k = round(n*p)
        parametri.append({"n": n, "k": k})

QUIZ.append(QuestionSet(text, parametri, resitev10, name="izpit"))

with open(POGLAVJE.replace(" ","_")+".xml", "w") as file_pointer:
    file_pointer.write(str(QUIZ))
with open(POGLAVJE.replace(" ","_")+".tex", "wb") as file_pointer:
    file_pointer.write(QUIZ.tex.encode())
os.system("latexmk -pdf {}".format("vs_kvizi.tex"))
